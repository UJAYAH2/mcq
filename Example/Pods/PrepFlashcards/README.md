# PrepFlashcards

[![CI Status](https://img.shields.io/badge/build-passing-brightgreen)]()
[![Version](https://img.shields.io/badge/pod-0.4.3-blue)]()
[![Platform](https://img.shields.io/badge/platform-ios-lightgrey)]()

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PrepFlashcards is available through [DLEPodRepo](https://gitlab.com/pearsontechnology/gpt/etext/ios/dlepodrepo). To install
it, simply add the following line to your Podfile:

```ruby
pod 'PrepFlashcards'
```

## Author

Pearson Education, Inc.

## License

Copyright © 2021 Pearson Education, Inc. See the LICENSE file for more info.
