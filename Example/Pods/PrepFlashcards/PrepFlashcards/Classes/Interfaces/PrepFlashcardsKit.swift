// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  PrepFlashcardsKit.swift
//  PrepFlashcards
//
//  Created by Tharindu Perera on 4/9/21.
//

import Foundation
import UIKit

public class PrepFlashcardsKit {
    
    /**
     Singleton object reference for PrepFlashcardsKit
    */
    public static let shared = PrepFlashcardsKit()
    
    private init() {
        UIFont.registerFonts()
    }
    
    /**
     Set theme/appearance config.
     
     - Note: Not calling this method will make the PrepFashcards use .light theme by default. This needs to be called to set user's theme prefrence before calling launchView methods for the first time and every time user changes the theme preference.
     
     - Parameters:
        - appearance: `PrepFlashcardsTheme`, Create Deck Model to be presented on.
     ```
     let kit = PrepFlashcardsKit.shared
     kit.setConfig(...)
     ```
     */
    public func setConfig(themeColors: PrepThemeColors, appearance: PrepFlashcardsTheme, platform: ParentAppPlatform) {
        Config.theme = appearance
        Config.platform = platform
        Config.themeColors = themeColors
    }
    
    /**
     Launch create deck.
     
     - Parameters:
        - controller: `ViewController`, Create Deck Model to be presented on.
        - delegate: Reference to `PrepFlashcardsDelegate` methods.
        - presentationStyle: `UIModalPresentationStyle`, by default it's fullScreen.
     ```
     let kit = PrepFlashcardsKit.shared
     kit.launchCreateDeck(...)
     ```
     */
    public func launchCreateDeck(controller: UIViewController, delegate: PrepFlashcardsDelegate?,
                                 presentationStyle: UIModalPresentationStyle = .fullScreen) {
        let decksetUpVC = DeckSetUpViewController.loadFromNib()
        decksetUpVC.flashcardsDelegate = delegate
        decksetUpVC.setUpType = .creation
        decksetUpVC.modalPresentationStyle = presentationStyle
        controller.present(decksetUpVC, animated: true, completion: nil)
    }
    
    /**
     Launch edit deck.
     
     - Parameters:
        - deck: existing `Deck` object to be edited.
        - controller: `ViewController`, Edit Deck Model to be presented on.
        - delegate: Reference to `PrepFlashcardsDelegate` methods.
        - presentationStyle: `UIModalPresentationStyle`, by default it's fullScreen.
     ```
     let kit = PrepFlashcardsKit.shared
     kit.launchCreateDeck(...)
     ```
     */
    public func launchEditDeck(deck: Deck, controller: UIViewController, delegate: PrepFlashcardsDelegate,
                                 presentationStyle: UIModalPresentationStyle = .fullScreen) {
        let decksetUpVC = DeckSetUpViewController.loadFromNib()
        decksetUpVC.flashcardsDelegate = delegate
        decksetUpVC.deck = deck
        decksetUpVC.setUpType = .edit
        decksetUpVC.modalPresentationStyle = presentationStyle
        controller.present(decksetUpVC, animated: true, completion: nil)
    }
    
    /**
     Launch create card.
     
     - Parameters:
        - controller: `ViewController`, Create Card Model to be presented on.
        - delegate: Reference to `PrepFlashcardsDelegate` methods.
        - presentationStyle: `UIModalPresentationStyle`, by default it's fullScreen.
     ```
     let kit = PrepFlashcardsKit.shared
     kit.launchCreateCard(...)
     ```
     */
    public func launchCreateCard(controller: UIViewController, delegate: PrepFlashcardsDelegate,
                                 presentationStyle: UIModalPresentationStyle = .fullScreen) {
        launchCreateCard(controller: controller, delegate: delegate, shouldDismissPresentingVC: false, presentationStyle: presentationStyle)
    }
    
    /**
     Launch create card via notes.
     
     - Parameters:
        - text: user selected content text.
        - controller: `ViewController`, Create Card Model to be presented on.
        - decks: available user `Deck` objects.
        - cacheDeckSelectionBy: the id used to cache user's last deck selection. This can be either bookId or productId
        - delegate: Reference to `PrepFlashcardsDelegate` methods.
        - shouldDismissPresentingVC: set this to true if presenting vc needs to be dismissed on card create via notes vc dismissal, by default it's false.
        - presentationStyle: `UIModalPresentationStyle`, by default it's fullScreen.
     ```
     let kit = PrepFlashcardsKit.shared
     kit.launchCreateCardViaNotes(...)
     ```
     */
    public func launchCreateCardViaNotes(text: String, controller: UIViewController, decks: [Deck], cacheDeckSelectionBy: String?,
                                         delegate: PrepFlashcardsDelegate,
                                         shouldDismissPresentingVC: Bool = false,
                                         presentationStyle: UIModalPresentationStyle = .fullScreen) {
        let cardSetUpVC = getCreateCardViaNotesVC(text: text, decks: decks, cacheDeckSelectionBy: cacheDeckSelectionBy,
                                                  delegate: delegate, shouldDismissPresentingVC: shouldDismissPresentingVC)
        cardSetUpVC.modalPresentationStyle = presentationStyle
        controller.present(cardSetUpVC, animated: true, completion: nil)
    }
    
    /**
     Get create card via notes ViewController.
     
     - Parameters:
        - text: user selected content text.
        - decks: available user `Deck` objects.
        - cacheDeckSelectionBy: the id used to cache user's last deck selection. This can be either bookId or productId
        - delegate: Reference to `PrepFlashcardsDelegate` methods.
        - shouldDismissPresentingVC: set this to true if presenting vc needs to be dismissed on card create via notes vc dismissal, by default it's false.
     ```
     let kit = PrepFlashcardsKit.shared
     let cardCreateVC = kit.launchCreateCardViaNotes(...)
     ```
     */
    public func getCreateCardViaNotesVC(text: String, decks: [Deck], cacheDeckSelectionBy: String?,
                                         delegate: PrepFlashcardsDelegate,
                                         shouldDismissPresentingVC: Bool = false) -> UIViewController {
        let cardSetUpVC = CardSetUpViewController.loadFromNib()
        cardSetUpVC.flashcardsDelegate = delegate
        cardSetUpVC.setUpType = .creation
        cardSetUpVC.decks = decks
        cardSetUpVC.isCreationFromNotes = true
        cardSetUpVC.deckSelectionCacheId = cacheDeckSelectionBy
        cardSetUpVC.selectedText = text
        cardSetUpVC.shouldDismissPresentingVC = shouldDismissPresentingVC
        
        return cardSetUpVC
    }
    
    /**
     Launch edit card.
     
     - Parameters:
        - card: existing `Card` object to be edited.
        - controller: `ViewController`, Create Card Model to be presented on.
        - delegate: Reference to `PrepFlashcardsDelegate` methods.
        - presentationStyle: `UIModalPresentationStyle`, by default it's fullScreen.
     ```
     let kit = PrepFlashcardsKit.shared
     kit.launchEditCard(...)
     ```
     */
    public func launchEditCard(card: Card, controller: UIViewController, delegate: PrepFlashcardsDelegate,
                                 presentationStyle: UIModalPresentationStyle = .fullScreen) {
        let cardSetUpVC = getEditCardVC(card: card, delegate: delegate)
        cardSetUpVC.modalPresentationStyle = presentationStyle
        controller.present(cardSetUpVC, animated: true, completion: nil)
    }
    
    /**
     Get edit card ViewController.
     
     - Parameters:
        - card: existing `Card` object to be edited.
        - delegate: Reference to `PrepFlashcardsDelegate` methods.
     ```
     let kit = PrepFlashcardsKit.shared
     let cardEditVC = kit.getEditCardVC(...)
     ```
     */
    public func getEditCardVC(card: Card, delegate: PrepFlashcardsDelegate) -> UIViewController {
        let cardSetUpVC = CardSetUpViewController.loadFromNib()
        cardSetUpVC.flashcardsDelegate = delegate
        cardSetUpVC.card = card
        cardSetUpVC.setUpType = .edit
        
        return cardSetUpVC
    }
    
    /**
     Clear cached data for thr user.
     
     ```
     let kit = PrepFlashcardsKit.shared
     kit.clearCache(...)
     ```
     */
    public func clearCache() {
        UserData.clear()
    }
    
}
