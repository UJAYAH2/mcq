// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  PrepFlashcardsDelegate.swift
//  Flashcards
//
//  Created by Tharindu Perera on 4/9/21.
//

import Foundation

@objc public protocol PrepFlashcardsDelegate: class {
    // Deck Flow
    @objc optional func deckDidCreate(deck: Deck, shouldLaunchCreateCard: Bool, analytics: NSDictionary,
                                      onCompletion: @escaping ((_ isSuccess: Bool, _ message: String?, _ createdDeck: Deck?) -> Void))
    @objc optional func deckDidEdit(deck: Deck, analytics: NSDictionary, onCompletion: @escaping ((_ isSuccess: Bool, _ message: String?) -> Void))
    @objc optional func deckDidDelete(deck: Deck, analytics: NSDictionary, onCompletion: @escaping ((_ isSuccess: Bool, _ message: String?) -> Void))
    @objc optional func deckCreationDidCancel()
    @objc optional func deckEditDidCancel()
    
    // Card Flow
    @objc optional func cardDidCreate(card: Card, analytics: [NSDictionary], onCompletion: @escaping ((_ isSuccess: Bool, _ message: String?) -> Void))
    @objc optional func cardDidCreateViaNotes(card: Card, deck: Deck, analytics: NSDictionary, onCompletion: @escaping ((_ isSuccess: Bool, _ message: String?) -> Void))
    @objc optional func cardDidEdit(card: Card, analytics: NSDictionary, onCompletion: @escaping ((_ isSuccess: Bool, _ message: String?) -> Void))
    @objc optional func cardDidDelete(card: Card, analytics: NSDictionary, onCompletion: @escaping ((_ isSuccess: Bool, _ message: String?) -> Void))
    @objc optional func cardCreateDidFlip(analytics: NSDictionary)
    @objc optional func cardEditDidFlip(analytics: NSDictionary)
    @objc optional func cardCreationDidCancel(analytics: NSDictionary?)
    @objc optional func cardEditDidCancel()
}
