// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  DeckSetUpViewController.swift
//  Flashcards
//
//  Created by Tharindu Perera on 4/9/21.
//

import UIKit

class DeckSetUpViewController: FormInputViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var deckNameLabel: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var deckNameTextField: FCTextField!
    @IBOutlet weak var actionButton: FCActionButton!
    @IBOutlet weak var deleteButton: FCRoundButton!
    @IBOutlet weak var closeButton: FCCloseButton!
    
    let maxDeckTitleLength = 1000
    
    weak var flashcardsDelegate: PrepFlashcardsDelegate?
    var setUpType: SetUpType = .creation
    var deck: Deck?
    
    private var isDeckTitleEditing = false
    private var deckTitle = "" {
        didSet {
            enableActionButtonIfNeeded()
            showDeckTitleCharLimitErrorIfNeeded()
        }
    }
    
    // MARK: - Controller Methods

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setUp() {
        super.setUp()
        
        switch setUpType {
        case .creation:
            titleLabel.text = Title.deckCreate.rawValue.localized
            actionButton.setTitle(Action.next.rawValue.localized, for: .normal)
            deleteButton.isHidden = true
        case .edit:
            deckTitle = deck?.title?.trim() ?? ""
            
            titleLabel.text = Title.deckEdit.rawValue.localized
            actionButton.setTitle(Action.save.rawValue.localized, for: .normal)
            deleteButton.isHidden = false
            
            deckNameTextField.text = deckTitle
            deckNameTextField.setCursorAtEnd()
            
        }
        enableActionButtonIfNeeded()
        showDeckTitleCharLimitErrorIfNeeded()
        deckNameTextField.delegate = self
        deckNameTextField.accessibilityLabel = A11yConstant.deckName.rawValue.localized
        deckNameTextField.becomeFirstResponder()
        titleLabel.accessibilityTraits = .header
        deleteButton.accessibilityLabel = A11yConstant.delete.rawValue.localized
        deckNameLabel.markAsRequired()
    }
    
    override func setUpAppearance() {
        super.setUpAppearance()
        titleLabel.textColor = Config.themeColors.primaryTextColor1
        closeButton.tintColor = Config.themeColors.navigationBarButtonColor
        deckNameLabel.textColor = Config.themeColors.primaryTextColor2
        errorLabel.textColor = Config.themeColors.errorTextColor
    }
    
    override func onUpdateKeyboardHeight(height: CGFloat) {
        super.onUpdateKeyboardHeight(height: height)
        
        guard device.isIphone else {
            return
        }
        
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: height , right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    // MARK: - Private Methods
    
    private func enableActionButtonIfNeeded() {
        actionButton.setEnabled(isEnabled: hasValidTitle())
    }
    
    private func showDeckTitleCharLimitErrorIfNeeded() {
        let charLimitDidExceed = isDeckTitleCharLimitExceeded()
        errorLabel.isHidden = !charLimitDidExceed
        if charLimitDidExceed {
            deckNameTextField.setErrorHighlighted()
            if UIAccessibility.isVoiceOverRunning {
                UIAccessibility.post(notification: .announcement, argument: errorLabel.text)
            }
        } else if isDeckTitleEditing {
            deckNameTextField.setActive()
        } else {
            deckNameTextField.setInactive()
        }
    }
    
    private func hasValidTitle() -> Bool {
        return !deckTitle.isEmpty && deckTitle.count <= maxDeckTitleLength
    }
    
    private func isDeckTitleCharLimitExceeded() -> Bool {
        return !deckTitle.isEmpty && deckTitle.count > maxDeckTitleLength
    }
    
    // MARK: - Actions
    
    @IBAction func onDeckTitleChange(_ sender: FCTextField) {
        deckTitle = sender.text?.trim() ?? ""
    }
    
    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: true) {
            switch self.setUpType {
            case .creation:
                self.flashcardsDelegate?.deckCreationDidCancel?()
            case .edit:
                self.flashcardsDelegate?.deckEditDidCancel?()
            }
        }
    }
    
    @IBAction func submit(_ sender: FCActionButton) {
        dismissFirstReponder()
        activityIndicator(show: true)
        switch self.setUpType {
        case .creation:
            let deck = Deck(title: deckTitle)
            let analyticsData = GAnalytics.deckCreate(path: .studyArea)
            flashcardsDelegate?.deckDidCreate?(deck: deck, shouldLaunchCreateCard: true, analytics: analyticsData,
                                               onCompletion: { isSuccess, message, _ in
                self.activityIndicator(show: false)
                if isSuccess {
                    self.dismiss(animated: true, completion: nil)
                } else if let msg = message {
                    self.showMesssage(message: msg, type: .error)
                }
            })
        case .edit:
            guard let deck = self.deck else { return }
            deck.title = deckTitle
            let analyticsData = GAnalytics.deckEdit(deck: deck)
            flashcardsDelegate?.deckDidEdit?(deck: deck, analytics: analyticsData, onCompletion: { isSuccess, message in
                self.activityIndicator(show: false)
                if isSuccess {
                    self.dismiss(animated: true, completion: nil)
                } else if let msg = message {
                    self.showMesssage(message: msg, type: .error)
                }
            })
        }
    }
    
    @IBAction func deleteDeck(_ sender: UIButton) {
        let deckDeleteVC = DeleteResourceViewController.loadFromNib()
        deckDeleteVC.flashcardsDelegate = flashcardsDelegate
        deckDeleteVC.resourceType = .deck
        deckDeleteVC.deck = self.deck
        deckDeleteVC.modalPresentationStyle = self.modalPresentationStyle
        self.present(deckDeleteVC, animated: true, completion: nil)
    }
}

extension DeckSetUpViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == deckNameTextField {
            isDeckTitleEditing = true
            if !isDeckTitleCharLimitExceeded() {
                deckNameTextField.setActive()
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == deckNameTextField {
            isDeckTitleEditing = false
            // As if auto correct triggered, that does not trigger text change event
            // and email value might not get updated
            deckTitle = textField.text?.trim() ?? ""
        }
    }
}
