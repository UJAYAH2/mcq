// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  BaseViewController.swift
//  Flashcards
//
//  Created by Tharindu Perera on 4/9/21.
//

import UIKit
import MBProgressHUD

class BaseViewController: UIViewController {
    
    var hud: MBProgressHUD = MBProgressHUD()
    let appearance: FCAppearance = Config.theme.appearance

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpAppearance()
        setUp()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return Config.themeColors.statusBarStyle
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setUp() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.handleOrientationChange),
                                               name: UIDevice.orientationDidChangeNotification,
                                               object: nil)
    }
    
    func setUpAppearance() {
        view.backgroundColor = Config.themeColors.primaryBackgroundColor
    }
    
    func activityIndicator(show: Bool) {
        // Temporary implementation untile UI/UX is finalized
        show ? showActivityIndicator() : hideActivityIndicator()
    }
    
    func showMesssage(message: String, type: FCMessageType) {
        // Temporary implementation untile UI/UX is finalized
        var title = ""
        switch type {
        case .success:
            title = "Success"
        case .error:
            title = "Failure"
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let yesAction = UIAlertAction(title: "Ok", style: .default)
        alertController.addAction(yesAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func handleOrientationChange() {
        // Child classes can override this to observe orientation changes
    }
    
    // MARK: - Private Methods
    
    private func showActivityIndicator() {
        DispatchQueue.main.async {
            self.hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            self.hud.mode = MBProgressHUDMode.indeterminate
            self.hud.label.text = nil
            self.hud.backgroundView.color = UIColor.lightGray.withAlphaComponent(0.3)
            self.hud.backgroundView.alpha = 0.3
            self.hud.show(animated: true)
        }
    }
    
    private func hideActivityIndicator() {
        DispatchQueue.main.async {
            self.hud.hide(animated: true)
        }
    }

}
