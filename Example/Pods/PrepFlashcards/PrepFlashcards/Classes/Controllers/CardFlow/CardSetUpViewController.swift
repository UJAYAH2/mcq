// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  CardSetUpViewController.swift
//  Flashcards
//
//  Created by Priyanka Suduge on 4/20/21.
//

import UIKit
import IQKeyboardManagerSwift

class CardSetUpViewController: FormInputViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dividerTopView: UIView!
    @IBOutlet weak var addToDeckLabel: UILabel!
    @IBOutlet weak var addToDeckDetailedLabel: UILabel!
    @IBOutlet weak var dividerBottomView: UIView!
    @IBOutlet weak var frontLabel: UILabel!
    @IBOutlet weak var frontDetailedLabel: UILabel!
    @IBOutlet weak var backLabel: UILabel!
    @IBOutlet weak var backDetailedLabel: UILabel!
    
    @IBOutlet weak var addAnotherButton: FCActionButton!
    @IBOutlet weak var saveAndExitButton: FCRoundButton!
    @IBOutlet weak var saveButton: FCActionButton!
    @IBOutlet weak var closeButton: FCCloseButton!
    @IBOutlet weak var flipButton: FCRoundButton!
    @IBOutlet weak var deleteButton: FCRoundButton!
    
    @IBOutlet weak var frontTextView: FCTextView!
    @IBOutlet weak var backTextView: FCTextView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var deckSelectionView: UIView!
    @IBOutlet weak var deckSelectionTextField: FCTextField!
    @IBOutlet weak var addDeckButton: FCSecondaryButton!
    @IBOutlet weak var deckPickerButton: UIButton!
    
    private let deckPickerView = UIPickerView()
    
    weak var flashcardsDelegate: PrepFlashcardsDelegate?
    var setUpType: SetUpType = .creation
    var card: Card?
    var decks: [Deck] = []
    var isCreationFromNotes = false
    var deckSelectionCacheId: String?
    var selectedText = ""
    var shouldDismissPresentingVC = false
    
    private var isNewDeckCreated = false
    private var cardFrontIsEmpty = true
    private var cardBackIsEmpty = true
    private var iqKeyboardState = false
    private var selectedDeck: Deck? {
        didSet {
            deckSelectionTextField.text = selectedDeck?.title
            deckPickerButton.accessibilityLabel = "\(selectedDeck?.title ?? "") "
            deckPickerButton.accessibilityValue = A11yConstant.deckPicker.rawValue.localized
            updateAvailabilityOfButtons()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        IQKeyboardManager.shared.enable = true
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        IQKeyboardManager.shared.enable = iqKeyboardState
    }
    
    override func setUp() {
        super.setUp()
        
        IQKeyboardManager.shared.enableAutoToolbar = false
        
        switch setUpType {
        
        case .creation:
            deleteButton.isHidden = true
            titleLabel.text = Title.cardCreate.rawValue.localized
            if isCreationFromNotes{
                sortDeckList()
                saveAndExitButton.isHidden = true
                addAnotherButton.isHidden = true
                saveButton.setTitle(Action.save.rawValue.localized, for: .normal)
                setUpDeckPikerView()
                addDeckButton.accessibilityLabel = A11yConstant.addDeck.rawValue.localized
                backTextView.insertText(selectedText)
            } else {
                saveButton.isHidden = true
                addAnotherButton.setTitle(Action.addAnother.rawValue.localized, for: .normal)
                saveAndExitButton.setTitle(Action.saveAndExit.rawValue.localized, for: .normal)
                frontTextView.becomeFirstResponder()
            }
            
        case .edit:
            saveAndExitButton.isHidden = true
            addAnotherButton.isHidden = true
            titleLabel.text = Title.cardEdit.rawValue.localized
            saveButton.setTitle(Action.save.rawValue.localized, for: .normal)
            deleteButton.accessibilityLabel = A11yConstant.delete.rawValue.localized
            frontTextView.insertText(card?.front ?? "")
            backTextView.insertText(card?.back ?? "")
            backTextView.becomeFirstResponder()
        }
        
        frontTextView.setPlaceholderText(A11yConstant.cardFrontPlaceholder.rawValue.localized)
        backTextView.setPlaceholderText(A11yConstant.cardBackPlaceholder.rawValue.localized)
        
        flipButton.accessibilityLabel = A11yConstant.flip.rawValue.localized
        closeButton.accessibilityLabel = A11yConstant.close.rawValue.localized
        
        updateAvailabilityOfButtons()
        
        titleLabel.accessibilityTraits = .header
        
        iqKeyboardState = IQKeyboardManager.shared.enable
        
        if !isCreationFromNotes {
            deckSelectionView.heightAnchor.constraint(equalToConstant: 0).isActive = true
            view.layoutIfNeeded()
        }
        
        addAnotherButton.correctTitleLineHeight(baselineOffset: -3.8)
        saveAndExitButton.correctTitleLineHeight(baselineOffset: -3.8)
        
        frontLabel.markAsRequired()
        backLabel.markAsRequired()
    }
    
    override func setUpAppearance() {
        super.setUpAppearance()
        titleLabel.textColor = Config.themeColors.primaryTextColor1
        
        dividerTopView.backgroundColor = Config.themeColors.dividerColor
        dividerBottomView.backgroundColor = Config.themeColors.dividerColor
        
        closeButton.tintColor = Config.themeColors.navigationBarButtonColor
        
        addToDeckLabel.textColor = Config.themeColors.primaryTextColor1
        addToDeckDetailedLabel.textColor =  Config.themeColors.primaryTextColor3
        frontLabel.textColor = Config.themeColors.primaryTextColor1
        frontDetailedLabel.textColor = Config.themeColors.primaryTextColor3
        backLabel.textColor = Config.themeColors.primaryTextColor1
        backDetailedLabel.textColor = Config.themeColors.primaryTextColor3
        
        addDeckButton.tintColor = Config.themeColors.secondaryActionActiveTitleColor
        addDeckButton.backgroundColor = Config.themeColors.secondaryActionActiveBackgroundColor
        addDeckButton.layer.borderColor = Config.themeColors.secondaryActionActiveBorderColor.cgColor
    }
    
    override func handleOrientationChange() {
        super.handleOrientationChange()
        addAnotherButton.reApplyGradientBackgroundIfNeeded()
    }
    
    private func setUpDeckPikerView() {
        deckPickerView.backgroundColor = Config.themeColors.inputEnabledColor
        deckPickerView.dataSource = self
        deckPickerView.delegate = self
        
        deckSelectionTextField.setUIPickerAsInput(deckPickerView)
        setDeckSelectionEnabled(isEnabled: !decks.isEmpty)
        deckSelectionTextField.delegate = self
        deckSelectionTextField.isAccessibilityElement = false
        
        // Select previously selected deck or first deck as needed
        
        if !decks.isEmpty {
            
            if let deckSelectionCacheId = self.deckSelectionCacheId,
               let previouslySelectedDeckId = UserData.getLastDeckSelection(forId: deckSelectionCacheId),
               let previouslySelectedDeckIndex = decks.firstIndex(where: {$0.id == previouslySelectedDeckId}) {
                selectedDeck = decks[previouslySelectedDeckIndex]
                deckPickerView.selectRow(previouslySelectedDeckIndex, inComponent: 0, animated: false)
            } else {
                selectedDeck = decks.first
                deckPickerView.selectRow(0, inComponent: 0, animated: false)
            }
        }
        
        // Toolbar
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        toolBar.backgroundColor = Config.themeColors.inputEnabledColor
        toolBar.barTintColor = Config.themeColors.inputEnabledColor
        toolBar.isTranslucent = false
        
        let doneButtton = UIBarButtonItem(title: Action.done.rawValue.localized, style: .done, target: self, action: #selector(dismissFirstReponder))
        doneButtton.tintColor = Config.themeColors.primaryTextColor1
        
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace,
                                            target: nil, action: nil)
        toolBar.setItems([flexibleSpace, doneButtton], animated: false)
        
        deckSelectionTextField.inputAccessoryView = toolBar
    }
    
    private func setDeckSelectionEnabled(isEnabled: Bool) {
        deckSelectionTextField.setEnabled(isEnabled: isEnabled)
        deckPickerButton.isEnabled = isEnabled
        deckPickerButton.accessibilityHint = isEnabled ? A11yConstant.doubleTapToSelect.rawValue.localized : A11yConstant.deckPicker.rawValue.localized
    }
    
    private func updateAvailabilityOfButtons() {
        
        if !cardFrontIsEmpty || !cardBackIsEmpty {
            flipButton.setActive()
        } else {
            flipButton.setInactive()
        }
        
        switch setUpType {
        
        case .creation:
            let hasValidContent = !cardFrontIsEmpty && !cardBackIsEmpty
            
            if isCreationFromNotes {
                saveButton.setEnabled(isEnabled: hasValidContent && selectedDeck != nil)
            } else {
                addAnotherButton.setEnabled(isEnabled: hasValidContent)
                hasValidContent ? saveAndExitButton.setActive() : saveAndExitButton.setInactive(ignoreBackground: true)
            }
            
        case .edit:
            if !cardFrontIsEmpty && !cardBackIsEmpty {
                saveButton.setEnabled(isEnabled: true)
            } else {
                saveButton.setEnabled(isEnabled: false)
            }
        }
        
    }
    
    private func sortDeckList() {
        decks.sort(by: {$0.title?.localizedCaseInsensitiveCompare($1.title ?? "") == ComparisonResult.orderedAscending})
    }
    
    private func handleNewlyCreatedDeck(deck: Deck) {
        decks.append(deck)
        selectedDeck = deck
        sortDeckList()
        deckPickerView.reloadAllComponents()
        let newDeckIndex = decks.firstIndex(of: deck)
        deckPickerView.selectRow(newDeckIndex ?? 0, inComponent: 0, animated: false)
        setDeckSelectionEnabled(isEnabled: true)
        deckSelectionTextField.accessibilityHint =  A11yConstant.deckPicker.rawValue.localized
        
        self.isNewDeckCreated = true
    }
    
    private func dismiss(completion:  (()->Void)? = nil) {
        dismiss(animated: true, completion: completion)
        if shouldDismissPresentingVC {
            presentingViewController?.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - Actions

    @IBAction func close(_ sender: UIButton) {
        dismiss {
            switch self.setUpType {
            case .creation:
                if self.isCreationFromNotes {
                    guard let deck = self.selectedDeck else { return }
                    let deckType: DeckType = self.isNewDeckCreated ? .NewDeck : .ExistingDeck
                    let analyticsData = GAnalytics.cardCreateViaContentSelector(deckType: deckType, action: .cancelAction,
                                                                                deck: deck, highlightText: self.selectedText)
                    self.flashcardsDelegate?.cardCreationDidCancel?(analytics: analyticsData)
                } else {
                    self.flashcardsDelegate?.cardCreationDidCancel?(analytics: nil)
                }
            case .edit:
                self.flashcardsDelegate?.cardEditDidCancel?()
            }
        }
    }
    
    @IBAction func showDeckPicker(_ sender: Any) {
        deckSelectionTextField.becomeFirstResponder()
    }
    
    @IBAction func launchDeckCreate(_ sender: UIButton) {
        PrepFlashcardsKit.shared.launchCreateDeck(controller: self, delegate: self, presentationStyle: modalPresentationStyle)
    }
    
    @IBAction func flipFrontAndBack(_ sender: UIButton) {
        let frontText = frontTextView.text ?? ""
        let backText = backTextView.text ?? ""
        frontTextView.text = ""
        backTextView.text = ""
        frontTextView.insertText(backText)
        backTextView.insertText(frontText)
        
        // Analytics
        switch self.setUpType {
        case .creation:
            let analyticsData = GAnalytics.cardCreateFlip()
            self.flashcardsDelegate?.cardCreateDidFlip?(analytics: analyticsData)
        case .edit:
            guard let card = self.card else { return }
            let analyticsData = GAnalytics.cardEditFlip(card: card)
            self.flashcardsDelegate?.cardEditDidFlip?(analytics: analyticsData)
        }
    }
    
    @IBAction func deleteCard(_ sender: UIButton) {
        let deckDeleteVC = DeleteResourceViewController.loadFromNib()
        deckDeleteVC.flashcardsDelegate = flashcardsDelegate
        deckDeleteVC.resourceType = .card
        deckDeleteVC.card = self.card
        deckDeleteVC.deck = self.selectedDeck
        deckDeleteVC.modalPresentationStyle = self.modalPresentationStyle
        self.present(deckDeleteVC, animated: true, completion: nil)
    }
    
    @IBAction func saveChanges(_ sender: FCActionButton) {
        dismissFirstReponder()
        activityIndicator(show: true)
        
        if isCreationFromNotes {
            
            guard let deck = selectedDeck else { return }
            
            let card = Card()
            card.front = frontTextView.text
            card.back = backTextView.text
            
            if let deckSelectionCacheId = self.deckSelectionCacheId {
                UserData.saveLastDeckSelection(cacheById: deckSelectionCacheId, deckId: selectedDeck?.id ?? "N/A")
            }
            
            let deckType: DeckType = self.isNewDeckCreated ? .NewDeck : .ExistingDeck
            let analyticsData = GAnalytics.cardCreateViaContentSelector(deckType: deckType, action: .saveAction, deck: deck, highlightText: card.back)
            self.flashcardsDelegate?.cardDidCreateViaNotes?(card: card, deck: deck, analytics: analyticsData, onCompletion: { (isSuccess, message) in
                
                self.activityIndicator(show: false)
                
                if isSuccess {
                    self.dismiss()
                } else if let msg = message {
                    self.showMesssage(message: msg, type: .error)
                }
            })
        } else {
            guard let card = self.card else { return }
            card.front = frontTextView.text
            card.back = backTextView.text
            
            let analyticsData = GAnalytics.cardEdit(card: card)
            self.flashcardsDelegate?.cardDidEdit?(card: card, analytics: analyticsData, onCompletion: { (isSuccess, message) in
                
                self.activityIndicator(show: false)
                
                if isSuccess {
                    self.dismiss()
                } else if let msg = message {
                    self.showMesssage(message: msg, type: .error)
                }
            })
        }
    }
    
    @IBAction func saveAndExit(_ sender: FCRoundButton) {
        
        dismissFirstReponder()
        activityIndicator(show: true)
        
        let card = Card()
        card.front = frontTextView.text
        card.back = backTextView.text
        
        let analyticsSaveMethod = GAnalytics.addCardsToExistingDeck(card: card, saveMethod: .saveClose)
        let analyticsPath = GAnalytics.cardCreate()
        self.flashcardsDelegate?.cardDidCreate?(card: card, analytics: [analyticsSaveMethod, analyticsPath], onCompletion: { (isSuccess, message) in
            
            self.activityIndicator(show: false)
            
            if isSuccess {
                self.dismiss()
            } else if let msg = message {
                self.showMesssage(message: msg, type: .error)
            }
        })
    }
    
    @IBAction func addAnotherCard(_ sender: FCActionButton) {
        
        dismissFirstReponder()
        activityIndicator(show: true)
        
        let card = Card()
        card.front = frontTextView.text
        card.back = backTextView.text
        
        let analyticsAddCards = GAnalytics.addCardsToExistingDeck(card: card, saveMethod: .addAnother)
        let analyticsPath = GAnalytics.cardCreate()
        self.flashcardsDelegate?.cardDidCreate?(card: card, analytics: [analyticsAddCards, analyticsPath], onCompletion: { (isSuccess, message) in
            
            self.activityIndicator(show: false)
            
            if isSuccess {
                self.frontTextView.text = ""
                self.backTextView.text = ""
                self.frontTextView.insertText("")
                self.backTextView.insertText("")
                self.frontTextView.becomeFirstResponder()
                self.scrollView.setContentOffset(.zero, animated: true)
            } else if let msg = message {
                self.showMesssage(message: msg, type: .error)
            }
        })
    }
    
}

// MARK: - UITextView Delegates

extension CardSetUpViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        
        if !frontTextView.text.trim().isEmpty {
            cardFrontIsEmpty = false
        } else {
            cardFrontIsEmpty = true
        }
        if !backTextView.text.trim().isEmpty {
            cardBackIsEmpty = false
        } else {
            cardBackIsEmpty = true
        }
        
        updateAvailabilityOfButtons()
    }
}

// MARK: - Deck PikerView DataSource & Delegate

extension CardSetUpViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return decks.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return decks[row].title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedDeck = decks[row]
    }
    
}

// MARK: - TextFieldDelegate Delegate

extension CardSetUpViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIAccessibility.post(notification: .screenChanged, argument: deckPickerView)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIAccessibility.post(notification: .screenChanged, argument: self)
        let selectedDeckIndex = deckPickerView.selectedRow(inComponent: 0)
        selectedDeck = decks[selectedDeckIndex]
    }
}

// MARK: - PrepFlashcards Delegate For Deck Creation

extension CardSetUpViewController: PrepFlashcardsDelegate {
    
    func deckDidCreate(deck: Deck, shouldLaunchCreateCard: Bool, analytics: NSDictionary, onCompletion: @escaping ((Bool, String?, Deck?) -> Void)) {
        let analyticsData = GAnalytics.deckCreate(path: .selectedText)
        self.flashcardsDelegate?.deckDidCreate?(deck: deck, shouldLaunchCreateCard: false, analytics: analyticsData, onCompletion: { (success, error, createdDeck) in
            if let createdDeck = createdDeck, success {
                self.handleNewlyCreatedDeck(deck: createdDeck)
            }
            onCompletion(success, error, createdDeck)
        })
    }
}
