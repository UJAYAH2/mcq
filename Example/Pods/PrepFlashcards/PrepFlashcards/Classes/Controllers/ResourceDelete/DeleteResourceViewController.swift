// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  DeleteResourceViewController.swift
//  PrepFlashcards
//
//  Created by Nalinda Wickramarathna on 4/20/21.
//

import UIKit

enum ResourceType {
    case deck
    case card
}

class DeleteResourceViewController: BaseViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var deleteTitleLabel: UILabel!
    @IBOutlet weak var deleteMessageLabel: UILabel!
    @IBOutlet weak var closeButton: FCCloseButton!
    @IBOutlet weak var deleteButton: FCActionButton!
    @IBOutlet weak var deleteBGImage: UIView!
    
    weak var flashcardsDelegate: PrepFlashcardsDelegate?
    var resourceType: ResourceType = .deck
    var deck: Deck?
    var card: Card?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setUp() {
        super.setUp()
        
        switch resourceType {
        case .deck:
            titleLabel.text = Title.deckDelete.rawValue.localized
            deleteTitleLabel.text = Title.deckDeleteConfirmation.rawValue.localized
        case .card:
            titleLabel.text = Title.cardDelete.rawValue.localized
            deleteTitleLabel.text = Title.cardDeleteConfirmation.rawValue.localized
        }
        titleLabel.accessibilityTraits = .header
        deleteButton.setEnabled(isEnabled: true)
        self.deleteBGImage.isHidden = Config.platform == .standard ? true : false
    }
    
    override func setUpAppearance() {
        super.setUpAppearance()
        titleLabel.textColor = Config.themeColors.primaryTextColor1
        
        deleteTitleLabel.textColor = Config.themeColors.primaryTextColor1
        deleteMessageLabel.textColor = Config.themeColors.primaryTextColor1
        
        closeButton.tintColor = Config.themeColors.navigationBarButtonColor
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func submit(_ sender: Any) {
        switch self.resourceType {
        case .deck:
            guard let deck = self.deck else { return }
            activityIndicator(show: true)
            
            let analyticsData = GAnalytics.deckDelete(deck: deck)
            self.flashcardsDelegate?.deckDidDelete?(deck: deck, analytics: analyticsData, onCompletion: { (isSuccess, message) in
                self.activityIndicator(show: false)
                if isSuccess {
                    self.dismiss(animated: true, completion: nil)
                    self.presentingViewController?.dismiss(animated: true, completion: nil)
                } else if let msg = message {
                    self.showMesssage(message: msg, type: .error)
                }
            })
        case .card:
            guard let card = self.card else { return }
            activityIndicator(show: true)
            
            let analyticsData = GAnalytics.cardDelete(card: card)
            self.flashcardsDelegate?.cardDidDelete?(card: card, analytics: analyticsData, onCompletion: { (isSuccess, message) in
                self.activityIndicator(show: false)
                if isSuccess {
                    self.dismiss(animated: true, completion: nil)
                    self.presentingViewController?.dismiss(animated: true, completion: nil)
                } else if let msg = message {
                    self.showMesssage(message: msg, type: .error)
                }
            })
        }
    }
    
}
