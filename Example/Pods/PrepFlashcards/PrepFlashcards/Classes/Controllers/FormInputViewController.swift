// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  FormInputViewController.swift
//  Flashcards
//
//  Created by Tharindu Perera on 4/12/21.
//

import UIKit

class FormInputViewController: BaseViewController {
    
    // MARK: - Controller Methods

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setUp() {
        super.setUp()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissFirstReponder))
        view.addGestureRecognizer(tapGesture)
        notifyKeyboardHeightChanges()
    }
    
    // MARK: - Private Methods
    
    private func notifyKeyboardHeightChanges() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillChangeFrame(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
    }
    
    @objc private func keyboardWillChangeFrame(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            
            if (keyboardFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.onUpdateKeyboardHeight(height: 0)
            } else {
                self.onUpdateKeyboardHeight(height: keyboardFrame?.size.height ?? 0)
            }
        }
    }
    
    // MARK: - Events (To be implemented by child classes if needed)
    
    @objc func dismissFirstReponder() {
        view.endEditing(true)
    }
    
    @objc func onUpdateKeyboardHeight(height: CGFloat) { }

}
