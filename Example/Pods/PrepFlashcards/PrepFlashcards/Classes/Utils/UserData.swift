// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  UserData.swift
//  PrepFlashcards
//
//  Created by Tharindu Perera on 5/7/21.
//

import Foundation

let userdefaultsPrefix = "PFC_"
let lastDeckSelectionKey = "\(userdefaultsPrefix)LastDeckSelection"

class UserData {
    
    static let userDefaults = UserDefaults.standard
    
    static func saveLastDeckSelection(cacheById: String, deckId: String) {
        var deckSelection = getLastDeckSelectionDictionary()
        deckSelection[cacheById] = deckId
        userDefaults.setValue(deckSelection, forKey: lastDeckSelectionKey)
    }
    
    static func getLastDeckSelection(forId: String) -> String? {
        return getLastDeckSelectionDictionary()[forId]
    }
    
    static func clear() {
        userDefaults.removeObject(forKey: lastDeckSelectionKey)
    }
    
    // MARK: - Private Methods
    
    private static func getLastDeckSelectionDictionary() -> [String: String] {
        let deckSelection = userDefaults.object(forKey: lastDeckSelectionKey) as? [String: String] ?? [:]
        return deckSelection
    }
    
}
