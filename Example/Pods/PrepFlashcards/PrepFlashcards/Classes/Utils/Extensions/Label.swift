// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  Label.swift
//  PrepFlashcards
//
//  Created by Tharindu Perera on 6/17/21.
//

import UIKit

extension UILabel {
    
    func markAsRequired(symbol: String = "*") {
        let content = self.text ?? ""
        let modifiedContent = "\(content) \(symbol)"
        
        let attributtedString = NSMutableAttributedString(string: modifiedContent)
        attributtedString.addAttribute(.foregroundColor, value: Config.themeColors.requiredIndicatorColor,
                                       range: NSRange(location: modifiedContent.count - symbol.count, length: symbol.count))
        
        self.attributedText = attributtedString
        self.accessibilityLabel = "\(content) \(A11yConstant.required.rawValue.localized)"
    }
}
