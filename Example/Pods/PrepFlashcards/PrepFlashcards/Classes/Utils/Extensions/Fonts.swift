// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  Fonts.swift
//  Flashcards
//
//  Created by Tharindu Perera on 4/9/21.
//

import Foundation

import UIKit

extension UIFont {

    static func registerFonts() {
        let bundle = Bundle.getBundle()
        UIFont.registerFont("TTCommons-Regular", extension: "ttf", in: bundle)
        UIFont.registerFont("TTCommons-Medium", extension: "ttf", in: bundle)
        UIFont.registerFont("TTCommons-DemiBold", extension: "ttf", in: bundle)
        UIFont.registerFont("TTCommons-Bold", extension: "ttf", in: bundle)
        UIFont.registerFont("Hind-Regular", extension: "ttf", in: bundle)
        UIFont.registerFont("Hind-Light", extension: "ttf", in: bundle)
        UIFont.registerFont("Hind-Medium", extension: "ttf", in: bundle)
        UIFont.registerFont("Hind-SemiBold", extension: "ttf", in: bundle)
        UIFont.registerFont("Hind-Bold", extension: "ttf", in: bundle)
    }

    static func registerFont(_ fontName: String, extension: String, in bundle: Bundle) {
        guard let fontURL = bundle.url(forResource: fontName, withExtension: `extension`) else { return }
        CTFontManagerRegisterFontsForURL(fontURL as CFURL, CTFontManagerScope.process, nil)
    }
}
