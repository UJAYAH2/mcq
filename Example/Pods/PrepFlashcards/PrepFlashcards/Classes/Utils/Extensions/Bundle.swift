// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  Bundle.swift
//  Flashcards
//
//  Created by Tharindu Perera on 4/9/21.
//

import Foundation

extension Bundle {

    class BundleClass {}

    static func getBundle() -> Bundle {
        let podBundle = Bundle(for: BundleClass.self)
        let bundleURL = podBundle.url(forResource: BundleConstants.name,
                                      withExtension: BundleConstants.ext) ?? Bundle.main.bundleURL
        return Bundle(url: bundleURL) ?? Bundle.main
    }
}
