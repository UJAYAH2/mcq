// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  Colors.swift
//  Flashcards
//
//  Created by Tharindu Perera on 4/9/21.
//

import Foundation

extension UIColor {
    
    convenience init(hex: String) {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            self.init(red: 255, green: 255, blue: 255, alpha: 1.0)
        } else {
            var rgbValue:UInt32 = 0
            Scanner(string: cString).scanHexInt32(&rgbValue)
            
            self.init(
                red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                alpha: CGFloat(1.0)
            )
        }
    }
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }

    convenience init(hex: Int) {
        self.init(red:(hex >> 16) & 0xff, green:(hex >> 8) & 0xff, blue:hex & 0xff)
    }
    
    // MARK: - Theme Colors
    
    static let indigo = UIColor(hex: "#05112A")
    static let tintedWhite = UIColor(hex: "#F7F9FD")
    static let backgroundBlack = UIColor(hex: "#101010")
    static let backgroundSepia = UIColor(hex: "#F4F2E4")
    static let darkGrey = UIColor(hex: "#4F5561")
    static let lightGrey = UIColor(hex: "#E8EEFA")
    static let mediumGrey = UIColor(hex: "#9BA1AD")
    static let textGrey = UIColor(hex: "#333333")
    static let lightBlack = UIColor(hex: "#1C1C1E")
    static let darkSepia = UIColor(hex: "#EDEBDF")
    static let linkPlumDefault = UIColor(hex: "#6D0176")
    static let standardDarkBlue = UIColor(hex: "#005F79")
    static let standardLightBlue = UIColor(hex: "#A1EBFF")
    
    static let backgroundColor = UIColor(named: "BackgroundColor", in: Bundle.getBundle(), compatibleWith: nil)!
    
    // MARK: - Button Gradient Colors
    
    static let actionButtonActiveGradientColor1 = UIColor(named: "ActionButtonActiveGradientColor1", in: Bundle.getBundle(), compatibleWith: nil)!
    static let actionButtonActiveGradientColor2 = UIColor(named: "ActionButtonActiveGradientColor2", in: Bundle.getBundle(), compatibleWith: nil)!
    static let actionButtonActiveGradientColor3 = UIColor(named: "ActionButtonActiveGradientColor3", in: Bundle.getBundle(), compatibleWith: nil)!
}
