// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  Config.swift
//  PrepFlashcards
//
//  Created by Tharindu Perera on 5/20/21.
//

import Foundation

struct Config {
    static var theme: PrepFlashcardsTheme = .light
    static var platform: ParentAppPlatform = .mojo
    static var themeColors: PrepThemeColors = PrepThemeColors()
}

public struct PrepThemeColors {
    public var primaryBackgroundColor = UIColor(hex:0xF7F9FD)
    public var primaryCTAButtonColor = [UIColor(hex:0xEE512E), UIColor(hex:0xDF006B), UIColor(hex:0xBD0071)]
    public var primaryCTAButtonTitleColor = UIColor(hex:0xFEFEFE)
    public var disabledButtonBackgroundColor = UIColor(hex: 0xE8EEFA)
    public var disabledButtonTitleColor = UIColor(hex: 0x9BA1AD)
    public var navigationBarButtonColor = UIColor(hex: 0x4F5561)
    public var dividerColor = UIColor(hex: 0xE8EEFA)
    public var requiredIndicatorColor = UIColor(hex: 0xFF0000)
    public var statusBarStyle: UIStatusBarStyle = .default
    public var scrollIndicatorStyle: UIScrollView.IndicatorStyle = .default
    public var keyboardAppearance: UIKeyboardAppearance = .light
    
    public var primaryTextColor1 = UIColor(hex:0x05112A)
    public var primaryTextColor2 = UIColor(hex:0x333333)
    public var primaryTextColor3 = UIColor(hex:0x4F5561)
    public var errorTextColor = UIColor(hex: 0xB9041A)
    
    public var inputEnabledColor = UIColor(hex: 0xFFFFFF)
    public var inputDisabledColor = UIColor(hex: 0xE8EEFA)
    public var inputPlaceholderColor = UIColor(hex: 0x9BA1AD)
    public var inputActiveBorderColor = UIColor(hex: 0x9BA1AD)
    public var inputInactiveBorderColor = UIColor(hex: 0xE8EEFA)
    public var inputErrorBorderColor = UIColor(hex: 0xB00A4B)
    public var inputActiveImageColor = UIColor(hex: 0x4F5561)
    public var inputInactiveImageColor = UIColor(hex: 0x9BA1AD)
    
    public var secondaryActionActiveBackgroundColor = UIColor(hex: 0xFFFFFF)
    public var secondaryActionInactiveBackgroundColor = UIColor(hex: 0xE8EEFA)
    public var secondaryActionActiveTitleColor = UIColor(hex: 0x05112A)
    public var secondaryActionInctiveTitleColor = UIColor(hex: 0x9BA1AD)
    public var secondaryActionActiveBorderColor = UIColor(hex: 0xE8EEFA)
    public var secondaryActionInactiveBorderColor = UIColor(hex: 0xE8EEFA)

    public init() {}
}
