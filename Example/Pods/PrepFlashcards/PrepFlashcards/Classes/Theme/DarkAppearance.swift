// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  DarkAppearance.swift
//  PrepFlashcards
//
//  Created by Tharindu Perera on 5/20/21.
//

import Foundation

class DarkAppearance: FCAppearance {
    
    static let shared = DarkAppearance()
    
    private init() { }
    
    var statusBarStyle: UIStatusBarStyle = .lightContent
    var viewBackgroundColor: UIColor = .backgroundBlack
    var titleColor: UIColor = .tintedWhite
    var navigationBarItemColor: UIColor = .lightGrey
    var dividerColor: UIColor = .lightBlack
    var requiredIndicatorColor: UIColor = .red
    var scrollIndicatorStyle: UIScrollView.IndicatorStyle = .white
    var keyboardAppearance: UIKeyboardAppearance = .dark
    
    var primaryTextColor1: UIColor = .tintedWhite
    var primaryTextColor2 = UIColor(hex: "#FEFEFE") // Text-white
    var primaryTextColor3: UIColor = .lightGrey
    var errorTextColor = UIColor(hex: "#FF6482")
    
    var inputEnabledColor: UIColor = .lightBlack
    var inputDisabledColor: UIColor = .lightBlack
    var inputPlaceholderColor: UIColor = .lightGrey
    var inputActiveBorderColor: UIColor = .mediumGrey
    var inputInactiveBorderColor: UIColor = .textGrey
    var inputErrorBorderColor = UIColor(hex: "#FF6482")
    var inputActiveImageColor: UIColor = .lightGrey
    var inputInactiveImageColor: UIColor = .mediumGrey
    
    var actionInactiveBackgroundColor: UIColor = .lightBlack
    var actionActiveTitleColor: UIColor = .white
    var actionInactiveTitleColor: UIColor = .mediumGrey
    
    var secondaryActionActiveBackgroundColor: UIColor = .backgroundBlack
    var secondaryActionInactiveBackgroundColor: UIColor = .lightBlack
    var secondaryActionActiveTitleColor: UIColor = .tintedWhite
    var secondaryActionInctiveTitleColor: UIColor = .mediumGrey
    var secondaryActionActiveBorderColor: UIColor = UIColor.mediumGrey.withAlphaComponent(0.5)
    var secondaryActionInactiveBorderColor: UIColor = .lightBlack
    
    var destroyActionBackgroundColor = UIColor(hex: "#FF6482")
    
}
