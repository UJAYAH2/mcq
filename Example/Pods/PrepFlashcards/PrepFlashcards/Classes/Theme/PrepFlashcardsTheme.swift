// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  PrepFCTheme.swift
//  PrepFlashcards
//
//  Created by Tharindu Perera on 5/20/21.
//

import Foundation

public enum PrepFlashcardsTheme: Int {
    case light = 0
    case dark = 1
    case sepia = 2
    
    var appearance: FCAppearance {
        switch self {
        case .light:
            return LightAppearance.shared
        case .dark:
            return DarkAppearance.shared
        case .sepia:
            return SepiaAppearance.shared
        }
    }
}

public enum ParentAppPlatform: Int {
    case mojo = 0
    case standard = 1
}
