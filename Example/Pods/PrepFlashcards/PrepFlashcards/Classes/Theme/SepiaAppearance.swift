// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  SepiaAppearance.swift
//  PrepFlashcards
//
//  Created by Tharindu Perera on 5/20/21.
//

import Foundation

class SepiaAppearance: FCAppearance {
    
    static let shared = SepiaAppearance()
    
    private init() { }
    
    var statusBarStyle: UIStatusBarStyle = .default
    var viewBackgroundColor: UIColor = .backgroundSepia
    var titleColor: UIColor = .indigo 
    var navigationBarItemColor: UIColor = .indigo
    var dividerColor: UIColor = .darkSepia
    var requiredIndicatorColor: UIColor = .red
    var scrollIndicatorStyle: UIScrollView.IndicatorStyle = .default
    var keyboardAppearance: UIKeyboardAppearance = .light
    
    var primaryTextColor1: UIColor = .indigo
    var primaryTextColor2: UIColor = .textGrey
    var primaryTextColor3: UIColor = .darkGrey
    var errorTextColor = UIColor(hex: "#B9041A")
    
    var inputEnabledColor = UIColor(hex: "#FCFAED")
    var inputDisabledColor: UIColor = .darkSepia
    var inputPlaceholderColor: UIColor = .textGrey
    var inputActiveBorderColor: UIColor = .mediumGrey
    var inputInactiveBorderColor: UIColor = .darkSepia
    var inputErrorBorderColor = UIColor(hex: "#B00A4B")
    var inputActiveImageColor: UIColor = .darkGrey
    var inputInactiveImageColor: UIColor = .mediumGrey
    
    var actionInactiveBackgroundColor: UIColor = .darkSepia
    var actionActiveTitleColor: UIColor = .white
    var actionInactiveTitleColor: UIColor = .darkGrey
    
    var secondaryActionActiveBackgroundColor: UIColor = .backgroundSepia
    var secondaryActionInactiveBackgroundColor: UIColor = .darkSepia
    var secondaryActionActiveTitleColor: UIColor = .indigo
    var secondaryActionInctiveTitleColor: UIColor = .darkGrey
    var secondaryActionActiveBorderColor: UIColor = UIColor.mediumGrey.withAlphaComponent(0.5)
    var secondaryActionInactiveBorderColor: UIColor = .darkSepia
    
    var destroyActionBackgroundColor = UIColor(hex: "#7E031A")
}
