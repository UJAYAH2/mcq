// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  FCAppearance.swift
//  PrepFlashcards
//
//  Created by Tharindu Perera on 5/20/21.
//

import Foundation
import UIKit

protocol FCAppearance {
    
    // MARK: - Main Colors
    var statusBarStyle: UIStatusBarStyle { get }
    
    var viewBackgroundColor: UIColor { get }
    var titleColor: UIColor { get }
    var navigationBarItemColor: UIColor { get }
    var dividerColor: UIColor { get }
    var requiredIndicatorColor: UIColor { get }
    var scrollIndicatorStyle: UIScrollView.IndicatorStyle { get }
    
    var keyboardAppearance: UIKeyboardAppearance { get }
    
    // MARK: - Text Colors
    var primaryTextColor1: UIColor { get }
    var primaryTextColor2: UIColor { get }
    var primaryTextColor3: UIColor { get }
    
    var errorTextColor: UIColor { get }
    
    // MARK: - Input Field Colors
    var inputEnabledColor: UIColor { get }
    var inputDisabledColor: UIColor { get }
    var inputPlaceholderColor: UIColor { get }
    var inputActiveBorderColor: UIColor { get }
    var inputInactiveBorderColor: UIColor { get }
    var inputErrorBorderColor: UIColor { get }
    var inputActiveImageColor: UIColor { get }
    var inputInactiveImageColor: UIColor { get }
    
    // MARK: - Button Colors
    var actionInactiveBackgroundColor: UIColor { get }
    var actionActiveTitleColor: UIColor { get }
    var actionInactiveTitleColor: UIColor { get }
    
    var secondaryActionActiveBackgroundColor: UIColor { get }
    var secondaryActionInactiveBackgroundColor: UIColor { get }
    var secondaryActionActiveTitleColor: UIColor { get }
    var secondaryActionInctiveTitleColor: UIColor { get }
    var secondaryActionActiveBorderColor: UIColor { get }
    var secondaryActionInactiveBorderColor: UIColor { get }
    
    var destroyActionBackgroundColor: UIColor { get }
}
