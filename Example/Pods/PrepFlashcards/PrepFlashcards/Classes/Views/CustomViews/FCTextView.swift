// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  FCTextView.swift
//  Flashcards
//
//  Created by Priyanka Suduge on 4/21/21.
//

import UIKit

class FCTextView: UITextView {
    
    private var placeholderText = ""
    
    lazy var placeholderLabel: UILabel = {
        let label = UILabel()
        label.textColor = Config.themeColors.inputPlaceholderColor
        label.backgroundColor = .clear
        label.font = UIFont(name: "Hind-Regular", size: 16)
        label.text = placeholderText
        return label
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func initialize() {
        textColor = Config.themeColors.primaryTextColor1
        tintColor = Config.themeColors.primaryTextColor1
        backgroundColor = Config.themeColors.inputEnabledColor
        keyboardAppearance = Config.themeColors.keyboardAppearance
        layer.cornerRadius = 5
        layer.borderWidth = 1
        layer.borderColor = Config.themeColors.inputInactiveBorderColor.cgColor
        indicatorStyle = Config.themeColors.scrollIndicatorStyle
        textContainerInset = UIEdgeInsets(top: 10, left: 12, bottom: 4, right: 12)
        
        setupNotificationObserver()
    }
    
    private func setupNotificationObserver() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(FCTextView.textDidChangeHandler(notification:)),
                                               name: FCTextView.textDidChangeNotification,
                                               object: self)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(FCTextView.textDidEndEditingHandler(notification:)),
                                               name: FCTextView.textDidEndEditingNotification,
                                               object: self)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(FCTextView.textDidBeginEditingHandler(notification:)),
                                               name: FCTextView.textDidBeginEditingNotification,
                                               object: self)
    }
    
    @objc func textDidChangeHandler(notification: Notification) {
        setNeedsLayout()
    }
    
    @objc func textDidEndEditingHandler(notification: Notification) {
        super.layoutSubviews()
        layer.borderColor = Config.themeColors.inputInactiveBorderColor.cgColor
    }
    
    @objc func textDidBeginEditingHandler(notification: Notification) {
        super.layoutSubviews()
        layer.borderColor = Config.themeColors.inputActiveBorderColor.cgColor
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        if text.isEmpty {
            placeholderLabel.copyProperties(from: self)
            addSubview(placeholderLabel)
            bringSubviewToFront(placeholderLabel)
        } else {
            placeholderLabel.removeFromSuperview()
        }
    }
}

// MARK: - Public Methods

extension FCTextView {
    
    func setPlaceholderText(_ placeholderText: String = "") {
        self.placeholderText = placeholderText
    }
}

// MARK: - Private Extensions

private extension UILabel {
    func copyProperties(from textView: UITextView) {
        let outerFrame = textView.bounds.inset(by: textView.textContainerInset)
        frame = CGRect(x: outerFrame.origin.x + 4, y: outerFrame.origin.y, width: outerFrame.width, height: 24)
        textAlignment = textView.textAlignment
    }
}
