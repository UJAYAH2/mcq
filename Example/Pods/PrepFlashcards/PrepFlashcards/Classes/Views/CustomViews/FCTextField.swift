// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  FCTextField.swift
//  Flashcards
//
//  Created by Tharindu Perera on 4/12/21.
//

import UIKit

class FCTextField: UITextField {
    
    var rightImageView: UIImageView?
    let defaultImagePadding: CGFloat = 16
    var leftPadding: CGFloat = 16
    var rightPadding: CGFloat = 8
    var padding: UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: isLTR ? leftPadding : rightPadding, bottom: 8, right: isLTR ? rightPadding : leftPadding)
    }
    var canPerformAction = true
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initialize()
    }
    
    private func initialize() {
        textColor = Config.themeColors.primaryTextColor1
        tintColor = Config.themeColors.primaryTextColor1
        keyboardAppearance = Config.themeColors.keyboardAppearance
        
        backgroundColor = Config.themeColors.inputEnabledColor
        layer.cornerRadius = 5
        
        if let placeholder = self.placeholder {
            attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor: Config.themeColors.inputPlaceholderColor])
        }
        setInactive()
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return canPerformAction
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.rightViewRect(forBounds: bounds)
        rect.origin.x -= defaultImagePadding

        return rect
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.leftViewRect(forBounds: bounds)
        rect.origin.x += defaultImagePadding

        return rect
    }

}

// MARK: - Public Methods

extension FCTextField {
    
    func setActive() {
        layer.borderWidth = 1.25
        layer.borderColor = Config.themeColors.inputActiveBorderColor.cgColor
    }
    
    func setInactive() {
        layer.borderWidth = 1.25
        layer.borderColor = Config.themeColors.inputInactiveBorderColor.cgColor
    }
    
    func setErrorHighlighted() {
        layer.borderWidth = 2.25
        layer.borderColor = Config.themeColors.inputErrorBorderColor.cgColor
    }
    
    func setEnabled(isEnabled: Bool) {
        self.isEnabled = isEnabled
        layer.borderColor = isEnabled ? Config.themeColors.inputInactiveBorderColor.cgColor : Config.themeColors.inputDisabledColor.cgColor
        backgroundColor = isEnabled ? Config.themeColors.inputEnabledColor : Config.themeColors.inputDisabledColor
        textColor = isEnabled ? Config.themeColors.primaryTextColor1 : Config.themeColors.inputInactiveImageColor
        if let placeholder = self.placeholder {
            attributedPlaceholder = NSAttributedString(string: placeholder, attributes:
                                                        [NSAttributedString.Key.foregroundColor: isEnabled ? Config.themeColors.inputPlaceholderColor : Config.themeColors.inputInactiveImageColor])
        }
        rightImageView?.setImageTintColor(color: isEnabled ? Config.themeColors.inputActiveImageColor : Config.themeColors.inputInactiveImageColor)
    }
    
    func setUIPickerAsInput(_ picker: UIPickerView) {
        tintColor = .clear
        inputView = picker
        
        let dropDownIndicator = UIImage.dropDownIndicator
        rightImageView = UIImageView(image: dropDownIndicator)
        rightImageView!.contentMode = .scaleAspectFit
        rightViewMode = .always
        rightView = rightImageView!
        
        canPerformAction = false
        
        let dropDownIndicatorWidth = rightView!.frame.width + (isLTR ? padding.right : padding.left) + defaultImagePadding
        
        rightPadding = dropDownIndicatorWidth
        setNeedsDisplay()
    }
}
