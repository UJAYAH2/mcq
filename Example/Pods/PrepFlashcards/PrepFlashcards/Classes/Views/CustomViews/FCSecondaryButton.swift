// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  FCSecondaryButton.swift
//  PrepFlashcards
//
//  Created by Tharindu Perera on 4/27/21.
//

import UIKit

class FCSecondaryButton: UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    private func initialize() {
        clipsToBounds = true
        layer.cornerRadius = 4
        layer.borderWidth = 1.25
        setActive()
    }
}

// MARK: - Public Methods

extension FCSecondaryButton {
    
    func setActive() {
        isEnabled = true
        setTitleColor(Config.themeColors.secondaryActionActiveTitleColor, for: .normal)
        tintColor = Config.themeColors.secondaryActionActiveTitleColor
        backgroundColor = Config.themeColors.secondaryActionActiveBackgroundColor
        layer.borderColor = Config.themeColors.secondaryActionActiveBorderColor.cgColor
    }
    
    func setInactive(ignoreBackground: Bool = false) {
        isEnabled = false
        setTitleColor(Config.themeColors.secondaryActionInctiveTitleColor, for: .normal)
        tintColor = Config.themeColors.secondaryActionInctiveTitleColor
        if !ignoreBackground {
            backgroundColor = Config.themeColors.secondaryActionInactiveBackgroundColor
        }
        layer.borderColor = Config.themeColors.secondaryActionInactiveBorderColor.cgColor
    }
}
