// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  GAnalytics.swift
//  PrepFlashcards
//
//  Created by Nalinda Wickramarathna on 5/4/21.
//

import Foundation

enum EventCategory: String {
    case key = "event_category"
    case flashcard = "Studytools - Flashcard"
    case contentSelector = "Content Selector"
}

enum EventAction: String {
    case key = "event_action"
    case deckDelete = "Delete Deck"
    case deckCreate = "Create Deck"
    case deckEdit = "Edit Deck"
    case cardDelete = "Edit Card: Delete"
    case cardCreate = "Create Card"
    case cardsAddToDeck = "Add to Deck"
    case cardEdit = "Edit Card: Save"
    case cardCreateFlip = "Add Card: Flip"
    case cardEditFlip = "Edit Card: Flip"
    case newCardNewDeck = "New Flashcard in New Deck"
    case newCardExistingDeck = "New Flashcard in Existing Deck"
}

enum EventLabel: String {
    case key = "event_label"
    case delete = "Delete"
    case next = "Next"
    case save = "Save"
    case cardFlip = "Flip front and back"
    case saveAction = "Action: Save"
    case cancelAction = "Action: Cancel"
}

enum Dimensions: String {
    case deckID = "deck_id"
    case cardID = "card_id"
    case pageID = "page_id"
    case deckName = "deck_name"
    case highlightText = "highlight_text"
}

enum Path: String {
    case studyArea = "Create From : Study"
    case selectedText = "Create From : Selected Text"
}

enum DeckType {
    case NewDeck
    case ExistingDeck
}

enum SaveMethod: String {
    case saveClose = "Save Method : Save and Close"
    case addAnother = "Save Method : Add Another"
}

enum PageID: String {
    case deckCreate = "C0CFB1B1-1193-4089-86BD-9D02D3E51785"
    case deckEdit = "518864F8-0CA0-4980-A2FA-0946FD9265A5"
    case deckDelete = "E8DE05C1-90D8-498C-BC5C-ED14F428E755"
    case cardCreate = "1EAB60C2-E828-4C47-8A23-70504D4112F9"
    case cardCreateViaNote = "E303F364-6411-4116-BF80-ADA0507D5523"
    case cardEdit = "4F1F1D88-F3FC-4EDD-8D3F-61A5C6BA45F1"
    case cardDelete = "BD020DA0-EA91-48B8-B43A-A32560CC5AE5"
}

class GAnalytics {

    static func deckCreate(path: Path) -> NSDictionary {
        let analyticsData: NSDictionary = [EventCategory.key.rawValue: EventCategory.flashcard.rawValue,
                                           EventAction.key.rawValue: EventAction.deckCreate.rawValue,
                                           EventLabel.key.rawValue: path.rawValue,
                                           Dimensions.pageID.rawValue: PageID.deckCreate.rawValue];
        return analyticsData
    }
    
    static func deckEdit(deck: Deck) -> NSDictionary {
        let analyticsData: NSDictionary = [EventCategory.key.rawValue: EventCategory.flashcard.rawValue,
                                           EventAction.key.rawValue: EventAction.deckEdit.rawValue,
                                           EventLabel.key.rawValue: EventLabel.save.rawValue,
                                           Dimensions.deckID.rawValue: deck.id ?? "N/A",
                                           Dimensions.pageID.rawValue: PageID.deckEdit.rawValue];
        return analyticsData
    }
    
    static func deckDelete(deck: Deck) -> NSDictionary {
        let analyticsData: NSDictionary = [EventCategory.key.rawValue: EventCategory.flashcard.rawValue,
                                           EventAction.key.rawValue: EventAction.deckDelete.rawValue,
                                           EventLabel.key.rawValue: EventLabel.delete.rawValue,
                                           Dimensions.deckID.rawValue: deck.id ?? "N/A",
                                           Dimensions.pageID.rawValue: PageID.deckDelete.rawValue];
        return analyticsData
    }
    
    static func addCardsToExistingDeck(card: Card, saveMethod: SaveMethod) -> NSDictionary {
        let analyticsData: NSDictionary = [EventCategory.key.rawValue: EventCategory.flashcard.rawValue,
                                           EventAction.key.rawValue: EventAction.cardsAddToDeck.rawValue,
                                           EventLabel.key.rawValue: saveMethod.rawValue];
        return analyticsData
    }
    
    static func cardCreate() -> NSDictionary {
        let analyticsData: NSDictionary = [EventCategory.key.rawValue: EventCategory.flashcard.rawValue,
                                           EventAction.key.rawValue: EventAction.cardCreate.rawValue,
                                           EventLabel.key.rawValue: Path.studyArea.rawValue,
                                           Dimensions.pageID.rawValue: PageID.cardCreate.rawValue];
        return analyticsData
    }
    
    static func cardEdit(card: Card) -> NSDictionary {
        let analyticsData: NSDictionary = [EventCategory.key.rawValue: EventCategory.flashcard.rawValue,
                                           EventAction.key.rawValue: EventAction.cardEdit.rawValue,
                                           EventLabel.key.rawValue: EventLabel.save.rawValue,
                                           Dimensions.cardID.rawValue: card.id ?? "N/A",
                                           Dimensions.pageID.rawValue: PageID.cardEdit.rawValue];
        return analyticsData
    }
    
    static func cardDelete(card: Card) -> NSDictionary {
        let analyticsData: NSDictionary = [EventCategory.key.rawValue: EventCategory.flashcard.rawValue,
                                           EventAction.key.rawValue: EventAction.cardDelete.rawValue,
                                           EventLabel.key.rawValue: EventLabel.delete.rawValue,
                                           Dimensions.cardID.rawValue: card.id ?? "N/A",
                                           Dimensions.pageID.rawValue: PageID.cardDelete.rawValue];
        return analyticsData
    }
    
    static func cardCreateFlip() -> NSDictionary {
        let analyticsData: NSDictionary = [EventCategory.key.rawValue: EventCategory.flashcard.rawValue,
                                           EventAction.key.rawValue: EventAction.cardCreateFlip.rawValue,
                                           EventLabel.key.rawValue: EventLabel.cardFlip.rawValue,
                                           Dimensions.pageID.rawValue: PageID.cardCreate.rawValue];
        return analyticsData
    }
    
    static func cardEditFlip(card: Card) -> NSDictionary {
        let analyticsData: NSDictionary = [EventCategory.key.rawValue: EventCategory.flashcard.rawValue,
                                           EventAction.key.rawValue: EventAction.cardEditFlip.rawValue,
                                           EventLabel.key.rawValue: EventLabel.cardFlip.rawValue,
                                           Dimensions.cardID.rawValue: card.id ?? "N/A",
                                           Dimensions.pageID.rawValue: PageID.cardEdit.rawValue];
        return analyticsData
    }
    
    static func cardCreateViaContentSelector(deckType: DeckType, action: EventLabel, deck: Deck, highlightText: String?) -> NSDictionary {
        let analyticsData: NSDictionary = [EventCategory.key.rawValue: EventCategory.contentSelector.rawValue,
                                           EventAction.key.rawValue: deckType == .NewDeck ? EventAction.newCardNewDeck.rawValue : EventAction.newCardExistingDeck.rawValue,
                                           EventLabel.key.rawValue: action.rawValue,
                                           Dimensions.pageID.rawValue: PageID.cardCreateViaNote.rawValue,
                                           Dimensions.deckName.rawValue: deck.title ?? "N/A",
                                           Dimensions.highlightText.rawValue: highlightText ?? "N/A"];
        return analyticsData
    }
}
