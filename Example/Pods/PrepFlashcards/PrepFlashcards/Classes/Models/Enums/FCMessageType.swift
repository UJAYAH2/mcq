//
//  FCMessageType.swift
//  PrepFlashcards
//
//  Created by Tharindu Perera on 4/21/21.
//

import Foundation

enum FCMessageType {
    case success
    case error
}
