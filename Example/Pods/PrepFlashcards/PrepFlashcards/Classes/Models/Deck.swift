// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//
//  Deck.swift
//  Flashcards
//
//  Created by Tharindu Perera on 4/9/21.
//  Copyright © 2021 Flashcards. All rights reserved.
//

import Foundation

public class Deck: NSObject {
    public var id: String?
    public var title: String?
    
    public override init() {
        super.init()
    }
    
    init(title: String?) {
        self.title = title
    }
}
