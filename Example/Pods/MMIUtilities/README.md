# MMIUtilities

[![CI Status](https://img.shields.io/travis/Pradaban,Sugapriya/MMIUtilities.svg?style=flat)](https://travis-ci.org/Pradaban,Sugapriya/MMIUtilities)
[![Version](https://img.shields.io/cocoapods/v/MMIUtilities.svg?style=flat)](https://cocoapods.org/pods/MMIUtilities)
[![License](https://img.shields.io/cocoapods/l/MMIUtilities.svg?style=flat)](https://cocoapods.org/pods/MMIUtilities)
[![Platform](https://img.shields.io/cocoapods/p/MMIUtilities.svg?style=flat)](https://cocoapods.org/pods/MMIUtilities)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MMIUtilities is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MMIUtilities'
```

## Author

Pradaban,Sugapriya, sugapriya.pradaban@pearson.com

## License

MMIUtilities is available under the MIT license. See the LICENSE file for more info.
