/* PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
 
 *  Copyright © 2018 Pearson Education, Inc.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Pearson Education, Inc.  The intellectual and technical concepts contained
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
 * patent applications, and are protected by trade secret or copyright law.
 * Dissemination of this information, reproduction of this material, and copying or distribution of this software
 * is strictly forbidden unless prior written permission is obtained
 * from Pearson Education, Inc.*/

import Foundation
open class MMIUtilitiesKit: NSObject {
    
    open class var sharedInstance: MMIUtilitiesKit {
        struct Singleton {
            static let instance = MMIUtilitiesKit()
        }
        return Singleton.instance
    }
    
    public func fetchAttributedString(_ mathML: [Any], textFont: UIFont, textColor: UIColor, completion: @escaping (NSMutableAttributedString) -> ()) {
        MMIMathML.sharedInstance.fetchAttributedString(mathML, textFont: textFont, textColor: textColor, completion: { (content) in
            completion(content)
        })
    }
    
    public func fetchDefaultAttributedString(_ mathML: [Any], textFont: UIFont, textColor: UIColor, completion: @escaping (NSMutableAttributedString) -> ()) {
        MMIMathML.sharedInstance.fetchDefaultAttributedString(mathML, textFont: textFont, textColor: textColor, completion: { (content) in
            completion(content)
        })
    }
    
    public func launchAuthentication(controller: Any, url: String, redirectURLs: [String]) {
        DispatchQueue.main.async {
            let navVC = UINavigationController()
            let authVc = AuthController()
            authVc.url = url
            authVc.redirectURLs = redirectURLs
            authVc.delegate = controller as? AuthDelegate
            navVC.viewControllers = [authVc]
            UIApplication.shared.keyWindow?.rootViewController?.present(navVC, animated: true, completion: nil)
        }
        
    }
    
    public func constructTaggedString(termDef: TermDef) -> [Any] {
        return MMIMathML.sharedInstance.constructTaggedString(termDef: termDef)
    }
    public func getPodVersion(bundleId : String) -> String {
        if let version = Bundle(identifier: bundleId)?.infoDictionary?["CFBundleShortVersionString"] as? String {
           return version
        }
        return String()
    }
}
