/* PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
 
 *  Copyright © 2018 Pearson Education, Inc.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Pearson Education, Inc.  The intellectual and technical concepts contained
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
 * patent applications, and are protected by trade secret or copyright law.
 * Dissemination of this information, reproduction of this material, and copying or distribution of this software
 * is strictly forbidden unless prior written permission is obtained
 * from Pearson Education, Inc.*/
import Kingfisher

class MMIMathML: NSObject {
    
    class var sharedInstance: MMIMathML {
        struct Singleton {
            static let instance = MMIMathML()
        }
        return Singleton.instance
    }
    
    func fetchDefaultAttributedString(_ mathML: [Any], textFont: UIFont, textColor: UIColor, completion: (NSMutableAttributedString) -> ()) {
        let content = NSMutableAttributedString()
        var hasPrefixValue = false
        
        for item in mathML {
            if let text = item as? String {
                let trimmedString = text.trimmingCharacters(in: .whitespaces)
                for word in trimmedString {
                    if word == kDotSymbol {
                        hasPrefixValue = true
                    }
                }
                let attrStr = self.convertHtmlString(text: trimmedString, contentFont: textFont)
                content.append(attrStr)
            }
            if let imageData = item as? NSDictionary {
                let attString = self.configureHTML(htmlStr: self.getImageURLString(imageData: imageData), font: textFont)
                let space = NSAttributedString(string: " ")
                content.append(space)
                content.append(attString)
                attString.addAttributes([NSAttributedString.Key.font: textFont], range: NSRange.init(location: 0, length: attString.length))
                if hasPrefixValue != true {
                    content.append(space)
                }
                
            }
        }
        content.addAttributes([NSAttributedString.Key.foregroundColor: textColor], range: NSRange.init(location: 0, length: content.length))
        completion(content)
    }
    
    func fetchAttributedString(_ mathML: [Any], textFont: UIFont, textColor: UIColor, completion: @escaping (NSMutableAttributedString) -> ()) {
        let content = NSMutableAttributedString()
        var hasPrefixValue = false
        
        for item in mathML {
            if let text = item as? String {
               // let trimmedString = text
                for word in text {
                    if word == kDotSymbol {
                        hasPrefixValue = true
                    }
                }
                let attributedString = self.convertHtmlString(text: text, contentFont: textFont)
                content.append(attributedString)
            }else if let text = item as? NSAttributedString {
                
                let attachment: NSTextAttachment? = nil
                
                text.enumerateAttribute(NSAttributedString.Key.attachment,
                                               in:NSRange.init(location: 0, length: text.length),
                                                    options: NSAttributedString.EnumerationOptions.init(rawValue: 0),
                                                    using: { (value: Any?, range: NSRange, _: UnsafeMutablePointer<ObjCBool>) in

                                                        if let attachment: NSTextAttachment = value as? NSTextAttachment {
                                                            
                                                            var image:UIImage? = attachment.image
                                                            if image == nil {
                                                                image =
                                                                    attachment.image(forBounds: attachment.bounds, textContainer: nil, characterIndex: range.location)
                                                            }
                                                                if let image1 = image {
                                                                    if let data = image1.pngData() {
                                                                        image = UIImage.init(data: data)?.withRenderingMode(.alwaysTemplate)
                                                                        attachment.image = image
                                                                    }
                                                               // }
                                                            }
                                                        }
                })

                content.append(text)
            }
            if let imageData = item as? NSDictionary {
                
                let attString = NSMutableAttributedString()
               // let attString = self.configureHTML(htmlStr: self.getImageURLString(imageData: imageData), font: textFont)
                let attachment:NSTextAttachment = NSTextAttachment()
               // attachment.image = image
                
               // axtractedImageAttribute.append(attachmentString)
                let imageUrl = imageData["src"] as? String ?? ""
                if let tempURL = URL(string: imageUrl) {
                    let data = try? Data(contentsOf: tempURL)
                        if let imageData = data {
                            if let image = UIImage(data: imageData) {
                                attachment.image = image.withRenderingMode(.alwaysTemplate)
                                let imgHeight = image.size.height
                                let imgWidth = image.size.width
                                    //Ref: https://stackoverflow.com/questions/26105803/center-nstextattachment-image-next-to-single-line-uilabel
                                    let y = (textFont.capHeight - imgHeight).rounded() / 2
                                    attachment.bounds = CGRect(x: 0, y: y, width: imgWidth, height: imgHeight).integral
                                let attachmentString:NSAttributedString = NSAttributedString(attachment: attachment)
                                attString.append(attachmentString)
                                let space = NSAttributedString(string: " ")
                                attString.addAttributes([NSAttributedString.Key.font: textFont], range: NSRange.init(location: 0, length: attString.length))
                                
                                content.append(space)
                                content.append(attString)
                                if hasPrefixValue != true {
                                    content.append(space)
                                }
                                content.addAttributes([NSAttributedString.Key.foregroundColor: textColor], range: NSRange.init(location: 0, length: content.length))
                                completion(content)
                               
                               }
                        }
                }

            
            }
         else {
                content.addAttributes([NSAttributedString.Key.foregroundColor: textColor], range: NSRange.init(location: 0, length: content.length))
        completion(content)
        }
        }
    }
    
    func convertHtmlString(text : String, contentFont: UIFont) -> NSAttributedString {
        if text.contains("</") {
            _ = String(format:"<span style=\"font-family: '-apple-system', 'San Fransisco'; font-size: \(contentFont.pointSize) ;\">%@</span>", text)
            
            let attrStr = NSMutableAttributedString(attributedString: text.convertHtml())
            if !text.contains("<sub>") && !text.contains("<sup>") {
                attrStr.setFontFace(font: contentFont)
            }

            if contentFont.fontName.lowercased().contains("Semibold") || contentFont.fontName.lowercased().contains("bold") {
                attrStr.enumerateAttribute(NSAttributedString.Key.font, in: NSRange(location: 0, length: attrStr.length), options: NSAttributedString.EnumerationOptions.init(rawValue: 0)) { (value, range, _) -> Void in
                    if let val = value as? UIFont {
                       let italicTag = val.fontName.lowercased().contains("italic")
                        if italicTag {
                            attrStr.addAttributes([NSAttributedString.Key.font : val.boldItalics()], range: range)
                        }
                        else{
                            attrStr.addAttributes([NSAttributedString.Key.font : val.bold()], range: range)
                        }
                    }
                }
                return attrStr
            } else {
                return attrStr
            }
            
        } else {
            let attrStr = NSMutableAttributedString(attributedString: text.convertHtml())
            attrStr.enumerateAttribute(NSAttributedString.Key.font, in: NSRange(location: 0, length: attrStr.length), options: NSAttributedString.EnumerationOptions.init(rawValue: 0)) { (value, range, _) -> Void in
                if let _ = value as? UIFont {
                    attrStr.addAttributes([NSAttributedString.Key.font : contentFont], range: range)
                }
            }
            return attrStr
        }
    }
    
    func constructTaggedString(termDef: TermDef) -> [Any]{
        var sortedTerms:[Any] = []
        var termSemanticsMathML : [SemanticMathML] = []

        if termDef.textsemantics.count == 0, termDef.mathml.count == 0 {
            sortedTerms.append(termDef.text)
            }else{
                termSemanticsMathML = self.getTerms(itemTerm: termDef)
                let termArray = termSemanticsMathML.sorted { ($0.fallbackimage == nil ? $0.charStart : $0.charAt) < ($1.fallbackimage == nil ? $1.charStart : $1.charAt) }
                sortedTerms = self.updateTerms(sortedTerms: termArray, termText: termDef.text)
            }
        return sortedTerms
    }
    
    func updateTerms(sortedTerms: [SemanticMathML], termText: String) -> [Any]{
        var termArray:[Any] = []
        var rangeArray:[Any] = []
        
        var cursor = 0
        for i in 0..<sortedTerms.count {
            var previousStr: String = ""
            var newStr: String = ""
            
            if sortedTerms[i].fallbackimage == nil{
                
                if sortedTerms[i].charStart > cursor{
                    getPreviousSegment(termText, sortedTerms, i, &cursor, &previousStr, &rangeArray, &termArray, &newStr)
                }
                else{
                    createCurrentSegment(termText, sortedTerms, i, &rangeArray, &newStr, &termArray, &cursor)
                }
                createLastSegment(i, sortedTerms, cursor, termText, &termArray)
            }else{
                if sortedTerms[i].charAt > cursor{
                    let textCount = termText.count
                    let previousOffset = textCount - sortedTerms[i].charAt
                    let previousRange = self.getRange(termText:termText , startOffset: cursor, endOffset: previousOffset)
                    previousStr = String(termText[previousRange])
                    termArray.append(previousStr)
                    termArray.append(self.getMathML(sematicMathMl: sortedTerms[i]))
                }
                else{
                    termArray.append(self.getMathML(sematicMathMl: sortedTerms[i]))
                }
                cursor = sortedTerms[i].charAt
                createLastSegment(i, sortedTerms, cursor, termText, &termArray)
            }
        }
        var cnt = 0
        for term in termArray {
            if let nestedArray = term as? [String] {
                let appendedString = self.replaceOccurenceString(term: [term], nestedArray: nestedArray, cnt: cnt, rangeArray: rangeArray, termText: termText)
                termArray.remove(at: cnt)
                termArray.insert(appendedString, at: cnt)
            }
            cnt += 1
        }
        return termArray
    }
    
    fileprivate func createNestedTags(_ rangeArray: inout [Any], _ range: CountableClosedRange<Int>, _ newStr: inout String, _ termText: String, _ firstRange: Range<String.Index>, _ termArray: inout [Any], _ sortedTerms: [SemanticMathML], _ i: Int) {
        let index = rangeArray.count - 1
        if var obj = (rangeArray[index] as? [Any]) {
            if range.overlaps((obj[0] as? CountableClosedRange)!) {
                obj.append(range)
                rangeArray.remove(at: index)
                rangeArray.insert(obj, at: index)
                newStr = String(termText[firstRange])
                if var stringArr = termArray[index] as? [String] {
                    if let tags = HtmlTags(rawValue: sortedTerms[i].type){
                        stringArr.append(constructTags(text: newStr, tag: tags))
                    }
                    termArray.remove(at: index)
                    termArray.insert(stringArr, at: index)
                }
            } else {
                rangeArray.append(range)
                newStr = String(termText[firstRange])
                if let tags = HtmlTags(rawValue: sortedTerms[i].type){
                    termArray.append(constructTags(text: newStr, tag: tags))
                }
            }
        }
        else if range.overlaps((rangeArray[index] as? CountableClosedRange)!) {
            print("Tag Overlapped")
            var innerRangeArray:[Any] = []
            innerRangeArray.append(rangeArray[index])
            innerRangeArray.append(range)
            rangeArray.remove(at: index)
            rangeArray.append(innerRangeArray)
            newStr = String(termText[firstRange])
            
            var innerTermArray:[Any] = []
            //VWRS-5093 : revel crash issue
            if index < termArray.count {
                innerTermArray.append(termArray[index])

            
            if let tags = HtmlTags(rawValue: sortedTerms[i].type){
                innerTermArray.append(constructTags(text: newStr, tag: tags))
            }
            termArray.remove(at: index)
            termArray.append(innerTermArray)
            }
            
        } else {
            rangeArray.append(range)
            newStr = String(termText[firstRange])
            if let tags =  HtmlTags(rawValue: sortedTerms[i].type){
                termArray.append(constructTags(text: newStr, tag: tags))
            }
        }
    }
    
    fileprivate func createLastSegment(_ i: Int, _ sortedTerms: [SemanticMathML], _ cursor: Int, _ termText: String, _ termArray: inout [Any]) {
        if i + 1 == sortedTerms.count, cursor < termText.count{
            let firstRange = self.getLastRange(termText: termText, startOffset: cursor, endOffset: 0)
            let str = String(termText[firstRange])
            termArray.append(str)
        }
    }
    
    fileprivate func createCurrentSegment(_ termText: String, _ sortedTerms: [SemanticMathML], _ i: Int, _ rangeArray: inout [Any], _ newStr: inout String, _ termArray: inout [Any], _ cursor: inout Int) {
        let textCount = termText.count
        
        //VWRS-2773 Crash reported by revel: Range exception crash
        guard (textCount - 1) >= sortedTerms[i].charEnd &&  sortedTerms[i].charStart < sortedTerms[i].charEnd else {
            return
        }
        
        let endOffset = (textCount - 1) - sortedTerms[i].charEnd
        let firstRange = self.getRange(termText: termText, startOffset:sortedTerms[i].charStart , endOffset: endOffset)

        if sortedTerms[i].charStart <= sortedTerms[i].charEnd {
            let range = sortedTerms[i].charStart ... sortedTerms[i].charEnd
            if i >= 1{
                createNestedTags(&rangeArray, range, &newStr, termText, firstRange, &termArray, sortedTerms, i)
                
            } else {
                rangeArray.append(range)
                newStr = String(termText[firstRange])
                if let tags = HtmlTags(rawValue: sortedTerms[i].type){
                    termArray.append(constructTags(text: newStr, tag: tags))
                }
                cursor = sortedTerms[i].charEnd + 1
            }
        }
        
    }
    

    fileprivate func getPreviousSegment(_ termText: String, _ sortedTerms: [SemanticMathML], _ i: Int, _ cursor: inout Int, _ previousStr: inout String, _ rangeArray: inout [Any], _ termArray: inout [Any], _ newStr: inout String) {
        let textCount = termText.count
        if sortedTerms.indices.contains(i) {
            let previousOffset = textCount - sortedTerms[i].charStart
            let previousRange = self.getRange(termText: termText, startOffset: cursor, endOffset: previousOffset)
            previousStr = String(termText[previousRange])
            rangeArray.append(previousRange)
            termArray.append(previousStr)
            
            let endOffset = (textCount - 1) - sortedTerms[i].charEnd
            let firstRange = self.getRange(termText: termText, startOffset: sortedTerms[i].charStart, endOffset: endOffset)
            newStr = String(termText[firstRange])
            //VWRS-6709
            guard sortedTerms[i].charEnd > sortedTerms[i].charStart else {
            return
            }
            let range1 = sortedTerms[i].charStart ... sortedTerms[i].charEnd
            
            rangeArray.append(range1)
            if let tags = HtmlTags(rawValue: sortedTerms[i].type){
                termArray.append(constructTags(text: newStr, tag: tags))
            }
            cursor = sortedTerms[i].charEnd + 1
        }
        
    }
    
    func getMathML(sematicMathMl: SemanticMathML) ->  [String : String?]{
        let mathMl = ["alt": sematicMathMl.fallbackimage?.alttext, "height": sematicMathMl.fallbackimage?.height, "id": sematicMathMl.fallbackimage?.imageid, "src": sematicMathMl.fallbackimage?.path, "type": "image", "width": sematicMathMl.fallbackimage?.width]
        return mathMl
    }
    
    func getRange(termText: String, startOffset: Int, endOffset: Int ) -> Range<String.Index> {
        let firstRange = termText.index(termText.startIndex, offsetBy: startOffset)..<termText.index(termText.endIndex, offsetBy: -endOffset)
        return firstRange
    }
    
    func getLastRange(termText: String, startOffset: Int, endOffset: Int ) -> Range<String.Index> {
        let firstRange = termText.index(termText.startIndex, offsetBy: startOffset)..<termText.index(termText.endIndex, offsetBy: endOffset)
        return firstRange
    }
    
    func replaceOccurenceString(term: [Any], nestedArray: [String], cnt: Int, rangeArray: [Any], termText: String) -> String{
        
        //VWRS-2773 Crash reported by revel: Range exception crash
        //Possible crashes
        //1. Array out of bound
        //2. Nested array count should be greater than one
        //3. cnt should be less than rangeArray count
        var appendedString : String = ""
        guard (nestedArray.count > 1 && cnt < rangeArray.count) else {
            return appendedString
        }

        appendedString = nestedArray[0]
        var totalTagToSkipCount = self.dropStartTag(string: appendedString)
        var nextTagLowerBound = 0
        
        for i in 1 ... nestedArray.count - 1 {
            if let ranges = rangeArray[cnt] as? [CountableClosedRange<Int>] {
                
                //4. i should less than ranges count
                if i < ranges.count {
                    let startIndex = ranges[i].lowerBound
                    let offset = (ranges[i].upperBound - ranges[i].lowerBound ) + 1
                    let st = nestedArray[i]
                    let str = st.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
                    
                    if nestedArray.count - 1 > i{
                        nextTagLowerBound = ranges[i+1].lowerBound
                    }
                    let currentTagUpperBound = ranges[i].upperBound
                    let position3 = ranges[i].lowerBound
                    let parentTagPosition = ((rangeArray[cnt] as? [Any])?.first as? CountableClosedRange<Int>)?.lowerBound
                    let pos = (position3 - parentTagPosition!) + totalTagToSkipCount
                    let currentTagCount = self.dropStartTag(string: nestedArray[i] )
                    totalTagToSkipCount = totalTagToSkipCount + currentTagCount
                    
                    if nextTagLowerBound >= currentTagUpperBound{
                        let nextTagCount = self.dropEndTag(string: nestedArray[i] )
                        totalTagToSkipCount = totalTagToSkipCount + nextTagCount
                    }
                    
                    //VWRS-2773 Crash reported by revel: Range exception crash (Crash reproduced here)
                    let endOffset = ((pos + str.count) > str.count) ? str.count : (pos + str.count)
                    let position = (pos >= 0 && pos < str.count) ? pos : 0
                    //VWRS-4869:commit for the crash issue
                    if appendedString.count > endOffset {
                        let range = appendedString.index(appendedString.startIndex, offsetBy:position )..<appendedString.index(appendedString.startIndex, offsetBy: endOffset)
                        if let fromString = termText.substring(location: startIndex, length: offset){
                            appendedString = appendedString.replacingOccurrences(of: fromString, with: nestedArray[i] , options: .caseInsensitive , range:range )
                        }
                    }
                
                }
            }
        }
        return appendedString
    }
    
    func dropStartTag(string : String) -> Int {
        let diff =  (((string.count)
            - (string.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil).count)) / 2)
        return diff
    }
    
    func dropEndTag(string: String) -> Int {
        let diff = ((((string.count)
            - (string.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil).count)) / 2) + 1)
        return diff
    }
    
    func constructTags(text: String, tag: HtmlTags) -> String {
        
        var str  = text
        switch tag {
        case .strong:
            str = "<\(kStrongTag)>\(text)\("</\(kStrongTag)>")"
            break
        case .cite:
            str = "<\(kCiteTag)>\(text)\("</\(kCiteTag)>")"
            break
        case .emphasis:
            str = "<\(kEmphasisTag)>\(text)\("</\(kEmphasisTag)>")"
            break
        case .subscripts:
            str = "<\(kSubscriptsTag)>\(text)\("</\(kSubscriptsTag)>")"
            break
        case .superscript:
            str = "<\(kSuperscriptTag)>\(text)\("</\(kSuperscriptTag)>")"
            break
        case .superseded:
            str = "<\(kSupersededTag)>\(text)\("</\(kSupersededTag)>")"
            break
        }
        return str
    }

    
    func getTerms(itemTerm: TermDef) -> [SemanticMathML]{
        
        var semanticsMathML : [SemanticMathML] = []
        
        for i in 0..<itemTerm.textsemantics.count{
            let start = itemTerm.textsemantics[i].charStart
            let end = itemTerm.textsemantics[i].charEnd
            let type = itemTerm.textsemantics[i].type
            let semanctic = SemanticMathML.init(charStart: start, charEnd: end, type: type, charAt: 0, xml: "", fallbackimage: nil)
            semanticsMathML.append(semanctic)
        }
        for i in 0..<itemTerm.mathml.count{
            let charat = itemTerm.mathml[i].charAt
            let xml = itemTerm.mathml[i].xml
            let fallback = itemTerm.mathml[i].fallbackimage
            let semanctic = SemanticMathML.init(charStart:0, charEnd: 0, type: "", charAt: charat, xml: xml, fallbackimage: fallback)
            semanticsMathML.append(semanctic)
        }
        return semanticsMathML
    }
    
    func configureHTML(htmlStr: String, font: UIFont) -> NSMutableAttributedString {
        var attributeString = NSMutableAttributedString()
        do {
            let paragraphStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 8
            let convertedString = NSString(string: htmlStr)
            if let htmlData = convertedString.data(using: String.Encoding.utf8.rawValue) {
                attributeString = try NSMutableAttributedString(data: htmlData, options: [.documentType:
                    NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
                let attributeRange = NSRange(location: 0, length: attributeString.length)
                attributeString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: attributeRange)
                attributeString.addAttribute(NSAttributedString.Key.font, value: font, range: attributeRange)
            }
        } catch {
            print("invalid content")
        }
        return attributeString
    }
    
    func getImageURLString( imageData: NSDictionary) -> String {
        let contents = imageData
        var imageHeight: Int = 0
        var imageWidth: Int = 0
        guard let src = contents["src"] as? String else {
            print("invalid src")
            return ""
        }
        guard let id = contents["id"] as? String else {
            print("invalid id")
            return ""
        }
        
        if let height = contents["height"] as? String, let tempImageHeight = Int(height) {
            imageHeight = tempImageHeight
        } else if let height = contents["height"] as? Int {
            imageHeight = height
        }
        
        if let width = contents["width"] as? String, let tempImageWidth = Int(width) {
            imageWidth = tempImageWidth
        } else if let width = contents["width"] as? Int {
            imageWidth = width
        }
        
        let imageURLStr = "<img src=\"%@\" height=\"%@\" width=\"%@\" id =\"%@\">"
        return(String(format: imageURLStr, src, String(describing: imageHeight), String(describing: imageWidth), id))
    }
}
extension String {
    func substring(location: Int, length: Int) -> String? {
        guard self.count >= location + length else { return nil }
        let start = index(startIndex, offsetBy: location)
        let end = index(startIndex, offsetBy: location + length)
        let range = start..<end
        return String(self[range])//substring(with: start..<end)
    }
    
    func indicesOf(string: String) -> [Int] {
        var indices = [Int]()
        var searchStartIndex = self.startIndex
        
        while searchStartIndex < self.endIndex,
            let range = self.range(of: string, range: searchStartIndex..<self.endIndex),
            !range.isEmpty
        {
            let index = distance(from: self.startIndex, to: range.lowerBound)
            indices.append(index)
            searchStartIndex = range.upperBound
        }
        
        return indices
    }
}
extension StringProtocol where Index == String.Index {
    func index(of string: Self, options: String.CompareOptions = []) -> Index? {
        return range(of: string, options: options)?.lowerBound
    }
    func endIndex(of string: Self, options: String.CompareOptions = []) -> Index? {
        return range(of: string, options: options)?.upperBound
    }
    func indexes(of string: Self, options: String.CompareOptions = []) -> [Index] {
        var result: [Index] = []
        var start = startIndex
        while start < endIndex,
            let range = self[start..<endIndex].range(of: string, options: options) {
                result.append(range.lowerBound)
                start = range.lowerBound < range.upperBound ? range.upperBound :
                    index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return result
    }
    func ranges(of string: Self, options: String.CompareOptions = []) -> [Range<Index>] {
        var result: [Range<Index>] = []
        var start = startIndex
        while start < endIndex,
            let range = self[start..<endIndex].range(of: string, options: options) {
                result.append(range)
                start = range.lowerBound < range.upperBound ? range.upperBound :
                    index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return result
    }
}
extension String{
    func convertHtml() -> NSAttributedString{
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do{
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html,.characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }catch{
            return NSAttributedString()
        }
    }
}

extension NSMutableAttributedString {
    func setFontFace(font: UIFont, color: UIColor? = nil) {
        beginEditing()
        self.enumerateAttribute(.font, in: NSRange(location: 0, length: self.length)) { (value, range, stop) in
            if let f = value as? UIFont, let newFontDescriptor = f.fontDescriptor.withFamily(font.familyName).withSymbolicTraits(f.fontDescriptor.symbolicTraits) {
                let newFont = UIFont(descriptor: newFontDescriptor, size: font.pointSize)
                removeAttribute(.font, range: range)
                addAttribute(.font, value: newFont, range: range)
                if let color = color {
                    removeAttribute(.foregroundColor, range: range)
                    addAttribute(.foregroundColor, value: color, range: range)
                }
            }
        }
        endEditing()
    }
}
extension UIFont {
    
    func withTraits(_ traits: UIFontDescriptor.SymbolicTraits) -> UIFont {
        
        // create a new font descriptor with the given traits
        if let fd = fontDescriptor.withSymbolicTraits(traits) {
            // return a new font with the created font descriptor
            
            return UIFont(descriptor: fd, size: pointSize)
            //return UIFont(name: font.fontName, size: font.pointSize)!
        }
        
        // the given traits couldn't be applied, return self
        return self
    }
    
    func italics() -> UIFont {
        return withTraits(.traitItalic)
    }
    
    func bold() -> UIFont {
        return withTraits(.traitBold)
    }
    
    func boldItalics() -> UIFont {
        
        //return UIFont.systemFont(ofSize: 20, weight: UIFontWeightSemibold)
        return withTraits([ .traitBold, .traitItalic ])
    }
}
