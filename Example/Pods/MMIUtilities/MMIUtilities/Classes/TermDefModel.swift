/* PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
 
 *  Copyright © 2018 Pearson Education, Inc.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Pearson Education, Inc.  The intellectual and technical concepts contained
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
 * patent applications, and are protected by trade secret or copyright law.
 * Dissemination of this information, reproduction of this material, and copying or distribution of this software
 * is strictly forbidden unless prior written permission is obtained
 * from Pearson Education, Inc.*/

import Foundation

public struct NewTermDefModel {
    var type = ""
    var id = ""
    public var items: [Item] = []
    
    public init(response: [String: Any]) {
        self.type = response["type"] as? String ?? ""
        self.id = response["id"] as? String ?? ""
        let items = response["items"] as? [[String:Any]] ?? []
        for item in items {
            let newItem = Item(response: item)
            self.items.append(newItem)
        }
    }
}

public struct Item {
    var itemid = ""
    var schema = ""
    public var term: TermDef?
    public var definition: TermDef?
    
    public init(response: [String: Any]) {
        self.itemid = response["itemid"] as? String ?? ""
        self.schema = response["schema"] as? String ?? ""
        let newTerm = response["term"] as? [String:Any] ?? [:]
        self.term = TermDef(response: newTerm)
        let newdef = response["definition"] as? [String:Any] ?? [:]
        self.definition = TermDef(response: newdef)
    }
}

public struct TermDef {
    var schema = ""
    var text = ""
    var textsemantics:[Textsemantics] = []
    public var mathml: [MathML] = []
    
    public init(response: [String: Any]) {
        self.schema = response["schema"] as? String ?? ""
        self.text = response["text"] as? String ?? ""
        let textSes = response["textsemantics"] as? [[String: Any]] ?? []
        for textSe in textSes {
            let textSemantics = Textsemantics(textSeResponse: textSe)
            self.textsemantics.append(textSemantics)
        }
        let mathMls = response["mathml"] as? [[String: Any]] ?? []
        for mathMl in mathMls {
            let newMathMls = MathML(response: mathMl)
            self.mathml.append(newMathMls)
        }
    }
}



public struct SemanticMathML {
    var charStart = 0
    var charEnd = 0
    var type = ""
    var charAt = 0
    var xml = ""
    var fallbackimage: Fallbackimage?
}

public struct MathML {
    var charAt = 0
    var xml = ""
    var fallbackimage: Fallbackimage?
    
    public init(response: [String: Any]) {
        if let chartPosition = response["charAt"] as? Int {
            self.charAt = chartPosition
        }
        self.xml = response["xml"] as? String ?? ""
        let fallbackimage = response["fallbackimage"] as? [String: Any] ?? [:]
        self.fallbackimage = Fallbackimage(response: fallbackimage)
    }
}

struct Fallbackimage {
    var schema = ""
    var imageid = ""
    var path = ""
    var height = ""
    var width = ""
    var alttext = ""
    
    init(response: [String: Any]) {
        self.schema = response["schema"] as? String ?? ""
        self.imageid = response["imageid"] as? String ?? ""
        self.path = response["path"] as? String ?? ""
        self.height = response["height"] as? String ?? ""
        self.width = response["width"] as? String ?? ""
        self.alttext = response["alttext"] as? String ?? ""
    }
}

struct Textsemantics {
    var charStart = 0
    var charEnd = 0
    var type = ""
    
    init(textSeResponse: [String: Any]) {
        if let start = textSeResponse["charStart"] as? Int {
            self.charStart = start
        }
        if let end = textSeResponse["charEnd"] as? Int {
            self.charEnd = end
        }
        self.type = textSeResponse["type"] as? String ?? ""
    }
}

public enum HtmlTags: String {
    case strong = "strong"
    case superseded = "superseded"
    case emphasis = "emphasis"
    case superscript = "superscript"
    case subscripts = "subscript"
    case cite = "cite"
}
