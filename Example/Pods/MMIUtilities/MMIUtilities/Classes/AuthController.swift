/* PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
 
 *  Copyright © 2018 Pearson Education, Inc.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Pearson Education, Inc.  The intellectual and technical concepts contained
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
 * patent applications, and are protected by trade secret or copyright law.
 * Dissemination of this information, reproduction of this material, and copying or distribution of this software
 * is strictly forbidden unless prior written permission is obtained
 * from Pearson Education, Inc.*/

import UIKit
import WebKit

class AuthController: UIViewController, WKUIDelegate, WKNavigationDelegate  {
    var webView: WKWebView?
    var url: String?
    var redirectURLs: [String] = []
    var delegate: AuthDelegate?
    
    override func loadView() {
        super.loadView()
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView?.uiDelegate = self
        webView?.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Login"
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.webView?.contentMode = .scaleAspectFit
        if let url = URL(string: url ?? ""){
            let request = URLRequest(url: url)
            self.webView?.load(request)
        }
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        let contentSize: CGSize = webView.scrollView.contentSize
        let rw = self.view.frame.size.width / contentSize.width
        webView.scrollView.minimumZoomScale = rw
        webView.scrollView.zoomScale = rw
        if let redirectURL =  webView.url {
            if redirectURLs.contains( String(describing:redirectURL)) {
                if #available(iOS 11.0, *) {
                    WKWebsiteDataStore.default().httpCookieStore.getAllCookies { (cookies) in
                        for cookie in cookies {
                            if cookie.name == kSsoKey {
                                self.delegate?.onSuccess(with: cookie.value)
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    }
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        
    }
    
}
