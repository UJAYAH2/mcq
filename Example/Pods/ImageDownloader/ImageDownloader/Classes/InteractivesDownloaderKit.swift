/* PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
 
 *  Copyright © 2017 Pearson Education, Inc.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Pearson Education, Inc.  The intellectual and technical concepts contained
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
 * patent applications, and are protected by trade secret or copyright law.
 * Dissemination of this information, reproduction of this material, and copying or distribution of this software
 * is strictly forbidden unless prior written permission is obtained
 * from Pearson Education, Inc.*/

import UIKit

open class InteractivesDownloaderKit: NSObject {
    
    open class func downloadImages(_ content: NSArray, _ cacheUrl: String,_ bookId : String, completion: @escaping ([UIImage])->()) {
        InteractivesImageDownloader.sharedInstance.fetchImages(content, cacheUrl, bookId, completion: { (images) in
            completion(images)
        })
    }
    
    open class func downloadImage(_ content: String, _ cacheUrl: String = "",_ bookId : String = "", completion: @escaping (UIImage)->()) {
        InteractivesImageDownloader.sharedInstance.fetchImage(content, cacheUrl, bookId , completion: { (image) in
            completion(image)
        })
    }
    
    open class func downloadUCAImage(content: String,cacheUrl: String, folderName: String,_ bookId : String = "", completion: @escaping (UIImage)->()) {
        InteractivesImageDownloader.sharedInstance.fetchUCAImage(imageUrl: content,cacheUrl: cacheUrl, folderName: folderName, bookId, completion: { (image) in
            completion(image)
        })
    }
    
    open class func deleteFolderWithName(folderName: String) {
        InteractivesUtilities.sharedInstance.DeleteDirectoryWithName(folderName: folderName)
    }
    
    open class func downloadImageFromURLWithoutSavingInLocalPath(_ imageUrl: String, _ cacheUrl: String = "", completion: @escaping (UIImage)->()) {
        InteractivesImageDownloader.sharedInstance.downloadImageFromURLWithoutSavingInLocalPath(imageUrl, cacheUrl, completion: { (downloadedImage) in
            completion(downloadedImage)
        })
    }

}
