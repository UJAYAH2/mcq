/* PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
 
 *  Copyright © 2017 Pearson Education, Inc.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Pearson Education, Inc.  The intellectual and technical concepts contained
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
 * patent applications, and are protected by trade secret or copyright law.
 * Dissemination of this information, reproduction of this material, and copying or distribution of this software
 * is strictly forbidden unless prior written permission is obtained
 * from Pearson Education, Inc.*/

import UIKit

class InteractivesUtilities: NSObject {
    
    class var sharedInstance: InteractivesUtilities {
        struct Singleton {
            static let instance = InteractivesUtilities()
        }
        return Singleton.instance
    }
    
    func displayNetworkError(_ mainView: UIViewController) {
        let alertController: UIAlertController = UIAlertController(title: "Network Error", message: "No internet connection available. Check your internet connection and try again.", preferredStyle: .alert)
        let closeAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void in

            DispatchQueue.main.async(execute: { () -> Void in
                if #available(iOS 10.0, *) {
                    //
                } else {
                    if let appSettings = NSURL(string: UIApplication.openSettingsURLString) {
                        UIApplication.shared.openURL(appSettings as URL)
                    }
                }
            })
        }
        alertController.addAction(closeAction)
        mainView.present(alertController, animated: true, completion: nil)
    }
    
    func getImageLocalFilePathForFileName(_ imageName: String ,_ bookId : String = "") -> String {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = URL(fileURLWithPath: path)
        if bookId != "" {
             let filePath = url.appendingPathComponent("\(bookId)/" + imageName).path
            return filePath
        }
        let filePath = url.appendingPathComponent(imageName).path
        return filePath
    }
    
    func getUCAImageLocalFilePathForFileName(imageName: String, folderName: String,_ bookId : String = "") -> String {
        let path = self.createFolderWithName(folderName: folderName)
        let url = NSURL(fileURLWithPath: path)
        if bookId != "" {
            let filePath = (url.appendingPathComponent("\(bookId)/" + imageName)?.path)!
            return filePath
        }
        let filePath = url.appendingPathComponent(imageName)?.path
        return filePath!
    }
    
    func createFolderWithName(folderName: String) -> String {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let dataPath = documentsDirectory.appendingPathComponent(folderName)
        do {
            try FileManager.default.createDirectory(atPath: dataPath.absoluteString, withIntermediateDirectories: false, attributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return dataPath.absoluteString
    }

    
    func isImageAvailableInCache(_ imageName: String,_ bookId : String) -> Bool {
        let fileManager = FileManager.default
        let filePath = InteractivesUtilities.sharedInstance.getImageLocalFilePathForFileName(imageName,bookId)
        return fileManager.fileExists(atPath: filePath)
    }
    
    func isUCAImageAvailableInCache(imageName: String, folderName: String,_ bookId : String) -> Bool {
        let fileManager = FileManager.default
        let filePath = InteractivesUtilities.sharedInstance.getUCAImageLocalFilePathForFileName(imageName: imageName, folderName: folderName,bookId)
        return fileManager.fileExists(atPath: filePath)
    }
    
    func DeleteDirectoryWithName(folderName: String) {
        let fileManager = FileManager.default
        let path = self.createFolderWithName(folderName: folderName)
        do {
            try fileManager.removeItem(atPath: path)
        } catch let error {
            print("Error:", error)
        }
    }

    
    func saveImageToDevice(_ image: UIImage, _ cacheUrl: String, fileName: String, _ bookId : String) {
        var filePath = cacheUrl
        if cacheUrl == "" {
            filePath = InteractivesUtilities.sharedInstance.getImageLocalFilePathForFileName(fileName,bookId)
        }
        if let data = image.jpegData(compressionQuality: 0.8) {
            let imagePath = filePath.components(separatedBy: "?").first ?? cacheUrl
            try? data.write(to: URL(fileURLWithPath: imagePath), options: [.atomic])
        }
    }
    
    func saveUCAImageToDevice(image: UIImage, cacheUrl: String, fileName: String, folderName: String, _ bookId : String) {
        var filePath = cacheUrl
        if cacheUrl == "" {
            filePath = InteractivesUtilities.sharedInstance.getUCAImageLocalFilePathForFileName(imageName: fileName, folderName: folderName,bookId)
        }
        if let data = image.jpegData(compressionQuality: 0.8) {
            let imagePath = filePath
            try? data.write(to: URL(fileURLWithPath: imagePath), options: [.atomic])         }
    }

    
    func isValidURL(_ string: String?) -> Bool {
        guard let urlString = string else {return false}
        guard let url = URL(string: urlString) else {return false}
        if !UIApplication.shared.canOpenURL(url) {return false}
        let regEx = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/|%]((\\w)*|([0-9]*)|([-|_])*)+)+(/)?(\\?.*)?"
//        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[regEx])
//        return predicate.evaluate(with: string)
        return urlString.matches(pattern: regEx)

    }
}

extension String {
 func matches(pattern: String) -> Bool {
    var result = false
    do {
        if let regex = try? NSRegularExpression(
               pattern: pattern,
               options: [.caseInsensitive]) {
            result = regex.firstMatch(
            in: self,
            options: [],
            range: NSRange(location: 0, length: utf16.count)) != nil
        }
    } catch {
        print(error)
    }
    return result
   
}
}
