/* PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
 
 *  Copyright © 2017 Pearson Education, Inc.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Pearson Education, Inc.  The intellectual and technical concepts contained
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
 * patent applications, and are protected by trade secret or copyright law.
 * Dissemination of this information, reproduction of this material, and copying or distribution of this software
 * is strictly forbidden unless prior written permission is obtained
 * from Pearson Education, Inc.*/

import UIKit
import Kingfisher

class InteractivesImageDownloader: NSObject {
    
    open class var sharedInstance: InteractivesImageDownloader {
        struct Singleton {
            static let instance = InteractivesImageDownloader()
        }
        return Singleton.instance
    }
    
    func fetchImages(_ imagesArray: NSArray, _ cacheUrl: String,_ bookId : String, completion: @escaping ([UIImage])->()) {
        let group = DispatchGroup()
        var images = [UIImage]()
        for content in (imagesArray as? [[String:Any]])! {
            if let imageUrl = content["imageURLValue"] as? String {
                var imageURL = imageUrl
                if imageUrl.hasPrefix("http://") {
                    imageURL = imageUrl.replacingOccurrences(of: "http://", with: "https://")
                }
                if InteractivesUtilities.sharedInstance.isValidURL(imageUrl) {
                    let imageName = (imageURL as NSString).lastPathComponent
                    if InteractivesUtilities.sharedInstance.isImageAvailableInCache(imageName, bookId) {
                        let imageFilePath = InteractivesUtilities.sharedInstance.getImageLocalFilePathForFileName(imageName,bookId)
                        let image = self.fetchImageFromURL(imageFilePath)
                        if image != nil {
                            images.append(image!)
                        }
                    } else {
                        group.enter()
                        self.downloadImageFromURL(imageURL, cacheUrl, bookId, completion: { (downloadedImage) in
                            InteractivesUtilities.sharedInstance.saveImageToDevice(downloadedImage, cacheUrl, fileName: imageName, bookId)
                            images.append(downloadedImage)
                            group.leave()
                        })
                    }
                } else {
                    let image = self.fetchImageFromURL(imageURL)
                    if image != nil {
                        images.append(image!)
                    }
                }
            }
        }
        DispatchGroup().notify(queue: DispatchQueue.main) {
            if images.count>0 {
                completion(images)
            }
        }
    }
    
    func fetchImage(_ imageUrl: String, _ cacheUrl: String,_ bookId : String, completion: @escaping (UIImage)->()) {
        var imageURL = imageUrl
        if imageUrl.hasPrefix("http://") {
            imageURL = imageUrl.replacingOccurrences(of: "http://", with: "https://")
        }
        if InteractivesUtilities.sharedInstance.isValidURL(imageUrl) {
       
            let imageName = (imageURL as NSString).lastPathComponent
            if InteractivesUtilities.sharedInstance.isImageAvailableInCache(imageName, bookId) {
                let imageFilePath = InteractivesUtilities.sharedInstance.getImageLocalFilePathForFileName(imageName,bookId)
                if let image = self.fetchImageFromURL(imageFilePath) {
                    completion(image)
                }
             } else {
                self.downloadImageFromURL(imageURL, cacheUrl, bookId, completion: { (downloadedImage) in
                    InteractivesUtilities.sharedInstance.saveImageToDevice(downloadedImage, cacheUrl, fileName: imageName, bookId)
                        completion(downloadedImage)
                })
            }
        } else {
            if let image = self.fetchImageFromURL(imageURL) {
                completion(image)
            }
        }
    }
    
    func fetchUCAImage(imageUrl: String, cacheUrl: String, folderName: String,_ bookId : String, completion: @escaping (UIImage)->()) {
        var imageURL = imageUrl
        if imageUrl.hasPrefix("http://") {
            imageURL = imageUrl.replacingOccurrences(of: "http://", with: "https://")
        }
        if InteractivesUtilities.sharedInstance.isValidURL(imageURL) {
            let imageName = (imageURL as NSString).lastPathComponent
            if InteractivesUtilities.sharedInstance.isUCAImageAvailableInCache(imageName: imageName, folderName: folderName, bookId) {
                let imageFilePath = InteractivesUtilities.sharedInstance.getUCAImageLocalFilePathForFileName(imageName: imageName, folderName: folderName)
                if let image = self.fetchImageFromURL(imageFilePath) {
                    completion(image)
                }
            } else {
                self.downloadUCAImageFromURL(imageUrl: imageURL, cacheUrl: cacheUrl, bookId, folderName: folderName, completion: { (downloadedImage) in
                    InteractivesUtilities.sharedInstance.saveUCAImageToDevice(image: downloadedImage, cacheUrl: cacheUrl, fileName: imageName, folderName: folderName, bookId)
                    completion(downloadedImage)
                })
            }
        } else {
            if let image = self.fetchImageFromURL(imageURL) {
                completion(image)
            }
        }
    }

    
    func downloadImageFromURL(_ imageUrl: String, _ cacheUrl: String,_ bookId : String, completion: @escaping (UIImage)->()) {
        var imageURL = imageUrl
        if imageUrl.hasPrefix("http://") {
            imageURL = imageUrl.replacingOccurrences(of: "http://", with: "https://")
        }
        let imageName = (imageURL as NSString).lastPathComponent
        if let tempURL = URL.init(string: imageURL) {
            let downloader: ImageDownloader! = ImageDownloader(name: "downloadImageFromURL")
            downloader.downloadImage(with: tempURL as URL, options: nil, progressBlock: { (receivedSize, totalSize) -> () in
                }, completionHandler: { (image, error, imageURL, data) -> () in
                    if image != nil {
                        InteractivesUtilities.sharedInstance.saveImageToDevice(image!, cacheUrl, fileName: imageName, bookId)
                        completion(image!)
                    }
            })
        }
    }
    
    func downloadImageFromURLWithoutSavingInLocalPath(_ imageUrl: String, _ cacheUrl: String, completion: @escaping (UIImage)->()) {
        var imageURL = imageUrl
        if imageUrl.hasPrefix("http://") {
            imageURL = imageUrl.replacingOccurrences(of: "http://", with: "https://")
        }
        if let tempURL = URL.init(string: imageURL) {
            let downloader: ImageDownloader! = ImageDownloader(name: "downloadImageFromURL")
            downloader.downloadImage(with: tempURL as URL, options: nil, progressBlock: { (receivedSize, totalSize) -> () in
            }, completionHandler: { (image, error, imageURL, data) -> () in
                if let downloadedImage = image {
                     completion(downloadedImage)
                }
            })
        }
    }
    
    func downloadUCAImageFromURL(imageUrl: String, cacheUrl: String,_ bookId : String, folderName: String, completion: @escaping (UIImage)->()) {
        var imageURL = imageUrl
        if imageUrl.hasPrefix("http://") {
            imageURL = imageUrl.replacingOccurrences(of: "http://", with: "https://")
        }
        let imageName = (imageURL as NSString).lastPathComponent
        if let tempURL = NSURL.init(string: imageURL) {
            let downloader = ImageDownloader(name: "downloadImageFromURL")
            downloader.downloadImage(with:tempURL as URL, options: nil, progressBlock: { (receivedSize, totalSize) -> () in
                }, completionHandler: { (image, error, imageURL, data) -> () in
                    if image != nil {
                        InteractivesUtilities.sharedInstance.saveUCAImageToDevice(image: image!, cacheUrl: cacheUrl, fileName: imageName, folderName: folderName, bookId)
                        completion(image!)
                    } else {
                        completion(UIImage())
                    }
            })
        }
    }

    func fetchImageFromURL(_ imageUrl: String) -> UIImage? {
        let contentImage = UIImage(contentsOfFile: imageUrl)
        if contentImage == nil {
            print("Image is not available in the location: \(imageUrl)")
            return nil
        }
        return contentImage
    }
}
