## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ImageDownloader is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ImageDownloader", :git => 'ssh://git@bitbucket.pearson.com/mobi/image-downloader.git', :branch => 'swift3'
```

## Usage
The image downloader pod is used to download an image or a set of images.

let imageURLValue = "https://images.contentful.com/7te8ye58mdt7/4HU01dAIEM2CSuysqeA2qK/2d9f395b6ad25f8216cc0737f2b9bc6d/14328353201184332706.jpg"

InteractivesDownloaderKit.downloadImage(imageURLValue) { (image) in

    DispatchQueue.main.async(execute: { () -> Void in

        self.imageViewerObj.image = image

    })

}


## License
PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
