// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//

import Foundation
import MCQ

protocol DataService
{
    func fetchSampleCard(completion: (MCQModel) -> Void)
}

class AppDataService: DataService
{
    func fetchSampleCard(completion: (MCQModel) -> Void)
    {
        let termDef = MCQTermDefModel()
        termDef.id = "61b8747e5fe1924d1db3ca42"
        termDef.definition = ["Question 1"]
        termDef.term = ["Answer 2"]
        termDef.saved = false
        termDef.type = "TEXT"
        termDef.choices = [["Answer 1"],["Answer 2"],["Answer 3"]]
        termDef.isInCurrentSession = false
        termDef.questionMedia = "TEXT"
        termDef.archived = false
        termDef.favourite = false
        
        let answer = MCQAnswer()
        answer.correct = true
        answer.id = 1
        answer.type = "TEXT"
        answer.value = "Answer 2"
        
        termDef.answers = [answer]
        
        
        let termDef_2 = MCQTermDefModel()
        termDef_2.id = "61b8747e5fe1924d1db3ca42"
        termDef_2.definition = ["Question 2"]
        termDef_2.term = ["Answer 3"]
        termDef_2.saved = false
        termDef_2.type = "TEXT"
        termDef_2.choices = [["Answer 1"],["Answer 2"],["Answer 3"]]
        termDef_2.isInCurrentSession = false
        termDef_2.questionMedia = "TEXT"
        termDef_2.archived = false
        termDef_2.favourite = false
        
        let answer_2 = MCQAnswer()
        answer_2.correct = true
        answer_2.id = 1
        answer_2.type = "TEXT"
        answer_2.value = "Answer 3"
        
        termDef_2.answers = [answer_2]
        
        
        let termDef_3 = MCQTermDefModel()
        termDef_3.id = "61b8747e5fe1924d1db3ca42"
        termDef_3.definition = ["Question 3"]
        termDef_3.term = ["Answer 3"]
        termDef_3.saved = false
        termDef_3.type = "TEXT"
        termDef_3.choices = [["Answer 1"],["Answer 2"],["Answer 3"]]
        termDef_3.isInCurrentSession = false
        termDef_3.questionMedia = "TEXT"
        termDef_3.archived = false
        termDef_3.favourite = false
        
        let answer_3 = MCQAnswer()
        answer_3.correct = true
        answer_3.id = 1
        answer_3.type = "TEXT"
        answer_3.value = "Answer 3"
        
        termDef_3.answers = [answer_3]
        
        let sample = MCQModel()
        sample.termDefSet = [termDef, termDef_2, termDef_3]
        
        completion(sample)
    }
}
