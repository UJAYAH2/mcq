// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//

import UIKit
import MCQ

class ViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    
    var viewModel = ViewController.ViewModel()
//    var mcq: MCQ?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.fetchCards()
    }
}

extension ViewController
{
    @IBAction func launchTapped(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.launchMCQ()
        }
    }
    
    func launchMCQ()
    {
        DispatchQueue.main.async { [self] in
            self.viewModel.index = 0
            if let card = self.viewModel.card {
//                MCQ(parent: self, card: card, index: self.viewModel.index - 1, isLastItem: self.viewModel.isLastItem()).load(to: self.containerView)
                
                MCQ(parent: self, card: card, index: self.viewModel.index - 1, isLastItem: self.viewModel.isLastItem()).load() { viewController in
                    loadToContainer(viewController)
                }
            }
        }
    }
    
    func loadToContainer(_ viewController: MCQViewController) {
        if let mcqlist = viewController as? MCQListController {
            if let subview = mcqlist.view {
                self.containerView.removeAllSubViews()
                self.containerView.addSubview(subview)
                self.addChild(mcqlist)
                mcqlist.didMove(toParent: self)
                subview.anchor(top: self.containerView.topAnchor, left: self.containerView.leftAnchor, bottom: self.containerView.bottomAnchor, right: self.containerView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: nil, height: nil, centerX: nil, centerY: nil)
            }
        } else if let mcqWeb = viewController as? MCQWebCardVC {
            if let subview = mcqWeb.view {
                self.containerView.removeAllSubViews()
                self.containerView.addSubview(subview)
                self.addChild(mcqWeb)
                mcqWeb.didMove(toParent: self)
                subview.anchor(top: self.containerView.topAnchor, left: self.containerView.leftAnchor, bottom: self.containerView.bottomAnchor, right: self.containerView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: nil, height: nil, centerX: nil, centerY: nil)
            }
        }
    }
}

extension UIView
{
    func removeAllSubViews() {
        for view in self.subviews {
            DispatchQueue.main.async {
                view.removeFromSuperview()
            }
        }
    }
}

