// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//

import Foundation
import MCQ

extension ViewController
{
    class ViewModel
    {
        private let dataService: DataService
        var card: MCQModel?
        var index = 0
        
        init(dataService: DataService = AppDataService()) {
            self.dataService = dataService
        }
        
        func fetchCards() {
            dataService.fetchSampleCard() { card in
                self.card = card
            }
        }
        
        func isLastItem() -> Bool {
            if let card = card {
                return index == (card.termDefSet.count - 1) ? true : false
            }
            
            return false
        }
    }
}
