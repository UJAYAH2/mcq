// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

