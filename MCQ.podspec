#
# Be sure to run `pod lib lint MCQ.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'MCQ'
  s.version          = '0.0.1'
  s.summary          = 'A short description of MCQ.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/UJAYAH2/mcq'
  s.license          = { :type => 'PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA', :file => 'LICENSE' }
  s.author           = 'Pearson Inc.'
  s.source           = { :git => 'git@gitlab.com:UJAYAH2/mcq.git', :tag => s.version.to_s }

  s.ios.deployment_target = '11.0'

  s.source_files = 'MCQ/Classes/**/*'

  s.resource_bundles = {
    'MCQ' => ['MCQ/**/*.{storyboard,xib,xcassets,json,png,lproj,plist,ttf,otf}']
  }

  s.dependency 'MMIUtilities', '~> 4.3.10'
  s.dependency 'PrepFlashcards', '0.4.7'

end
