/*PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA

*  Copyright © 2021 Pearson Education, Inc.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Pearson Education, Inc.  The intellectual and technical concepts contained
* herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
* patent applications, and are protected by trade secret or copyright law.
* Dissemination of this information, reproduction of this material, and copying or distribution of this software
* is strictly forbidden unless prior written permission is obtained
* from Pearson Education, Inc.
*/

import UIKit
import MMIUtilities

typealias MarkedAnswerState = (isCorrectAnswer: Bool, isCorrectAttempt: Bool, index: Int)

public class MCQListController: MCQViewController {

    @IBOutlet weak public var tableView: UITableView!
    var navBarLabel: UILabel?
    weak var mcqDelegate: McqDelegate?
    var analyticsValue : [String : Any] = [:]
    var mcqStudy = MCQModel()
    var isAnimatedToNextPage = false
    var mcq: MCQTermDefModel = MCQTermDefModel()
    var mcqChoices: [NSMutableAttributedString] = []
    var mcqAccessibilityString: [String] = []
    var isSelectedAnswerCorrect = false
    var startTime = ""
    var endTime = ""
    var pageId = ""
    var isLastItem = false
    var correctAnswerState: MarkedAnswerState? = nil
    var incorrectAnswerState: MarkedAnswerState? = nil

    public override func viewDidLoad() {
        super.viewDidLoad()
        self.pageId = UUID().uuidString
        self.startTime = MCQUtilities.sharedInstance.getCurrentTimeStamp()
        self.tableView.register(MCQNextButtonTableViewCell.self, forCellReuseIdentifier: "StudyMCQNextButtonTableViewCell")
        self.tableView.estimatedRowHeight = 60.0
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.sectionHeaderHeight = 0.0
        self.navigationController?.isNavigationBarHidden = false
        
        // **************
//        StudyUtilities.sharedInstance.setBarButtonAccessibility(barButton: self.navigationItem.leftBarButtonItem, identifier: StudyGuideConstants.kCloseButtonAccessibility.localized, value: StudyGuideConstants.kCloseButtonAccessibilityId.localized)
        
        MCQUtilities.sharedInstance.setBarButtonAccessibility(barButton: self.navigationItem.leftBarButtonItem, identifier: MCQGuideConstants.kCloseButtonAccessibility, value: MCQGuideConstants.kCloseButtonAccessibilityId)
        
        // **************
        self.rotated()
        NotificationCenter.default.addObserver(self, selector: #selector(rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadContentView), name: UIContentSizeCategory.didChangeNotification, object: nil)
        self.view.backgroundColor = MCQRootModel.shared.themeColors.primaryBackgroundColor
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.startTime = MCQUtilities.sharedInstance.getCurrentTimeStamp()
        self.mcqStudy = MCQRootModel.shared.mcqModel.mcq
        self.mcq = MCQRootModel.shared.mcqModel.mcq.termDefSet[self.mcqStudy.lastVisitedPageIndex]
        self.updateChoices()
        self.resetValues()
        let analyticsValue = [MCQGuideConstants.EventCategory: MCQGuideConstants.StudyToolsMCQ, MCQGuideConstants.EventLabel: "Open MCQ", MCQGuideConstants.EventAction: "Open", MCQGuideConstants.PageName: MCQGuideConstants.StudyToolsMCQ, MCQGuideConstants.PageId: self.pageId, MCQGuideConstants.QuestionId: self.mcqStudy.id, MCQGuideConstants.EventValue: MCQRootModel.shared.summaryMastery] as [String : Any]
        self.mcqDelegate?.didLoadMCQ(analyticsValue)
    }

    func resetValues() {
        self.tableView.isUserInteractionEnabled = true
        self.isAnimatedToNextPage = false
        correctAnswerState = nil
        incorrectAnswerState = nil
        if let answerCells = self.tableView.visibleCells.filter ({ $0.isKind(of: MCQAnswerCell.self) }) as? [MCQAnswerCell] {
            let _ = answerCells.map({ $0.isUserInteractionEnabled = true })
        }
    }

    func updateChoices() {
        self.mcqChoices.removeAll()
        self.mcqAccessibilityString.removeAll()
        if MCQRootModel.shared.deckType == .Expert {
            for ans in mcq.choices {
                if let choice = ans as? String {
                    let nsMutableString = NSMutableAttributedString(string: choice)
                    self.mcqAccessibilityString.append(choice)
                    self.mcqChoices.append(nsMutableString)
                }
            }
        } else {
            if let modelChoices = mcq.choices as? [[Any]] {
                for (index, item) in modelChoices.enumerated() {
                    MMIUtilitiesKit.sharedInstance.fetchAttributedString(item , textFont: UIFont(name: MCQFont.HindRegular, size: 16) ?? UIFont.systemFont(ofSize: 16, weight: .regular), textColor: MCQRootModel.shared.themeColors.primaryTitleTextColor, completion: {[weak self] (content) in
                        if (self?.mcqChoices.count ?? 0) == index {
                            self?.mcqChoices.append(content)
                        } else {
                            self?.mcqChoices[index] = content
                        }
                        MCQUtilities.sharedInstance.configureAccessibilityString(item , completion: {[unowned self] (accessibilityString) in
                            self?.mcqAccessibilityString.append(accessibilityString)
                        })
                    })
                }
            }
        }
        self.tableView.reloadData()

    }

    //MARK:IBAction Methods
    @IBAction func didTapCloseIcon(sender: AnyObject) {
//        self.analyticsValue = ["Title": self.mcqStudy.title, "ID": self.mcqStudy.id, "Widget_Context_ID": self.mcqStudy.id, "Widget_Context_Name": "StudyTools","componentName" : StudyGuideConstants.Study,"componentId" : self.mcqStudy.id, "componentVersion" : StudyKit.podVersion]
        let analyticsValue = [MCQGuideConstants.EventCategory: MCQGuideConstants.StudyToolsMCQ, MCQGuideConstants.EventLabel: "Close MCQ", MCQGuideConstants.EventAction: "Close MCQ", MCQGuideConstants.EventValue: MCQRootModel.shared.summaryMastery, MCQGuideConstants.PageId: self.pageId, MCQGuideConstants.QuestionId: self.mcqStudy.id] as [String : Any]
        self.mcqDelegate?.didDismissMCQ(analyticsValue)
        NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIContentSizeCategory.didChangeNotification, object: nil)
    }

    @objc func reloadContentView() {
        self.tableView.reloadData()
    }

    func verifySelectedAnswer(_ indexPath: IndexPath) -> Bool {
        var isSelectedAnswerCorrect = false
        if MCQRootModel.shared.deckType == .Expert, let selectedTerm = self.mcq.choices[indexPath.row - 1] as?  String {
                isSelectedAnswerCorrect = self.mcq.answers?.filter({$0.correct == true}).first?.value == selectedTerm
        } else {
            if let selectedTermArray = self.mcq.choices[indexPath.row - 1] as?  [Any] {
                var selectedTermId = ""
                if let textTerm = selectedTermArray.first as? String {
                    selectedTermId = textTerm
                } else if let dictTerm = selectedTermArray.first as? NSDictionary {
                    if let src = dictTerm["src"] as? String {
                        selectedTermId = src
                    }
                }
                if let mcqTextTerm = mcq.term.first as? String {
                    isSelectedAnswerCorrect = (selectedTermId == mcqTextTerm)
                } else if let mcqDictTerm = mcq.term.first as? NSDictionary {
                    if let src = mcqDictTerm["src"] as? String {
                        isSelectedAnswerCorrect = (selectedTermId == src)
                    }
                }
            }
        }
        return isSelectedAnswerCorrect
    }

    func getCorrectAnsIndex() -> Int {
        var correctAnsIndex = -1
        if MCQRootModel.shared.deckType == .Expert {
            if let answers = mcq.answers as? [MCQAnswer] {
                for ans in answers {
                    if ans.correct == true {
                        if let index = self.mcqAccessibilityString.firstIndex(of: ans.value) {
                            correctAnsIndex = index
                        }
                    }
                }
            }
        } else {
            MCQUtilities.sharedInstance.configureAccessibilityString(mcq.term, completion: {[unowned self] (accessString) in
                if let index = self.mcqAccessibilityString.firstIndex(of: accessString) {
                    correctAnsIndex = index
                }
            })
        }

        return correctAnsIndex
    }

    @objc func rotated() {
        tableView.reloadData()
    }

    func displayCorrectAnswerCell(_ tempCell: MCQAnswerCell,_ incorrectCell: MCQAnswerCell) {
        tempCell.contentView.alpha = 1.0
        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
            tempCell.updateCorrectOrIncorrectAnswer(isCorrect: true, isCorrectAttempt: false, completion: { [unowned self] in
                DispatchQueue.main.async {
                    if self.isAnimatedToNextPage == false {
                        self.isAnimatedToNextPage = true
//                        let reloadIndex = IndexPath(row: self.mcqChoices.count + 1, section: 0)
//                        self.tableView.reloadRows(at: [reloadIndex], with: .automatic)
                        self.tableView.reloadData()
                    }
                }
            })
        }
    }

    func configureSelectedAnswer(_ cell: MCQAnswerCell, isSelectedAnswerCorrect: Bool, correctAnsIndex: Int) {
        MCQRootModel.shared.isAnimationStarted = false
        
        if isSelectedAnswerCorrect == false {
            incorrectAnswerState = (false, false, cell.indexPath.row)
            correctAnswerState = (true, false, correctAnsIndex + 1) // + 1 after for the qusetion
            cell.updateCorrectOrIncorrectAnswer(isCorrect: isSelectedAnswerCorrect, isCorrectAttempt: false, completion: { [unowned self] in
                if let answerCells = self.tableView.visibleCells.filter ({ $0.isKind(of: MCQAnswerCell.self) }) as? [MCQAnswerCell] {
                    if answerCells.count > correctAnsIndex {
                        let correctAnsCell = answerCells[correctAnsIndex]
                        self.displayCorrectAnswerCell(correctAnsCell, cell)
                    } else {
                        DispatchQueue.main.async {
                            if self.isAnimatedToNextPage == false {
                                self.isAnimatedToNextPage = true
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
            })

        } else {
            incorrectAnswerState = nil
            correctAnswerState = (true, true, cell.indexPath.row)
            cell.updateCorrectOrIncorrectAnswer(isCorrect: isSelectedAnswerCorrect, isCorrectAttempt: true, completion: { [unowned self] in
                DispatchQueue.main.async() {
                    self.isAnimatedToNextPage = true
                    let reloadIndex = IndexPath(row: self.mcqChoices.count + 1, section: 0)
                    self.tableView.reloadRows(at: [reloadIndex], with: .automatic)
                }
            })
        }
        let analyticsValue = [MCQGuideConstants.EventCategory: MCQGuideConstants.StudyToolsMCQ, MCQGuideConstants.EventLabel: "Attempt MCQ", MCQGuideConstants.EventAction: "Attempt MCQ", MCQGuideConstants.EventValue: MCQRootModel.shared.summaryMastery, MCQGuideConstants.PageId: self.pageId, MCQGuideConstants.QuestionId: self.mcqStudy.id] as [String : Any]
        self.mcqDelegate?.didAttemptMCQ(analyticsValue)
    }

    @objc fileprivate func configuresFocusChange() {
        if !self.isSelectedAnswerCorrect {
            if let answerCells = self.tableView.visibleCells.filter ({ $0.isKind(of: MCQAnswerCell.self) }) as? [MCQAnswerCell] {
                let correctAnsIndex = self.getCorrectAnsIndex()
                guard answerCells.count > correctAnsIndex else { return }
                let crctCell = answerCells[correctAnsIndex]
                UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: crctCell)
            }
        }
    }

    @objc func configureAnnouncement() {
        let accessString = self.isSelectedAnswerCorrect ? "Correct Answer" : "Incorrect Answer and Correct answer is"
        UIAccessibility.post(notification: UIAccessibility.Notification.announcement, argument: accessString)
        Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.configuresFocusChange), userInfo: nil, repeats: false)
    }

    func configureSelectedCell(_ cell: MCQAnswerCell, attemptStatus: (Bool, Bool), isSelectedAnswerCorrect: Bool) {
        let isAttemptIncorrect = attemptStatus.1
        UIView.animate(withDuration: 0.15, animations: {
            MCQRootModel.shared.isAnimationStarted = false
            if isAttemptIncorrect == true {
                cell.updateCorrectOrIncorrectAnswer(isCorrect: isSelectedAnswerCorrect, isCorrectAttempt: false, completion: {
                })
            } else {
                cell.updateCorrectOrIncorrectAnswer(isCorrect: isSelectedAnswerCorrect, isCorrectAttempt: true, completion: { [unowned self] in
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        let reloadIndex = IndexPath(row: self.mcqChoices.count + 1, section: 0)
                        self.tableView.reloadRows(at: [reloadIndex], with: .automatic)
                    }
                })
            }
        })
    }

    func initiateAnimation() {
        self.mcqStudy.lastVisitedPageIndex += 1
        if self.mcqStudy.lastVisitedPageIndex < MCQRootModel.shared.mcqModel.mcq.termDefSet.count && self.mcqStudy.lastVisitedPageIndex >= 0 {
            self.mcq = MCQRootModel.shared.mcqModel.mcq.termDefSet[self.mcqStudy.lastVisitedPageIndex]
            self.updateChoices()
        } else {
            self.endTime = MCQUtilities.sharedInstance.getCurrentTimeStamp()
            self.mcqDelegate?.didMoveFromMCQ(self.startTime, self.endTime)
        }
    }

    func resetStudyTools() {
        self.mcqStudy.lastVisitedPageIndex = 0
    }

}

// MARK: - UITableViewDataSource
extension MCQListController: UITableViewDelegate,UITableViewDataSource, TinyQuizGestureDelegate {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            if mcq.imageURL.isEmpty {
                return UITableView.automaticDimension
            }
            return 196
        } else if indexPath.row == self.mcqChoices.count + 1 {
            return 88
        } else if indexPath.row == self.mcqChoices.count {
            return UITableView.automaticDimension // answer item height + bottom view for the last answer item
        } else {
            return UITableView.automaticDimension // answer item height
        }
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mcqChoices.count + 2
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var tableCell = UITableViewCell(frame: CGRect.zero)
        if indexPath.row == 0 {
            if mcq.imageURL.isEmpty == true {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "StudyMCQQuestionCell") as? MCQQuestionCell {
                    cell.configureView(termDefModel: mcq)
                    tableCell = cell
                }
            } else {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "StudyMCQImageCell") as? MCQImageCell {
                    cell.configureView(termDefModel: mcq)
                    tableCell = cell
                }
            }
        } else {
            if indexPath.row == self.mcqChoices.count + 1 {
                if let cell = self.tableView?.dequeueReusableCell(withIdentifier: "StudyMCQNextButtonTableViewCell", for: indexPath) as? MCQNextButtonTableViewCell {
                    cell.indexPath = indexPath
                    cell.delegate = self
                    cell.showNextButton = self.isAnimatedToNextPage
                    cell.isLastItem = self.isLastItem
                    tableCell = cell
                }
            } else {
                let choiceAttributedString = self.mcqChoices.count > indexPath.row - 1 ? self.mcqChoices[indexPath.row - 1] : NSMutableAttributedString(string: "")
                if let cell = tableView.dequeueReusableCell(withIdentifier: "StudyMCQAnswerCell") as? MCQAnswerCell {
                    cell.delegate = self
                    if choiceAttributedString.string != "" {
                        var markedAnswerState: MarkedAnswerState?
                        if let correctAnswer = self.correctAnswerState, indexPath.row == correctAnswer.index  {
                            markedAnswerState = correctAnswer
                        } else if let incorrectAnswer = self.incorrectAnswerState, indexPath.row == incorrectAnswer.index {
                            markedAnswerState = incorrectAnswer
                        }
                        cell.configureDefaultAnswerAttributedCell(choiceAttributedString, index: indexPath,
                                                                  isLast: self.mcqChoices.count == indexPath.row, markedAnswerState: markedAnswerState)
                        MCQUtilities.sharedInstance.setButtonAccessibility(view: cell.answerBackgroundView,
                                                                                  identifier: "Option" + String(indexPath.row),
                                                                                  value: self.mcqAccessibilityString[indexPath.row - 1])
                    } else {
                        cell.isHidden = true
                        cell.answerBackgroundView.isAccessibilityElement = false
                    }
                    tableCell = cell
                }
            }
        }
        tableCell.contentView.frame = tableCell.contentView.frame.insetBy(dx: 10, dy: 10)
        return tableCell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard !isAnimatedToNextPage else { return }
        
        if let answerCell = tableView.cellForRow(at: indexPath) as? MCQAnswerCell {
            let choiceAttributedString = self.mcqChoices.count > indexPath.row - 1 ? self.mcqChoices[indexPath.row - 1] : NSMutableAttributedString(string: "")
            if !choiceAttributedString.string.isEmpty {
                answerCell.onAnswerTaped()
            }
        }
    }

}

extension MCQListController {
    func didStartSelection(_ indexPath: IndexPath, cell: MCQAnswerCell) {
        tableView.isUserInteractionEnabled = false
        MCQRootModel.shared.isAnimationStarted = true
        for visibleCell in self.tableView.visibleCells {
            if let tempCell = visibleCell as? MCQAnswerCell {
                tempCell.isUserInteractionEnabled = false
                if tempCell == cell {
                    tempCell.animateSelectedCellOnTouchDown()
                } else {
                    tempCell.animateDefaultCellOnTouchDown()
                }
            }
        }
    }

    func didEndSelection(_ indexPath: IndexPath, cell: MCQAnswerCell) {
           let isSelectedAnswerCorrect = self.verifySelectedAnswer(indexPath)
           self.isSelectedAnswerCorrect = isSelectedAnswerCorrect
           Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.configureAnnouncement), userInfo: nil, repeats: false)
           let correctAnsIndex = self.getCorrectAnsIndex()
           mcq.attempts.removeAll()
           mcq.attempts.append(isSelectedAnswerCorrect ? .correct : .incorrect)
           cell.isUserInteractionEnabled = false

           //update the status for user selected cell
           if let selectedCell = self.tableView.visibleCells.filter ({ $0 == cell }).first as? MCQAnswerCell {
               self.configureSelectedAnswer(selectedCell, isSelectedAnswerCorrect: isSelectedAnswerCorrect, correctAnsIndex: correctAnsIndex)
           }
            tableView.isUserInteractionEnabled = true
       }

    func didMoveToNext(_ indexPath: IndexPath, cell: MCQNextButtonTableViewCell) {
        let analyticsValue = [MCQGuideConstants.EventCategory: MCQGuideConstants.StudyToolsMCQ, MCQGuideConstants.EventLabel: MCQGuideConstants.NEXT, MCQGuideConstants.EventAction: MCQGuideConstants.NEXT, MCQGuideConstants.EventValue: MCQRootModel.shared.summaryMastery, MCQGuideConstants.PageId: self.pageId, MCQGuideConstants.QuestionId: self.mcqStudy.id] as [String : Any]
        self.mcqDelegate?.didTapNextMCQ(analyticsValue)
        self.initiateAnimation()
    }
}


