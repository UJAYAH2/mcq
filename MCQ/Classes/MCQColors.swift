/*PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA

*  Copyright © 2021 Pearson Education, Inc.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Pearson Education, Inc.  The intellectual and technical concepts contained
* herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
* patent applications, and are protected by trade secret or copyright law.
* Dissemination of this information, reproduction of this material, and copying or distribution of this software
* is strictly forbidden unless prior written permission is obtained
* from Pearson Education, Inc.
*/

import UIKit

public struct MCQThemeColors {
    public var secondaryHeaderColor = UIColor(hex:0x05112A)
    public var primaryBackgroundColor = UIColor(hex:0xF7F9FD)
    public var primaryTitleTextColor = UIColor(hex:0x4F5561)
    public var primaryCTAButtonColor = [UIColor(hex:0xEE512E), UIColor(hex:0xDF006B), UIColor(hex:0xBD0071)]
    public var primaryCTAButtonTitleColor = UIColor(hex:0xFEFEFE)
    public var secondaryBackgroundColor = UIColor(hex: 0xFEFEFE)
    public var secondaryButtonColor = UIColor(hex: 0xFEFEFE)
    public var secondaryButtonTitleColor = UIColor(hex: 0x05112A)
    public var viewBorderColor = UIColor(hex: 0xE8EEFA)
    public var radioButtonUnselectedColor = UIColor(hex: 0xC7C7C7)
    public var correctAnswerColor = UIColor(hex:0x00655B)
    public var incorrectAnswerColor = UIColor(hex:0x7E031A)
    public var disabledButtonBackgroundColor = UIColor(hex: 0xE8EEFA)
    public var disabledButtonTitleColor = UIColor(hex: 0x9BA1AD)
    public init() {}
}

public enum ThemeMode: String {
   case light = "white"
   case sepia = "sepia"
   case dark = "dark"
}

extension UIColor {
    
    static var nonMojoPrimaryCTAButtonColor: UIColor {
        let theme = ThemeMode(rawValue: MCQRootModel.shared.themeMode) ?? .light
        switch theme {
        case .light:
            return UIColor(hex: 0x005F79)
        case .sepia:
            return UIColor(hex:0x6D0177)
        case .dark:
            return UIColor(hex: 0xA1EBFF)
        }
    }
        
    static var nonMojoPrimaryCTAButtonTitleColor: UIColor {
        let theme = ThemeMode(rawValue: MCQRootModel.shared.themeMode) ?? .light
        switch theme {
        case .light:
            return UIColor(hex: 0xFEFEFE)
        case .sepia:
            return UIColor(hex:0xFEFEFE)
        case .dark:
            return UIColor(hex: 0x333333)
        }
    }
   
    
    static var actionActiveTitleColor: UIColor {
        return UIColor.white
    }
   

    
    static let actionButtonActiveGradientColor1 = UIColor(hex: 0xEE512E)
    static let actionButtonActiveGradientColor2 = UIColor(hex: 0xDF006B)
    static let actionButtonActiveGradientColor3 = UIColor(hex: 0xBD0071)
    
    static let linkPlumDefault = UIColor(hex: 0x6D0176)
    
}

extension String {
    
    static var correctAnswerColor: String {
        let theme = ThemeMode(rawValue: MCQRootModel.shared.themeMode) ?? .light
        switch theme {
        case .light:
            return "#00655B"
        case .sepia:
            return "#00655B"
        case .dark:
            return "#8EF8EE"
        }
    }
    
    static var incorrectAnswerColor: String {
        let theme = ThemeMode(rawValue: MCQRootModel.shared.themeMode) ?? .light
        switch theme {
        case .light:
            return "#7E031A"
        case .sepia:
            return "#7E031A"
        case .dark:
            return "#FFA7D1"
        }
    }
    
    static var webTextColor: String {
        let theme = ThemeMode(rawValue: MCQRootModel.shared.themeMode) ?? .light
        switch theme {
        case .light:
            return "#05112A"
        case .sepia:
            return "#05112A"
        case .dark:
            return "#F7F9FD"
        }
    }
}
