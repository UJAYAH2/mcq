/*PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA

*  Copyright © 2021 Pearson Education, Inc.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Pearson Education, Inc.  The intellectual and technical concepts contained
* herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
* patent applications, and are protected by trade secret or copyright law.
* Dissemination of this information, reproduction of this material, and copying or distribution of this software
* is strictly forbidden unless prior written permission is obtained
* from Pearson Education, Inc.
*/

import Foundation

open class MCQBaseModel: NSObject {
    public var termDefSet: [MCQTermDefModel] = []
    public var mcq = MCQModel()
}

open class MCQModel: NSObject {
    public var id: String = ""
    public var title: String = ""
    public var termDefSet: [MCQTermDefModel] = []
    public var lastVisitedPageIndex: Int = 0
    public var studyToolsSelectedSegment: Int = 0

    public override init() {}
}

open class MCQTermDefModel: NSObject {
    public var id: String = ""
    public var bookId: String = ""
    public var definition: [Any] = []
    public var term: [Any] = []
    public var attempts: [Status] = []
    public var saved: Bool = false
    public var type: String = "Text"
    public var imageURL: String = ""
    public var cacheUrl: String = ""
    public var alt: String = ""
    public var choices: [Any] = []
    public var audioURL: String = ""
    public var isInCurrentSession: Bool = false
    
    public var questionMedia: String = ""
    public var question: String = ""
    public var kind: String = ""
    public var creatorId: String = ""
    public var deckId: String = ""
    public var platform: String = ""
    public var source: String = ""
    public var createdAt: String = ""
    public var updatedAt: String = ""
    public var answers: [MCQAnswer]? = nil
    public var archived: Bool = false
    public var favourite: Bool = false

    public override init() {}
}

open class MCQAnswer: NSObject {
    public var correct: Bool = false
    public var id: Int = 0
    public var type: String = ""
    public var value: String = ""
    
    public override init() {}
}

public enum Status: String {
    case correct
    case incorrect
    case skipped
}
