/*PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA

*  Copyright © 2021 Pearson Education, Inc.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Pearson Education, Inc.  The intellectual and technical concepts contained
* herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
* patent applications, and are protected by trade secret or copyright law.
* Dissemination of this information, reproduction of this material, and copying or distribution of this software
* is strictly forbidden unless prior written permission is obtained
* from Pearson Education, Inc.
*/

struct MCQFont {
    //Font
    static let SFProDisplaySemibold = "SF-Pro-Display-Semibold"
    static let SFProTextRegular = "SF-Pro-Text-Regular"//need to add resource file
    static let SFProTextSemibold = "SF-Pro-Text-Semibold"

    //Font
    static let FontSFProDisplaySemibold = "SFProDisplay-Semibold"
    static let FontSFProTextRegular = "SFProText-Regular"
    static let FontSFProTextSemibold = "SFProText-Semibold"
    static let FontSFProDisplayRegular = "SF-Pro-Display-Regular"////need to add resource file
    
    static let HindRegular = "Hind-Regular"
    static let HindBold = "Hind-Bold"
    static let TTCommonsRegular = "TTCommons-Regular"
    static let TTCommonsDemiBold = "TTCommons-DemiBold"
    static let TTCommonsBold = "TTCommons-Bold"
    static let NewYorkSmallRegular = "NewYorkSmall-Regular"
    static let HindSemiBold = "Hind-SemiBold"
}

struct MCQGuideConstants
{
    static let StudyToolsMCQ = "Studytools - MCQ"
    static let StudyBundleId = "org.cocoapods.Study"

    static let NoNetworkConnection = "NoNetworkConnection"//.localized
    static let OK = "OK"//.localized
    static let Error = "Error"//.localized
    static let UnKnownError = "Action failed. Please try again later."//.localized
    static let Alert = "Alert"//.localized

    static let NEXT = "NEXT"//.localized
    static let Complete = "Complete"//.localized

    //Accessibility
    static let kQuestionView = "QuestionView"
    static let kCloseButtonAccessibility = "CloseIconButton"
    static let kCloseButtonAccessibilityId = "studyguide_close"

    //GA keys
    static let EventCategory = "event_category"
    static let EventLabel = "event_label"
    static let EventAction = "event_action"
    static let EventValue = "event_value"
    static let PageId = "page_id"
    static let PageName = "page_name"
    static let QuestionId = "question_id"
}
