/*PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA

*  Copyright © 2021 Pearson Education, Inc.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Pearson Education, Inc.  The intellectual and technical concepts contained
* herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
* patent applications, and are protected by trade secret or copyright law.
* Dissemination of this information, reproduction of this material, and copying or distribution of this software
* is strictly forbidden unless prior written permission is obtained
* from Pearson Education, Inc.
*/


import UIKit
import ImageDownloader
import MBProgressHUD

class MCQUtilities: NSObject {
    var isRTL:Bool = false
    var themeMode: ThemeMode = .light
    class var sharedInstance: MCQUtilities {
        struct Singleton {
            static let instance = MCQUtilities()
        }
        return Singleton.instance
    }
    
    func fetchImage(_ name: String) -> UIImage? {
        let bundle = self.getBundle()
        return UIImage(named: name, in: bundle, compatibleWith: nil)
    }

    func getBundle() -> Bundle {
        let podBundle = Bundle(for: MCQUtilities.self)
        let bundleURL = podBundle.url(forResource: "MCQ", withExtension: "bundle")
        let bundle = Bundle(url: bundleURL!)!
        return bundle
    }

    func validateUrl (stringURL: NSString) -> Bool {
        let urlRegEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[urlRegEx])
        return predicate.evaluate(with: stringURL)
    }

    func configureAccessibilityString(_ content: [Any], completion: (String) -> ()) {
        var contentString = ""
        for item in content {
            if let stringValue = item as? String {
                let strippedString = MCQUtilities.sharedInstance.stripHTMLCharacters(stringValue)
                contentString.append(strippedString)
            } else if let mathMLDict = item as? NSDictionary {
                if let altString = mathMLDict["alt"] as? String {
                    contentString.append(altString)
                }
            }
        }
        completion(contentString)
    }

    func stripHTMLCharacters(_ value: String) -> String {
        if let tempData = value.data(using: String.Encoding.unicode, allowLossyConversion: true) {
            do {
                let attributedString = try NSAttributedString(data: tempData, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)

                return attributedString.string
            } catch {
            }
        }
        return ""
    }

    func startActivityIndicator(_ activityIndicator: UIActivityIndicatorView) {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }

    func stopActivityIndicator(_ activityIndicator: UIActivityIndicatorView) {
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }

    func getCurrentTimeStamp() -> String {
        var dateString = ""
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateString = formatter.string(from: now)
        return dateString
    }
    
    func setAccessibility(view: UIView, identifier: String, value: String, traits: UIAccessibilityTraits = .staticText) {
        view.isAccessibilityElement = !view.isHidden && value != ""
        view.accessibilityIdentifier = identifier
        view.accessibilityTraits = traits
        view.accessibilityValue = "" //Fix for TextView data is read twice on VoiceOver
        view.accessibilityLabel = value
    }
    
    func updateImage(_ url: String, _ cacheUrl: String, cardView: UIView) {
        var cardImageView: UIImageView = UIImageView()
        var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()

        if let flashFrontCardView = cardView as? MCQImageCell {
            cardImageView = flashFrontCardView.mcqImageView
            activityIndicator = flashFrontCardView.imageActivityIndicatorView
        }
        self.startActivityIndicator(activityIndicator)
        if cacheUrl != "" , let downloadedphoto = UIImage(contentsOfFile: cacheUrl) {
            DispatchQueue.main.async(execute: { () -> Void in
                cardImageView.image = downloadedphoto
                self.stopActivityIndicator(activityIndicator)
            })
        } else {
            InteractivesDownloaderKit.downloadImage(url, cacheUrl) { (image) in
                DispatchQueue.main.async(execute: { () -> Void in
                    cardImageView.image = image
                    self.stopActivityIndicator(activityIndicator)
                    cardImageView.setNeedsDisplay()
                    if (image.size.width < cardImageView.bounds.size.width) && (image.size.height < cardImageView.bounds.size.height) {
                        cardImageView.contentMode = .center
                    }
                })
            }
        }
    }

    func setBarButtonAccessibility(barButton: UIBarButtonItem!, identifier: String, value: String) {
        barButton.isAccessibilityElement = true
        barButton.accessibilityLabel = value
        barButton.accessibilityIdentifier = identifier
        barButton.accessibilityTraits = barButton.isEnabled  ? UIAccessibilityTraits.button : UIAccessibilityTraits.notEnabled
        barButton.accessibilityTraits = barButton.isEnabled  ? UIAccessibilityTraits.button : UIAccessibilityTraits.notEnabled
    }

    func setButtonAccessibility(view: UIView, identifier: String, value: String ) {
        if view.isHidden == false {
            self.setAccessibility(view: view, identifier: identifier, value: value)
            view.accessibilityTraits = UIAccessibilityTraits.button
        }
    }

}

extension UIFont {

    var definitionFont: UIFont {
        return UIFont(name: MCQFont.HindRegular, size: 16) ?? UIFont.systemFont(ofSize: 16, weight: .regular)
    }
    
    static func registerFont(bundle: Bundle, fontName: String, fontExtension: String)  {
        guard let fontURL = bundle.url(forResource: fontName, withExtension: fontExtension) else {
            fatalError("Couldn't find font \(fontName)")
        }

        guard let fontDataProvider = CGDataProvider(url: fontURL as CFURL) else {
            fatalError("Couldn't load data from the font \(fontName)")
        }

        let font = CGFont(fontDataProvider)

        var error: Unmanaged<CFError>?
        let success = CTFontManagerRegisterGraphicsFont(font!, &error)
    }
}

extension Double {
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }

    convenience init(hex: Int) {
        self.init(red:(hex >> 16) & 0xff, green:(hex >> 8) & 0xff, blue:hex & 0xff)
    }
    
    var quizCellBkgColor: UIColor {
        return UIColor(red: 245, green: 245, blue: 245)
    }

    var viewBackgroundColor: UIColor {
        return UIColor(red: 250, green: 250, blue: 250)
    }

    var progressBarProgressColor: UIColor {
        return UIColor(red: 21, green: 115, blue: 151)
    }

    var singleSelectionBorderColor: UIColor {
        return UIColor(red: 0.0/225.0, green: 93.0/225.0, blue: 131.0/225.0, alpha: 1.0)
    }

    var correctBkgColor: UIColor {
        return UIColor(red: 224, green: 237, blue: 230)
    }

    var correctBorderColor: UIColor {
        return UIColor(red: 3, green: 130, blue: 56)
    }

    var incorrectBorderColor: UIColor {
        return UIColor(red: 202, green: 70, blue: 68)
    }
}

extension UILabel {
    func scaleFont() {
        self.font = UIFontMetrics.default.scaledFont(for: self.font, maximumPointSize: 28.0)
        self.adjustsFontForContentSizeCategory = true
    }
    var numberOfVisibleLines: Int {
        let textSize = CGSize(width: CGFloat(self.frame.size.width), height: CGFloat(MAXFLOAT))
        let rHeight: Int = lroundf(Float(self.sizeThatFits(textSize).height))
        let charSize: Int = lroundf(Float(self.font.pointSize))
        return rHeight / charSize
    }
}

extension UIViewController {
    func alert(title: String, message: String, action1: String? = nil) {
        let alert = UIAlertController(title: title , message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: action1 ?? MCQGuideConstants.OK, style: .default, handler: { action in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension Int {
    var random: Int {
        return Int(arc4random_uniform(UInt32(abs(self))))
    }
    var indexRandom: [Int] {
        return  Array(0..<self).shuffle
    }
}

extension Array {
    var shuffle: [Element] {
        var elements = self
        for index in indices {
            let anotherIndex = Int(arc4random_uniform(UInt32(elements.count - index))) + index
            anotherIndex != index ? elements.swapAt(index, anotherIndex) : ()
        }
        return elements
    }

    func choose(_ x: Int) -> [Element] {
        if x > count { return shuffle }
        let indexes = count.indexRandom[0..<x]
        var result: [Element] = []
        for index in indexes {
            result.append(self[index])
        }
        return result
    }

    mutating func removeObject<U: Equatable>(_ object: U) {
        for (idx, objectToCompare) in self.enumerated() {  //in old swift use enumerate(self)
            if let to = objectToCompare as? U {
                if object == to {
                    self.remove(at: idx)
                }
            }
        }
    }
}

extension UIView {
    public func anchor(top: NSLayoutYAxisAnchor?, left: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, right: NSLayoutXAxisAnchor?,  paddingTop: CGFloat?, paddingLeft: CGFloat?, paddingBottom: CGFloat?, paddingRight: CGFloat?, width: CGFloat?, height: CGFloat?, centerX: NSLayoutXAxisAnchor?, centerY: NSLayoutYAxisAnchor?) {
        translatesAutoresizingMaskIntoConstraints = false
        
        if let safeTop = top, let safePaddingTop = paddingTop {
            topAnchor.constraint(equalTo: safeTop, constant: safePaddingTop).isActive = true
        }
        
        if let safeLeft = left, let safePaddingLeft = paddingLeft {
            leftAnchor.constraint(equalTo: safeLeft, constant: safePaddingLeft).isActive = true
        }
        
        if let safeBottom = bottom, let safePaddingBottom = paddingBottom  {
            bottomAnchor.constraint(equalTo: safeBottom, constant: -safePaddingBottom).isActive = true
        }
        
        if let safeRight = right, let safePaddingRight = paddingRight  {
            rightAnchor.constraint(equalTo: safeRight, constant: -safePaddingRight).isActive = true
        }
        
        if let safeWidth = width, safeWidth != 0 {
            widthAnchor.constraint(equalToConstant: safeWidth).isActive = true
        }
        
        if let safeHeight = height, safeHeight != 0{
            heightAnchor.constraint(equalToConstant: safeHeight).isActive = true
        }
        
        if let centerX = centerX {
            centerXAnchor.constraint(equalTo: centerX).isActive = true
        }
        
        if let centerY = centerY {
            centerYAnchor.constraint(equalTo: centerY).isActive = true
        }
    }
}

class StudyMCQWebCellBackground: UIView {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
}

class CellUIView: UIView {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        self.backgroundColor = UIColor.clear
    }
}

        

