/*PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA

*  Copyright © 2021 Pearson Education, Inc.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Pearson Education, Inc.  The intellectual and technical concepts contained
* herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
* patent applications, and are protected by trade secret or copyright law.
* Dissemination of this information, reproduction of this material, and copying or distribution of this software
* is strictly forbidden unless prior written permission is obtained
* from Pearson Education, Inc.
*/

import UIKit

class MCQActionButton: UIButton {
    
    private var isActive = true
    private let gradientLayer = CAGradientLayer()
    private var isButtonDrawn = false
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.cornerRadius = frame.height / 2.0
        isButtonDrawn = true
        if isActive {
            setBackgroundAsNeeded()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initialize()
    }
    
    private func initialize() {
        clipsToBounds = true
        titleLabel?.font =  UIFont(name: MCQFont.HindSemiBold, size: 16) ?? UIFont.systemFont(ofSize: 16, weight: .semibold)
        titleLabel?.textAlignment = .center
        setEnabled(isEnabled: true)
    }
    
    private func addGradientBackground() {
        gradientLayer.removeFromSuperlayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = MCQRootModel.shared.themeColors.primaryCTAButtonColor.compactMap({$0.cgColor})
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    private func setBackgroundAsNeeded() {
        guard isButtonDrawn else {
            return
        }
        addGradientBackground()
    }
}

// MARK: - Public Methods

extension MCQActionButton {
    
    func setEnabled(isEnabled: Bool) {
        self.isEnabled = isEnabled
        isActive = isEnabled
        setTitleColor(isEnabled ? MCQRootModel.shared.themeColors.primaryCTAButtonTitleColor : MCQRootModel.shared.themeColors.disabledButtonTitleColor, for: .normal)
        if isEnabled {
            setBackgroundAsNeeded()
        } else {
            accessibilityHint = nil
            gradientLayer.removeFromSuperlayer()
            backgroundColor = MCQRootModel.shared.themeColors.disabledButtonBackgroundColor
        }
    }
}
