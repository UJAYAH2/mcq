/* PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA

 *  Copyright © 2021 Pearson Education, Inc.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Pearson Education, Inc.  The intellectual and technical concepts contained
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
 * patent applications, and are protected by trade secret or copyright law.
 * Dissemination of this information, reproduction of this material, and copying or distribution of this software
 * is strictly forbidden unless prior written permission is obtained
 * from Pearson Education, Inc.*/

import UIKit
import MMIUtilities

class MCQImageCell: UITableViewCell {
    @IBOutlet weak var definitionLabel: UILabel!
    @IBOutlet weak var mcqImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var definitionHeightConstraint: NSLayoutConstraint!
    lazy var imageActivityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50)) as UIActivityIndicatorView
        activityIndicatorView.center = self.mcqImageView.center
        activityIndicatorView.hidesWhenStopped = true
        activityIndicatorView.style = UIActivityIndicatorView.Style.gray
        self.addSubview(activityIndicatorView)
        return activityIndicatorView
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layoutMargins = UIEdgeInsets.zero
        self.preservesSuperviewLayoutMargins = false
        let selectedBackgroundView = UIView()
        selectedBackgroundView.backgroundColor = UIColor().quizCellBkgColor
        self.selectedBackgroundView = selectedBackgroundView
        self.definitionLabel.font = UIFont().definitionFont
        self.containerView.layer.borderWidth = 1
        self.containerView.layer.cornerRadius = 10
        setTheme()
    }
    
    private func setTheme() {
        definitionLabel.textColor = MCQRootModel.shared.themeColors.secondaryHeaderColor
        containerView.backgroundColor = MCQRootModel.shared.themeColors.secondaryButtonColor
        containerView.layer.borderColor = MCQRootModel.shared.themeColors.viewBorderColor.cgColor
    }

    func startActivityIndicator(_ activityIndicator: UIActivityIndicatorView) {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }

    func stopActivityIndicator(_ activityIndicator: UIActivityIndicatorView) {
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }

    func configureView(termDefModel: MCQTermDefModel) {
        if termDefModel.definition.isEmpty == false {
            MMIUtilitiesKit.sharedInstance.fetchAttributedString(termDefModel.definition, textFont: UIFont().definitionFont, textColor: self.definitionLabel.textColor, completion: {[unowned self] (content) in
                self.definitionLabel.attributedText = content
                let sizeThatFitsTextView = self.definitionLabel.sizeThatFits(CGSize.init(width: self.definitionLabel.frame.size.width, height: CGFloat(MAXFLOAT)))
                self.definitionLabel.numberOfVisibleLines > 4 ? (self.definitionHeightConstraint.constant = sizeThatFitsTextView.height - 150) : (self.definitionHeightConstraint.constant = sizeThatFitsTextView.height)
                self.definitionLabel.numberOfLines = 4
                MCQUtilities.sharedInstance.configureAccessibilityString(termDefModel.definition, completion: { (content) in
                    self.definitionLabel.accessibilityLabel = content
                    self.definitionLabel.accessibilityValue = ""
                    self.definitionLabel.accessibilityIdentifier = MCQGuideConstants.kQuestionView
                })
            })
            if MCQUtilities.sharedInstance.validateUrl(stringURL: termDefModel.imageURL as NSString) {
                self.configureImage(termDefModel.imageURL, termDefModel.cacheUrl, termDefModel.alt)
            }
        }
        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: self.definitionLabel)
    }

    func configureImage(_ imageURL: String, _ cacheUrl: String, _ alt: String) {
        MCQUtilities.sharedInstance.updateImage(imageURL, cacheUrl, cardView: self)
        MCQUtilities.sharedInstance.setAccessibility(view: mcqImageView, identifier: MCQGuideConstants.kQuestionView, value: alt)
    }
}


