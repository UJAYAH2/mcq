/*PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA

*  Copyright © 2021 Pearson Education, Inc.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Pearson Education, Inc.  The intellectual and technical concepts contained
* herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
* patent applications, and are protected by trade secret or copyright law.
* Dissemination of this information, reproduction of this material, and copying or distribution of this software
* is strictly forbidden unless prior written permission is obtained
* from Pearson Education, Inc.
*/

import UIKit
protocol TinyQuizGestureDelegate: class {
    func didStartSelection(_ indexPath: IndexPath, cell: MCQAnswerCell)
    func didEndSelection(_ indexPath: IndexPath, cell: MCQAnswerCell)
    func didMoveToNext(_ indexPath: IndexPath, cell: MCQNextButtonTableViewCell)
}
class MCQAnswerCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var correctOrIncorrectImageView: UIImageView!
    @IBOutlet weak var answerBackgroundView: UIView!
    @IBOutlet weak var topBorderView: UIView!
    @IBOutlet weak var bottomBorderView: UIView!
    @IBOutlet weak var bottomBackgroundView: UIView!
    @IBOutlet weak var lastCellBottomView: UIView!
    
    weak var delegate: TinyQuizGestureDelegate!
    var indexPath: IndexPath!
    
    private var theme = ThemeMode.light

    override func prepareForReuse() {
        super.prepareForReuse()
        lastCellBottomView.isHidden = false
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.titleLabel.font = UIFont(name: MCQFont.HindRegular, size: 16) ?? UIFont.systemFont(ofSize: 16, weight: .regular)
        bottomBackgroundView.layer.cornerRadius = 10.0
        bottomBorderView.layer.cornerRadius = 10.0
        lastCellBottomView.isHidden = false
        correctOrIncorrectImageView.isHidden = false
    }

    func configureDefaultAnswerAttributedCell(_ choice: NSMutableAttributedString, index: IndexPath,
                                              isLast: Bool, markedAnswerState: MarkedAnswerState?) {
        self.configureDefaultBackgroundCell()
        let updateAttributedString = NSMutableAttributedString(attributedString: choice)
        self.titleLabel.attributedText = updateAttributedString
        self.indexPath = index
        if MCQUtilities.sharedInstance.isRTL {
            self.titleLabel.textAlignment = .right
        }
        lastCellBottomView.isHidden = !isLast
        setThemeColor()
        if let answeredState = markedAnswerState {
            updateCorrectOrIncorrectAnswer(isCorrect: answeredState.isCorrectAnswer, isCorrectAttempt: answeredState.isCorrectAttempt, isAnimated: false) { }
        } else {
            configureDefaultBackgroundCell()
        }
    }
    
    func setThemeColor() {
        answerBackgroundView.backgroundColor = MCQRootModel.shared.themeColors.secondaryButtonColor
        topBorderView.backgroundColor = MCQRootModel.shared.themeColors.viewBorderColor
        bottomBackgroundView.backgroundColor = MCQRootModel.shared.themeColors.secondaryButtonColor
        bottomBorderView.backgroundColor = MCQRootModel.shared.themeColors.viewBorderColor
        correctOrIncorrectImageView.tintColor = MCQRootModel.shared.themeColors.radioButtonUnselectedColor
        titleLabel.textColor = MCQRootModel.shared.themeColors.primaryTitleTextColor
    }

    func configureDefaultBackgroundCell() {
        let image = MCQUtilities.sharedInstance.fetchImage("RadioButton" as String)
        correctOrIncorrectImageView.image = image
        titleLabel.font = UIFont(name: MCQFont.HindRegular, size: 16) ?? UIFont.systemFont(ofSize: 16, weight: .regular)
    }

    func animateSelectedCellOnTouchDown() {
        self.isUserInteractionEnabled = true
    }

    func animateDefaultCellOnTouchDown() {
        self.isUserInteractionEnabled = false
    }

    func updateCorrectOrIncorrectAnswer(isCorrect: Bool, isCorrectAttempt: Bool, isAnimated: Bool = true, completion: @escaping () -> Void) {
        var imgName = "WrongIcon"
        if isCorrectAttempt {
            imgName = "CorrrectAttempt"
        } else if isCorrect {
            imgName = "CorrectIcon"
        }
        let image = MCQUtilities.sharedInstance.fetchImage(imgName as String)
        
        let titleValue = "\(titleLabel.text ?? "") \((imgName == "WrongIcon") ? "Incorrect answer" : "Correct answer")"
        MCQUtilities.sharedInstance.setButtonAccessibility(view: self.answerBackgroundView,
                                                                  identifier: "Option" + String(indexPath.row),
                                                                  value: titleValue)
        
        titleLabel.font = UIFont(name: MCQFont.HindBold, size: 16) ?? UIFont.systemFont(ofSize: 16, weight: .regular)
        setAnswerStateIndicators(image: image, tint: isCorrect ? MCQRootModel.shared.themeColors.correctAnswerColor : MCQRootModel.shared.themeColors.incorrectAnswerColor, isAnimated: isAnimated)
        completion()
    }

    func onAnswerTaped() {
        if let index = self.indexPath {
            let image = MCQUtilities.sharedInstance.fetchImage("RadioButtonSelected" as String)
            var selectedRadioButtonColor = UIColor.clear
            
            selectedRadioButtonColor = MCQRootModel.shared.themeColors.secondaryHeaderColor 
            self.correctOrIncorrectImageView.image = image
            self.correctOrIncorrectImageView.tintColor = selectedRadioButtonColor
            self.titleLabel.textColor = selectedRadioButtonColor
            self.titleLabel.font = UIFont(name: MCQFont.HindBold, size: 16) ?? UIFont.systemFont(ofSize: 16, weight: .regular)
            
            self.delegate?.didStartSelection(index, cell: self)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.45) {
                self.delegate?.didEndSelection(index, cell: self)
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

extension MCQAnswerCell {
    
    private func setAnswerStateIndicators(image: UIImage?, tint: UIColor, isAnimated: Bool) {
        self.correctOrIncorrectImageView.image = image
        self.correctOrIncorrectImageView.tintColor = tint
        if isAnimated {
            self.correctOrIncorrectImageView.animateInWithBounceEffect()
        }
        
        self.titleLabel.textColor = tint
    }
    
}

extension UIView {
    
    func animateInWithBounceEffect() {
        self.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
        
        UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            self.alpha = 1
            self.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
}
