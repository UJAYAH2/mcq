/*PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA

*  Copyright © 2021 Pearson Education, Inc.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Pearson Education, Inc.  The intellectual and technical concepts contained
* herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
* patent applications, and are protected by trade secret or copyright law.
* Dissemination of this information, reproduction of this material, and copying or distribution of this software
* is strictly forbidden unless prior written permission is obtained
* from Pearson Education, Inc.
*/

import UIKit
import MMIUtilities

public enum DeckType: String {
    case Custom
    case Keyterm
    case Expert
    case None
}

open class MCQKit: NSObject {
    static var mcqController: MCQListController!
    static var mcqWebCardController: MCQWebCardVC!
    static var mcqView: UIView!
    static var mcqWebView: UIView!
    static var podVersion = MMIUtilitiesKit.sharedInstance.getPodVersion(bundleId: MCQGuideConstants.StudyBundleId)
}
