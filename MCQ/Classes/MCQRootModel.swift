/*PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA

*  Copyright © 2021 Pearson Education, Inc.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Pearson Education, Inc.  The intellectual and technical concepts contained
* herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
* patent applications, and are protected by trade secret or copyright law.
* Dissemination of this information, reproduction of this material, and copying or distribution of this software
* is strictly forbidden unless prior written permission is obtained
* from Pearson Education, Inc.
*/

import UIKit
import MMIUtilities

open class MCQRootModel {
    var mainResponse: [String: Any] = [:]
    var mcqModel = MCQBaseModel()
    var themeMode: String = ThemeMode.light.rawValue
    var summaryMastery: Double = 0.0
    var isAnimationStarted:Bool = false
    var deckType: DeckType = .None
    var themeColors = MCQThemeColors()
    
    public static let shared = MCQRootModel()

    func reloadMCQContent(_ completion:() -> ()) {
        if let mcqTermDefModel = self.mcqModel.mcq.termDefSet.first {
            if mcqTermDefModel.choices.count == 0 {
                MCQRootModel.shared.updateChoices(item: mcqTermDefModel, termDefSet: self.mcqModel.termDefSet, completion: { (choices) in
                    mcqTermDefModel.choices = choices
                })
            }
            if let termDef = MCQRootModel.shared.mcqModel.termDefSet.filter({ $0.id == mcqTermDefModel.id}).first {
                mcqTermDefModel.saved = termDef.saved
            }
        }
        completion()
    }

    func updateChoices(item: MCQTermDefModel, termDefSet: [MCQTermDefModel], completion: ([Any]) -> ()) {
        if item.term.isEmpty == false {
            let defaultChoiceCount = termDefSet.count <= 3 ? termDefSet.count - 1 : 3
            var allTermDefSet = termDefSet
            if let repeatedItem = allTermDefSet.filter({ $0.id == item.id }).first {
                allTermDefSet.removeObject(repeatedItem)
            }
            var termsChoices = [Any]()
            if MCQRootModel.shared.deckType == .Expert {
                if let flashcardAns = item.answers  {
                    for ans in flashcardAns {
                        termsChoices.append(ans.value)
                    }
                }
            } else {
                    var choices = allTermDefSet.map({ $0.term }).choose(defaultChoiceCount)
                    choices.append(item.term)
                    termsChoices = choices.shuffle
            }
            
            completion(termsChoices)
        }
    }
}
