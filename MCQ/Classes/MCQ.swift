// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//

import UIKit

public protocol McqDelegate: UIViewController {
    func didMoveFromMCQ(_ startTime: String, _ endTime: String)
    func didLoadMCQ(_ analyticsDict: [String : Any])
    func didDismissMCQ(_ analyticsDict: [String : Any])
    func didAttemptMCQ(_ analyticsDict: [String : Any])
    func didTapNextMCQ(_ analyticsDict: [String : Any])
}

public class MCQ
{
    private let parent: McqDelegate?
    private var rootModel = MCQRootModel.shared
    public var delegate: McqDelegate?
    private var isHTMLContent = false
//    private let ptTagList = 00287499378345
//    private let ptTagWeb = 00287499378348
    
    public init<T: McqDelegate>(parent: T, card: MCQModel, index: Int, isLastItem: Bool = false, themeMode: ThemeMode = .light)
    {
        self.parent = parent
        
        MCQRootModel.shared.mcqModel.mcq.termDefSet = card.termDefSet
        
        var last = index - 1
        last = last < 0 ? 0 : last
        MCQRootModel.shared.mcqModel.mcq.lastVisitedPageIndex = last
        isHTMLContent = getHTMLContentStatus()
        isHTMLContent ? configureMCQWebCardView(isLastItem: isLastItem) : configureMCQView(isLastItem: isLastItem)
        MCQRootModel.shared.mcqModel.mcq = card
        MCQRootModel.shared.reloadMCQContent {
        }
    }
    
    public func set(card: MCQModel) {
        MCQRootModel.shared.mcqModel.mcq.termDefSet = card.termDefSet
    }
}

extension MCQ
{
    func getHTMLContentStatus() -> Bool {
        let currentMCQ = MCQRootModel.shared.mcqModel.mcq.termDefSet[MCQRootModel.shared.mcqModel.mcq.lastVisitedPageIndex]
         
        if currentMCQ.questionMedia == "HTML" || currentMCQ.questionMedia == "MEDIA" {
            return true
        } else {
            if let flashcardAns = currentMCQ.answers  {
                for ans in flashcardAns {
                    if ans.type == "HTML" || ans.type == "MEDIA" {
                        return true
                    }
                }
            }
        }
        
        return false
    }
    
    func getStudyStoryboard() -> UIStoryboard {
        let podBundle = Bundle(for: MCQListController.self)
        let bundleURL = podBundle.url(forResource: "MCQ", withExtension: "bundle")
        let bundle = Bundle(url: bundleURL!)
        let storyboard = UIStoryboard(name: "MCQ", bundle: bundle)
        return storyboard
    }
    
    //MARK: MCQ
    func configureMCQView(isLastItem: Bool) {
        let storyboard = self.getStudyStoryboard()
        if let controller = storyboard.instantiateViewController(withIdentifier: "StudyMCQListController") as? MCQListController {
            controller.mcqDelegate = self.parent
            MCQKit.mcqView = controller.view
            MCQKit.mcqController = controller
            MCQKit.mcqController.isLastItem = isLastItem
        }
    }
    
    //MARK: MCQWebCard
    func configureMCQWebCardView(isLastItem: Bool) {
        let storyboard = self.getStudyStoryboard()
        if let controller = storyboard.instantiateViewController(withIdentifier: "StudyMCQWebCardVC") as? MCQWebCardVC {
            controller.mcqDelegate = self.parent
            MCQKit.mcqWebView = controller.view
            MCQKit.mcqWebCardController = controller
            MCQKit.mcqWebCardController.isLastItem = isLastItem
        }
    }
    
    public func load(completion: (MCQViewController) -> ())  {
//        self.remove(from: container)
//        DispatchQueue.main.async { [self] in
//            if let parent = self.parent {
//                if isHTMLContent {
//                    if MCQKit.mcqWebCardController != nil {
//                        if let subView = MCQKit.mcqWebCardController.view {
//                            subView.tag = ptTagWeb
//                            container.addSubview(subView)
//                            parent.addChild(MCQKit.mcqWebCardController)
//                            MCQKit.mcqWebCardController.didMove(toParent: parent)
//                            subView.anchor(top: container.topAnchor, left: container.leftAnchor, bottom: container.bottomAnchor, right: container.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: nil, height: nil, centerX: nil, centerY: nil)
//                        }
//                    }
//                } else {
//                    if MCQKit.mcqController != nil {
//                        MCQKit.mcqController.tableView.reloadData()
//                        if let subView = MCQKit.mcqController.view {
//                            subView.tag = ptTagList
//                            container.addSubview(subView)
//                            parent.addChild(MCQKit.mcqController)
//                            MCQKit.mcqController.didMove(toParent: parent)
//                            subView.anchor(top: container.topAnchor, left: container.leftAnchor, bottom: container.bottomAnchor, right: container.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: nil, height: nil, centerX: nil, centerY: nil)
//                        }
//                    }
//                }
//            }
//        }
        
        if isHTMLContent {
            if MCQKit.mcqWebCardController != nil {
                completion(MCQKit.mcqWebCardController)
            }
        } else {
            if MCQKit.mcqController != nil {
                completion(MCQKit.mcqController)
            }
        }
    }
    
//    public func remove(from container: UIView) {
//        for view in container.subviews {
//            switch view.tag {
//            case ptTagWeb:
//                view.removeFromSuperview()
//                MCQKit.mcqWebCardController.removeFromParent()
//                break
//            case ptTagList:
//                view.removeFromSuperview()
//                MCQKit.mcqController.removeFromParent()
//                break
//            default:
//                break
//            }
//        }
//    }
}
