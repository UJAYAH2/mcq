/*PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA

*  Copyright © 2021 Pearson Education, Inc.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Pearson Education, Inc.  The intellectual and technical concepts contained
* herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
* patent applications, and are protected by trade secret or copyright law.
* Dissemination of this information, reproduction of this material, and copying or distribution of this software
* is strictly forbidden unless prior written permission is obtained
* from Pearson Education, Inc.
*/

import UIKit
import MMIUtilities

class MCQQuestionCell: UITableViewCell {

    @IBOutlet weak var textContainerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        self.layoutMargins = UIEdgeInsets.zero
        self.preservesSuperviewLayoutMargins = false
        self.selectedBackgroundView = selectedBackgroundView
        self.titleLabel.font = UIFont().definitionFont
        self.textContainerView.layer.borderWidth = 1.0
        self.textContainerView.layer.cornerRadius = 10.0
        selectionStyle = .none
        setTheme()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setTheme() {
        titleLabel.textColor = MCQRootModel.shared.themeColors.secondaryHeaderColor
        textContainerView.backgroundColor = MCQRootModel.shared.themeColors.secondaryButtonColor
        textContainerView.layer.borderColor = MCQRootModel.shared.themeColors.viewBorderColor.cgColor
    }

    func configureView(termDefModel: MCQTermDefModel) {
        MMIUtilitiesKit.sharedInstance.fetchAttributedString(termDefModel.definition, textFont: UIFont().definitionFont, textColor: self.titleLabel.textColor!, completion: {[unowned self] (content) in
            if content.string.count > 0 {
                let paragraphStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
                paragraphStyle.lineSpacing = 6.0
                content.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: content.string.count - 1))
                self.titleLabel.attributedText = content
                self.titleLabel.isHidden = false
                if MCQUtilities.sharedInstance.isRTL {
                    self.titleLabel.textAlignment = .right
                }
                MCQUtilities.sharedInstance.configureAccessibilityString(termDefModel.definition, completion: {[unowned self] (content) in
                    self.titleLabel.accessibilityLabel = content
                    self.titleLabel.accessibilityValue = ""
                    self.titleLabel.accessibilityIdentifier = MCQGuideConstants.kQuestionView
                })
            }
        })
        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: self.titleLabel)
    }
}

