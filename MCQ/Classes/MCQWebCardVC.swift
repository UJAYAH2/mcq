// PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
//
//  Copyright © 2021 Pearson Education, Inc.
//  All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains
// the property of Pearson Education, Inc.  The intellectual and technical concepts contained
// herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
// patent applications, and are protected by trade secret or copyright law.
// Dissemination of this information, reproduction of this material, and copying or distribution of this software
// is strictly forbidden unless prior written permission is obtained
// from Pearson Education, Inc.
//

import UIKit
import WebKit
import MMIUtilities
import MBProgressHUD

public class MCQWebCardVC: MCQViewController {


    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    
    //question
    @IBOutlet weak var questionBaseView: CellUIView!
    @IBOutlet weak var questionWebView: WKWebView!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var webViewHeight: NSLayoutConstraint!
    @IBOutlet weak var questionLblHeight: NSLayoutConstraint!
    @IBOutlet weak var questionLblView: UIView!
    
    // Answer 1
    @IBOutlet weak var backView1: UIView!
    @IBOutlet weak var answerView1: WKWebView!
    @IBOutlet weak var iconView1: UIImageView!
    @IBOutlet weak var answerView1Height: NSLayoutConstraint!
    @IBOutlet weak var answerFrontView1: UIView!
    @IBOutlet weak var answer1Label: UILabel!
    @IBOutlet weak var answer1LblHeight: NSLayoutConstraint!
    // Answer 2
    @IBOutlet weak var backView2: StudyMCQWebCellBackground!
    @IBOutlet weak var answerView2: WKWebView!
    @IBOutlet weak var iconView2: UIImageView!
    @IBOutlet weak var answerView2Height: NSLayoutConstraint!
    @IBOutlet weak var answerFrontView2: UIView!
    @IBOutlet weak var answer2Label: UILabel!
    @IBOutlet weak var answer2LblHeight: NSLayoutConstraint!
    
    // Answer 3
    @IBOutlet weak var backView3: StudyMCQWebCellBackground!
    @IBOutlet weak var answerView3: WKWebView!
    @IBOutlet weak var iconView3: UIImageView!
    @IBOutlet weak var answerView3Height: NSLayoutConstraint!
    @IBOutlet weak var answerFrontView3: UIView!
    @IBOutlet weak var answer3Label: UILabel!
    @IBOutlet weak var answer3LblHeight: NSLayoutConstraint!
    
    // Answer4
    @IBOutlet weak var backView4: StudyMCQWebCellBackground!
    @IBOutlet weak var answerView4: WKWebView!
    @IBOutlet weak var iconView4: UIImageView!
    @IBOutlet weak var answerView4Height: NSLayoutConstraint!
    @IBOutlet weak var answerFrontView4: UIView!
    @IBOutlet weak var answer4Label: UILabel!
    @IBOutlet weak var answer4LblHeight: NSLayoutConstraint!
    
    //Next Button
    @IBOutlet weak var nextButton: MCQActionButton!
   
    var isAnswered = false
    var isAnsweredCorrectly = false
    var selectedAnswerIndex = 0
    var correctAnswerIndex = 0
    
    weak var mcqDelegate: McqDelegate?
    
    var analyticsValue : [String : Any] = [:]
    var mcqStudy = MCQModel()
    var mcq: MCQTermDefModel = MCQTermDefModel()
    var mcqChoices: [String] = []
    var mcqAccessibilityString: [String] = []
    var isSelectedAnswerCorrect = false
    var startTime = ""
    var endTime = ""
    var pageId = ""
    var isLastItem = false
    
    var webViewCount = 0
    var urlString = ""
    var totalWebCount = 0
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pageId = UUID().uuidString
        self.startTime = MCQUtilities.sharedInstance.getCurrentTimeStamp()
        
        self.navigationController?.isNavigationBarHidden = false
        
        // ***********
        
//        StudyUtilities.sharedInstance.setBarButtonAccessibility(barButton: self.navigationItem.leftBarButtonItem, identifier: StudyGuideConstants.kCloseButtonAccessibility.localized, value: StudyGuideConstants.kCloseButtonAccessibilityId.localized)
        
        MCQUtilities.sharedInstance.setBarButtonAccessibility(barButton: self.navigationItem.leftBarButtonItem, identifier: MCQGuideConstants.kCloseButtonAccessibility, value: MCQGuideConstants.kCloseButtonAccessibilityId)
        
        // ***********
        
        NotificationCenter.default.addObserver(self, selector: #selector(rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadContentView), name: UIContentSizeCategory.didChangeNotification, object: nil)
        
        self.containerView.layer.borderWidth = 1
        self.containerView.layer.cornerRadius = 8.0
        self.containerView.backgroundColor = UIColor.white
        self.view.backgroundColor = UIColor.clear
        
        self.containerView.backgroundColor = MCQRootModel.shared.themeColors.secondaryButtonColor
        self.containerView.layer.borderColor = MCQRootModel.shared.themeColors.viewBorderColor.cgColor
        
        questionWebView.configuration.userContentController.addUserScript(viewPortScript())
        questionWebView.tag = 10
        questionWebView.uiDelegate = self
        questionWebView.navigationDelegate = self
        questionWebView.backgroundColor = UIColor.clear
        questionWebView.scrollView.backgroundColor = UIColor.clear
        questionWebView.scrollView.bounces = false
        
        answerView1.configuration.userContentController.addUserScript(viewPortScript())
        answerView1.tag = 1
        answerView1.uiDelegate = self
        answerView1.navigationDelegate = self
        answerView1.backgroundColor = UIColor.clear
        answerView1.scrollView.backgroundColor = UIColor.clear
        answerView1.scrollView.bounces = false
        
        answerView2.configuration.userContentController.addUserScript(viewPortScript())
        answerView2.tag = 2
        answerView2.uiDelegate = self
        answerView2.navigationDelegate = self
        answerView2.backgroundColor = UIColor.clear
        answerView2.scrollView.backgroundColor = UIColor.clear
        answerView2.scrollView.bounces = false
        
        answerView3.configuration.userContentController.addUserScript(viewPortScript())
        answerView3.tag = 3
        answerView3.uiDelegate = self
        answerView3.navigationDelegate = self
        answerView3.backgroundColor = UIColor.clear
        answerView3.scrollView.backgroundColor = UIColor.clear
        answerView3.scrollView.bounces = false
        
        answerView4.configuration.userContentController.addUserScript(viewPortScript())
        answerView4.tag = 4
        answerView4.uiDelegate = self
        answerView4.navigationDelegate = self
        answerView4.backgroundColor = UIColor.clear
        answerView4.scrollView.backgroundColor = UIColor.clear
        answerView4.scrollView.bounces = false

    }
    
    @objc func rotated() {
        self.loadWebViews()
    }
    
    @objc func reloadContentView() {
        self.loadWebViews()
    }
    
    @IBAction func didTapCloseIcon(sender: AnyObject) {
        NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIContentSizeCategory.didChangeNotification, object: nil)
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.view.isHidden = false
        scrollView.setContentOffset(.zero, animated: true)
        
        self.answer1Label.textColor = MCQRootModel.shared.themeColors.secondaryHeaderColor
        self.answer2Label.textColor = MCQRootModel.shared.themeColors.secondaryHeaderColor
        self.answer3Label.textColor = MCQRootModel.shared.themeColors.secondaryHeaderColor
        self.answer4Label.textColor = MCQRootModel.shared.themeColors.secondaryHeaderColor
        self.questionLabel.textColor = MCQRootModel.shared.themeColors.secondaryHeaderColor
        
        self.nextButton.isHidden = true
        self.isAnswered = false
        self.startTime = MCQUtilities.sharedInstance.getCurrentTimeStamp()
        self.mcqStudy = MCQRootModel.shared.mcqModel.mcq
        self.mcq = MCQRootModel.shared.mcqModel.mcq.termDefSet[self.mcqStudy.lastVisitedPageIndex]
        
        nextButton.setTitle(self.isLastItem ? MCQGuideConstants.Complete : MCQGuideConstants.NEXT, for: .normal)
        self.updateChoices()
        
        cleanWebView()
    }
    
    func cleanWebView() {
        
        self.questionLblView.isHidden = true
        self.questionBaseView.isHidden = true
        
        self.questionLabel.isHidden = true
        self.answer1Label.isHidden = true
        self.answer2Label.isHidden = true
        self.answer3Label.isHidden = true
        self.answer4Label.isHidden = true
        
        self.questionLabel.text = ""
        self.answer1Label.text = ""
        self.answer2Label.text = ""
        self.answer3Label.text = ""
        self.answer4Label.text = ""
        
        self.questionWebView.isHidden = true
        self.answerView1.isHidden = true
        self.answerView2.isHidden = true
        self.answerView3.isHidden = true
        self.answerView4.isHidden = true
        
        webViewHeight.constant = 0
        questionLblHeight.constant = 0
        answer1LblHeight.constant = 0
        answer2LblHeight.constant = 0
        answer3LblHeight.constant = 0
        answer4LblHeight.constant = 0
        
        answerView1Height.constant = 0
        answerView2Height.constant = 0
        answerView3Height.constant = 0
        answerView4Height.constant = 0
        
        iconView1.isHidden = true
        iconView2.isHidden = true
        iconView3.isHidden = true
        iconView4.isHidden = true
        
        totalWebCount = 0
        webViewCount = 0

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            //Load request or write code here
            self.startIndicator()
            self.loadWebViews()
        })
    }
    
    func loadWebViews() {
        // Set Question
        urlString = ""
        webViewCount = 0 // reset the count to hide indicator
        
        let htmlString = self.mcq.definition.first as? String
//        print("--- questions --- \(htmlString)")

        if self.mcq.questionMedia == "HTML" || self.mcq.questionMedia == "MEDIA" {
            self.questionBaseView.isHidden = false
            self.questionWebView.isHidden = false
            self.questionLblView.isHidden = true
            totalWebCount += 1
            self.questionWebView.loadHTMLString(htmlString ?? "", baseURL: nil)
        }else {
            self.questionBaseView.isHidden = true
            self.questionWebView.isHidden = true
            self.questionLblView.isHidden = false
            self.configureAttributedCell(htmlString ?? "", label: self.questionLabel, index: 0, tag: 10)
        }
        
        
        self.loadContents()
    }
    
    func updateChoices() {
        self.mcqChoices.removeAll()
        self.mcqAccessibilityString.removeAll()
        for ans in mcq.choices {
            if let choice = ans as? String {
                self.mcqAccessibilityString.append(choice)
                self.mcqChoices.append(choice)
            }
        }
    }
    
    func resetValues() {
        
        backView1.isUserInteractionEnabled = true
        backView2.isUserInteractionEnabled = true
        backView3.isUserInteractionEnabled = true
        backView4.isUserInteractionEnabled = true
    
        selectedAnswerIndex = 0
        correctAnswerIndex = 0
        webViewHeight.constant = 0
        questionLblHeight.constant = 0
        answer1LblHeight.constant = 0
        answer2LblHeight.constant = 0
        answer3LblHeight.constant = 0
        answer4LblHeight.constant = 0
        
        answerView1Height.constant = 0
        answerView2Height.constant = 0
        answerView3Height.constant = 0
        answerView4Height.constant = 0
    }
    
    func verifySelectedAnswer(_ indexPath: IndexPath) -> Bool {
        var isSelectedAnswerCorrect = false
        if let selectedTermArray = self.mcq.choices[indexPath.row - 1] as?  [Any] {
            var selectedTermId = ""
            if let textTerm = selectedTermArray.first as? String {
                selectedTermId = textTerm
            } else if let dictTerm = selectedTermArray.first as? NSDictionary {
                if let src = dictTerm["src"] as? String {
                    selectedTermId = src
                }
            }
            
            let ans = self.mcq.answers?.filter{$0.correct == true}.first?.value ?? ""
            isSelectedAnswerCorrect = selectedTermId == ans
        }
        return isSelectedAnswerCorrect
    }
    
    @IBAction func didTappedNextBtn(_ sender: Any) {
        let analyticsValue = [MCQGuideConstants.EventCategory: MCQGuideConstants.StudyToolsMCQ, MCQGuideConstants.EventLabel: MCQGuideConstants.NEXT, MCQGuideConstants.EventAction: MCQGuideConstants.NEXT, MCQGuideConstants.EventValue: MCQRootModel.shared.summaryMastery, MCQGuideConstants.PageId: self.pageId, MCQGuideConstants.QuestionId: self.mcqStudy.id] as [String : Any]
        self.mcqDelegate?.didTapNextMCQ(analyticsValue)
        self.initiateAnimation()
    }
    
    func initiateAnimation() {
        self.mcqStudy.lastVisitedPageIndex += 1
        if self.mcqStudy.lastVisitedPageIndex < MCQRootModel.shared.mcqModel.mcq.termDefSet.count && self.mcqStudy.lastVisitedPageIndex >= 0 {
            self.mcq = MCQRootModel.shared.mcqModel.mcq.termDefSet[self.mcqStudy.lastVisitedPageIndex]
            self.resetValues()
            self.updateChoices()
            self.loadWebViews()
        } else {
//            self.resetValues()
            self.view.isHidden = true
            selectedAnswerIndex = 0
            correctAnswerIndex = 0
            webViewHeight.constant = 0
            
            self.endTime = MCQUtilities.sharedInstance.getCurrentTimeStamp()
            self.mcqDelegate?.didMoveFromMCQ(self.startTime, self.endTime)
        }
    }
}

extension MCQWebCardVC {
    
    func setDefaultCell(_ webView: WKWebView, _ tag: Int, _ backView: UIView, _ frontView: UIView, _ icon: UIImageView, _ textLabel: UILabel, status: Bool?) {
        
        webView.tag = tag
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapOnView(sender:)))
        frontView.addGestureRecognizer(tap)
        icon.isHidden = false
        
        if status == nil {
            webView.scrollView.bounces = false
            
            let image = MCQUtilities.sharedInstance.fetchImage("RadioButton" as String)
            icon.image = image
            icon.tintColor = MCQRootModel.shared.themeColors.radioButtonUnselectedColor
            backView.backgroundColor = MCQRootModel.shared.themeColors.secondaryButtonColor
            
            
        } else {
            if let status = status {
                switch status {
                case false:
                        let imgName = "WrongIcon"
                        let image = MCQUtilities.sharedInstance.fetchImage(imgName as String)
                        icon.image = image
                    icon.tintColor = MCQRootModel.shared.themeColors.correctAnswerColor
                        textLabel.textColor = MCQRootModel.shared.themeColors.incorrectAnswerColor
                case true:
                    let imgName = "CorrectIcon"
                    let image = MCQUtilities.sharedInstance.fetchImage(imgName as String)
                    icon.image = image
                    icon.tintColor = MCQRootModel.shared.themeColors.correctAnswerColor
                    textLabel.textColor = MCQRootModel.shared.themeColors.correctAnswerColor
                default:
                    let image = MCQUtilities.sharedInstance.fetchImage("RadioButtonSelected" as String)
                    var selectedRadioButtonColor = UIColor.clear
                    
                    selectedRadioButtonColor = MCQRootModel.shared.themeColors.radioButtonUnselectedColor
                    icon.image = image
                    icon.tintColor = selectedRadioButtonColor
                    textLabel.textColor = selectedRadioButtonColor
                }
            }
        }
        
        
    }
    
    @objc func tapOnView(sender: UITapGestureRecognizer) {
        if !isAnswered {
            if let view = sender.view {
                let index = view.tag - 1
                
                self.isAnswered = true
                self.selectedAnswerIndex = index
                
                let image = MCQUtilities.sharedInstance.fetchImage("RadioButtonSelected" as String)
                var selectedRadioButtonColor = UIColor.clear
                
                selectedRadioButtonColor = MCQRootModel.shared.themeColors.secondaryHeaderColor
                self.answer1Label.textColor = MCQRootModel.shared.themeColors.secondaryHeaderColor
                  self.answer2Label.textColor = MCQRootModel.shared.themeColors.secondaryHeaderColor
                  self.answer3Label.textColor = MCQRootModel.shared.themeColors.secondaryHeaderColor
                  self.answer4Label.textColor = MCQRootModel.shared.themeColors.secondaryHeaderColor 
                
                let viewTag = index + 1
                switch viewTag {
                case 1:
                  self.iconView1.image = image
                  self.iconView1.tintColor = selectedRadioButtonColor
                case 2:
                   self.iconView2.image = image
                   self.iconView2.tintColor = selectedRadioButtonColor
                case 3:
                    self.iconView3.image = image
                    self.iconView3.tintColor = selectedRadioButtonColor
                case 4:
                   self.iconView4.image = image
                   self.iconView4.tintColor = selectedRadioButtonColor
                default:
                    print("New webview")
                }
                
                
                let choiceAttributedString = self.mcqChoices[index]
//                print("--- Tapped Answer --- \(choiceAttributedString)")
//                print(" --- correct Answer --- \(self.mcq.term.first as! String)")
                if !choiceAttributedString.isEmpty {
//                    answerCell.onAnswerTaped()
                    if choiceAttributedString == self.mcq.term.first as! String {
                        isAnsweredCorrectly = true
                    }
                    else {
                        isAnsweredCorrectly = false
                    }
                }else {
                    isAnsweredCorrectly = false
                }
                
                if let index = self.mcqChoices.firstIndex(of: self.mcq.term.first as! String) {
                    correctAnswerIndex = index
                }
                isSelectedAnswerCorrect = isAnsweredCorrectly
                
//                print(" --- correct Answer Index--- \(correctAnswerIndex)")
                mcq.attempts.removeAll()
                mcq.attempts.append(isSelectedAnswerCorrect ? .correct : .incorrect)
                updateAnsweredUI()
                self.nextButton.isHidden = false
            }
        }
    }
    
    func loadContents() {
        print("---Count \(self.mcqChoices.count) ----")
        if self.mcq.choices.count > 0 {
            switch self.mcq.choices.count {
            case 1:
                self.backView2.isHidden = true
                self.backView3.isHidden = true
                self.backView4.isHidden = true

                self.setDefaultCell(answerView1, 1, backView1, answerFrontView1, iconView1, answer1Label, status: getStatus(0))

                if self.mcq.answers?[0].type == "HTML" || self.mcq.answers?[0].type == "MEDIA"{
                    self.answerView1.isHidden = false
                    totalWebCount += 1
                    self.answerView1.loadHTMLString(self.mcqChoices[0], baseURL:nil)
                }
                else {
                    self.answerView1.isHidden = true
                    self.configureAttributedCell(self.mcqChoices[0], label: self.answer1Label, index: 0, tag: 1)
                }

            case 2:
                self.backView3.isHidden = true
                self.backView4.isHidden = true

                self.setDefaultCell(answerView1, 1, backView1, answerFrontView1, iconView1, answer1Label, status: getStatus(0))
                self.setDefaultCell(answerView2, 2, backView2, answerFrontView2, iconView2, answer2Label, status: getStatus(1))

                if self.mcq.answers?[0].type == "HTML" || self.mcq.answers?[0].type == "MEDIA"{
                    self.answerView1.isHidden = false
                    totalWebCount += 1
                    self.answerView1.loadHTMLString(self.mcqChoices[0], baseURL:nil)
                }else {
                    self.answerView1.isHidden = true
                    self.configureAttributedCell(self.mcqChoices[0], label: self.answer1Label, index: 0, tag: 1)
                }

                if self.mcq.answers?[1].type == "HTML" || self.mcq.answers?[1].type == "MEDIA"{
                    self.answerView2.isHidden = false
                    totalWebCount += 1
                    self.answerView2.loadHTMLString(self.mcqChoices[1], baseURL:nil)
                }else {
                     self.answerView2.isHidden = true
                    self.configureAttributedCell(self.mcqChoices[1], label: self.answer2Label, index: 1, tag: 2)
                }

            case 3:
                self.backView4.isHidden = true

                self.setDefaultCell(answerView1, 1, backView1, answerFrontView1, iconView1, answer1Label, status: getStatus(0))
                self.setDefaultCell(answerView2, 2, backView2, answerFrontView2, iconView2, answer2Label, status:getStatus(1))
                self.setDefaultCell(answerView3, 3, backView3, answerFrontView3, iconView3, answer3Label, status: getStatus(2))

                if self.mcq.answers?[0].type == "HTML" || self.mcq.answers?[0].type == "MEDIA"{
                    self.answerView1.isHidden = false
                    totalWebCount += 1
                    self.answerView1.loadHTMLString(self.mcqChoices[0], baseURL:nil)
                }else {
                    self.answerView1.isHidden = true
                    self.configureAttributedCell(self.mcqChoices[0], label: self.answer1Label, index: 0, tag: 1)
                }

                if self.mcq.answers?[1].type == "HTML" || self.mcq.answers?[1].type == "MEDIA"{
                    self.answerView2.isHidden = false
                    totalWebCount += 1
                    self.answerView2.loadHTMLString(self.mcqChoices[1], baseURL:nil)
                }else {
                    self.answerView2.isHidden = true
                    self.configureAttributedCell(self.mcqChoices[1], label: self.answer2Label, index: 1, tag: 2)
                }

                if self.mcq.answers?[2].type == "HTML" || self.mcq.answers?[2].type == "MEDIA"{
                    self.answerView3.isHidden = false
                    totalWebCount += 1
                    self.answerView3.loadHTMLString(self.mcqChoices[2], baseURL:nil)
                }else {
                    self.answerView3.isHidden = true
                    self.configureAttributedCell(self.mcqChoices[2], label: self.answer3Label, index: 2, tag: 3)
                }

            default:
                self.setDefaultCell(answerView1, 1, backView1, answerFrontView1, iconView1, answer1Label, status: getStatus(0))
                self.setDefaultCell(answerView2, 2, backView2, answerFrontView2, iconView2, answer2Label, status: getStatus(1))
                self.setDefaultCell(answerView3, 3, backView3, answerFrontView3, iconView3, answer3Label, status: getStatus(2))
                self.setDefaultCell(answerView4, 4, backView4, answerFrontView4, iconView4, answer4Label, status: getStatus(3))

                if self.mcq.answers?[0].type == "HTML" || self.mcq.answers?[0].type == "MEDIA"{
                    self.answerView1.isHidden = false
                    totalWebCount += 1
                    self.answerView1.loadHTMLString(self.mcqChoices[0], baseURL:nil)
                }else {
                     self.answerView1.isHidden = true
                    self.configureAttributedCell(self.mcqChoices[0], label: self.answer1Label, index: 0, tag: 1)
                }

                if self.mcq.answers?[1].type == "HTML" || self.mcq.answers?[1].type == "MEDIA"{
                    self.answerView2.isHidden = false
                    totalWebCount += 1
                    self.answerView2.loadHTMLString(self.mcqChoices[1], baseURL:nil)
                }else {
                    self.answerView2.isHidden = true
                    self.configureAttributedCell(self.mcqChoices[1], label: self.answer2Label, index: 1, tag: 2)
                }

                if self.mcq.answers?[2].type == "HTML" || self.mcq.answers?[2].type == "MEDIA"{
                    self.answerView3.isHidden = false
                    totalWebCount += 1
                    self.answerView3.loadHTMLString(self.mcqChoices[2], baseURL:nil)
                }else {
                    self.answerView3.isHidden = true
                    self.configureAttributedCell(self.mcqChoices[2], label: self.answer3Label, index: 2, tag: 3)
                }

                if self.mcq.answers?[3].type == "HTML" || self.mcq.answers?[3].type == "MEDIA"{
                    self.answerView4.isHidden = false
                    totalWebCount += 1
                    self.answerView4.loadHTMLString(self.mcqChoices[3], baseURL:nil)
                }else {
                    self.answerView4.isHidden = true
                    self.configureAttributedCell(self.mcqChoices[3], label: self.answer4Label, index: 3, tag: 4)
                }
                self.stopIndicator()
            }

            if totalWebCount == 0 {
                self.stopIndicator()
            }
        }

    }
    
    func updateAnsweredUI() {
        
        switch self.mcq.choices.count {
        case 1:
            self.setDefaultCell(answerView1, 1, backView1, answerFrontView1, iconView1, answer1Label, status: getStatus(0))
        
        case 2:
            self.setDefaultCell(answerView1, 1, backView1, answerFrontView1, iconView1, answer1Label, status: getStatus(0))
            self.setDefaultCell(answerView2, 2, backView2, answerFrontView2, iconView2, answer2Label, status: getStatus(1))
        case 3:
            
            self.setDefaultCell(answerView1, 1, backView1, answerFrontView1, iconView1, answer1Label, status: getStatus(0))
            self.setDefaultCell(answerView2, 2, backView2, answerFrontView2, iconView2, answer2Label, status:getStatus(1))
            self.setDefaultCell(answerView3, 3, backView3, answerFrontView3, iconView3, answer3Label, status: getStatus(2))
                
        default:
            self.setDefaultCell(answerView1, 1, backView1, answerFrontView1, iconView1, answer1Label, status: getStatus(0))
            self.setDefaultCell(answerView2, 2, backView2, answerFrontView2, iconView2, answer2Label, status: getStatus(1))
            self.setDefaultCell(answerView3, 3, backView3, answerFrontView3, iconView3, answer3Label, status: getStatus(2))
            self.setDefaultCell(answerView4, 4, backView4, answerFrontView4, iconView4, answer4Label, status: getStatus(3))
        }
        
    }
    
    func getStatus(_ index: Int) -> Bool? {
        if isAnswered {
            if isAnsweredCorrectly {
                if index == selectedAnswerIndex {
                    return true
                }else {
                    return nil
                }
            } else {
                if index == selectedAnswerIndex {
                    return false
                } else if index == correctAnswerIndex {
                    return true
                } else {
                    return nil
                }
            }
        } else {
            return nil
        }
    }
    
    func configureAttributedCell(_ choice: String, label:UILabel, index: Int,tag:Int) {
        
        let attrString = NSMutableAttributedString(string: String(choice),
        attributes: [ NSAttributedString.Key.font: UIFont(name: MCQFont.HindRegular, size: 16) ?? UIFont.boldSystemFont(ofSize: 16)])
        
        label.attributedText = attrString
        
        print("-- Text --- \(attrString)")
        label.adjustsFontSizeToFitWidth = true

        if MCQUtilities.sharedInstance.isRTL {
            label.textAlignment = .right
        }
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        
        label.isHidden = false
        
        let labelHeight = attrString.height(withConstrainedWidth: self.view.frame.size.width - 80)
    
        switch tag {
        case 10:
            self.questionLblView.isHidden = false
            questionLblHeight.constant = labelHeight + 20
            print("--- QuestionLbl height --- \(questionLblHeight.constant)")
        case 1:
            answer1LblHeight.constant = labelHeight
            answerView1Height.constant = labelHeight + 20
            print("--- answer1 height --- \(answerView1Height.constant)")
        case 2:
            answer2LblHeight.constant = labelHeight
            answerView2Height.constant =  labelHeight + 20
            print("--- answer2 height --- \(answerView2Height.constant)")
        case 3:
             answer3LblHeight.constant = labelHeight
            answerView3Height.constant =  labelHeight + 20
            print("--- answer3 height --- \(answerView3Height.constant)")
        case 4:
            answer4LblHeight.constant = labelHeight
            answerView4Height.constant =  labelHeight + 20
            
            print("--- answer4 height --- \(answerView4Height.constant)")
        default:
            print("New webview")
        }
        label.sizeToFit()
    }
}


extension MCQWebCardVC : WKUIDelegate {
    
    public func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        let preferences = WKPreferences()
        //preferences.javaEnabled = true
        preferences.javaScriptCanOpenWindowsAutomatically = true
        let configuration = WKWebViewConfiguration()
        configuration.preferences = preferences
        return WKWebView(frame: webView.frame, configuration: configuration)
    }
    
    private func viewPortScript() -> WKUserScript {
        let viewPortScript = """
            var meta = document.createElement('meta');
            meta.setAttribute('name', 'viewport');
            meta.setAttribute('content', 'width=device-width','shrink-to-fit=YES','initial-scale=1.0');
            meta.setAttribute('user-scalable', 'no');
            document.getElementsByTagName('head')[0].appendChild(meta);
        """
        return WKUserScript(source: viewPortScript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
    }
}

extension MCQWebCardVC: WKNavigationDelegate {
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(.allow)
    }
    
    public func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
       
    }

    public func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
         self.stopIndicator()
    }
    public func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.stopIndicator()
    }
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        

       webView.evaluateJavaScript("document.getElementsByTagName('body')[0].style.fontFamily =\"Hind-Bold\"")
       webView.evaluateJavaScript("document.getElementsByTagName('body')[0].style.fontSize = \"16\"")
        
        // get the html string to hide indicator
       webView.evaluateJavaScript("document.documentElement.innerHTML.toString()", completionHandler: { html, error in
            self.urlString = html as! String
        })
        
       let textColorString = " document.getElementsByTagName('body')[0].style.webkitTextFillColor= '#color'"
        if isAnswered {
            if webView.tag == correctAnswerIndex + 1 {
                let htmlTextcorrect = textColorString.replacingOccurrences(of: "#color", with: String.correctAnswerColor)
                webView.evaluateJavaScript(htmlTextcorrect)
            }else if webView.tag == (selectedAnswerIndex + 1) && !isAnsweredCorrectly {
                let htmlTextIncorrect = textColorString.replacingOccurrences(of: "#color", with: String.incorrectAnswerColor)
                webView.evaluateJavaScript(htmlTextIncorrect)
            }
        }
        else {
            let htmlTextIncorrect = textColorString.replacingOccurrences(of: "#color", with: String.webTextColor)
            webView.evaluateJavaScript(htmlTextIncorrect)
        }
       
        webView.evaluateJavaScript("document.readyState", completionHandler: { [weak self] (complete, error) in
            if complete != nil {
                webView.evaluateJavaScript("document.documentElement.scrollHeight", completionHandler: { [weak self] (height, error) in
                    if let scrollHeight = height as? CGFloat {
//                        print("--- \(webView.tag) --- Height ---\(scrollHeight)")

                        self?.updateWebViewHeight(webView: webView)
//                        if self?.urlString.contains("<head>") ?? false {
                        if self?.urlString.count ?? 0 > 0 {
                             self?.webViewCount += 1
                            if self?.webViewCount == self?.totalWebCount {
                                self?.stopIndicator()
                                self?.view.layoutIfNeeded()
                            }
                        }else {
                            self?.stopIndicator()
                        }
                       
                    }
                })
            }
        })
        
    }
    
    func updateWebViewHeight(webView:WKWebView) {
        print("--- WebTag --- \(webView.tag) --- webHeight --\(webView.scrollView.contentSize.height)")
        webView.scrollView.isScrollEnabled = false
        switch webView.tag {
        case 10:
            webViewHeight.constant = webView.scrollView.contentSize.height > 30 ? webView.scrollView.contentSize.height : 60
        case 1:
            answerView1Height.constant = webView.scrollView.contentSize.height > 30 ? webView.scrollView.contentSize.height : 60
        case 2:
            answerView2Height.constant = webView.scrollView.contentSize.height > 30 ? webView.scrollView.contentSize.height : 60
        case 3:
            answerView3Height.constant = webView.scrollView.contentSize.height > 30 ? webView.scrollView.contentSize.height : 60
        case 4:
            answerView4Height.constant = webView.scrollView.contentSize.height > 30 ? webView.scrollView.contentSize.height : 60
        default:
            print("New webview")
        }
        webView.isOpaque = false
        webView.backgroundColor = UIColor.clear
        webView.scrollView.backgroundColor = UIColor.clear
        //        webView.contentMode = .scaleAspectFit
                webView.sizeToFit()
        webView.layoutIfNeeded()
    }
    
    func startIndicator(){
        DispatchQueue.main.async {
            self.iconView1.isHidden = true
            self.iconView2.isHidden = true
            self.iconView3.isHidden = true
            self.iconView4.isHidden = true
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
    }
    func stopIndicator() {
        DispatchQueue.main.async {
            self.iconView1.isHidden = false
            self.iconView2.isHidden = false
            self.iconView3.isHidden = false
            self.iconView4.isHidden = false
            
           MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        print("-- TextHeight --- \(ceil(boundingBox.height))")
        return ceil(boundingBox.height)
    }

    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)

        return ceil(boundingBox.width)
    }
}

extension NSMutableAttributedString {
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        print("-- AttributeTextHeight --- \(ceil(boundingBox.height))")
        return ceil(boundingBox.height)
    }

    func width(withConstrainedHeight height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
    
        return ceil(boundingBox.width)
    }
}
