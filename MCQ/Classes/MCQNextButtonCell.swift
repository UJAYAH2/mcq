/* PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA

*  Copyright © 2021 Pearson Education, Inc.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Pearson Education, Inc.  The intellectual and technical concepts contained
* herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
* patent applications, and are protected by trade secret or copyright law.
* Dissemination of this information, reproduction of this material, and copying or distribution of this software
* is strictly forbidden unless prior written permission is obtained
* from Pearson Education, Inc.*/

import UIKit

class MCQNextButtonTableViewCell: UITableViewCell {

    var indexPath: IndexPath?
    weak var delegate: TinyQuizGestureDelegate?
    var showNextButton: Bool? {
        didSet {
            isUserInteractionEnabled = true
            nextButton.isHidden = showNextButton ?? false ? false : true
        }
    }
    var isLastItem: Bool = false {
        didSet {
            nextButton.setTitle(isLastItem ? MCQGuideConstants.Complete : MCQGuideConstants.NEXT, for: .normal)
        }
    }

    let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()

     lazy var nextButton: MCQActionButton = {
        let button = MCQActionButton()
        button.setTitle(MCQGuideConstants.NEXT, for: .normal)
        button.addTarget(self, action: #selector(handleNextAction), for: .touchUpInside)
        return button
    }()

    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        backgroundColor = UIColor().viewBackgroundColor
        selectionStyle = .none
        isUserInteractionEnabled = true
        setupView()
    }

    private func setupView() {
        
        
        self.containerView.backgroundColor = MCQRootModel.shared.themeColors.primaryBackgroundColor
        self.contentView.addSubview(containerView)
        containerView.addSubview(nextButton)
        containerView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 108, centerX: nil, centerY: nil)
        nextButton.anchor(top: nil, left: nil, bottom: nil, right: nil, paddingTop: nil, paddingLeft: nil, paddingBottom: nil, paddingRight: nil, width: 123, height: 40, centerX: containerView.centerXAnchor, centerY: containerView.centerYAnchor)
    }

    @objc func handleNextAction() {
        if let safeIndexPath = self.indexPath {
            self.delegate?.didMoveToNext(safeIndexPath, cell: self)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}


